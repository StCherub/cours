

Ce dépôt contient des éléments de formation à code_aster, salome et à la modélisation numérique en mécanique.
Il s'agit d'éléments de cours (polycopiés, présentations) et de fascicules de travaux pratiques.

Tous les documents sont au format LATEX.

> Home page: <https://www.code-aster.org/>

# Content of the repository

Quatre répertoires principaux:
- general
- codeaster
- salome
- modelisation

## Contenu de general

Contient des macros et des packages Latex pour générer les documents.

## Contenu de codeaster

Il y a des polycopiés de cours pour codeaster et des énoncés de travaux pratiques

## Contenu de salome

Contient des fascicules de travaux pratiques pour salome (GEOM et MESH)

## Contenu de modelisation

Documents génériques sur la modélisation numérique en mécanique: polycopiés, slides.
