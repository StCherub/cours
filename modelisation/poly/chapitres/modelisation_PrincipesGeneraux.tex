\chapter{Principes de la modélisation numérique}

La modélisation (ou simulation) numérique est une discipline scientifique visant à avoir une représentation plus ou moins fidèle d'un phénomène physique réel.
Cette discipline n'est pas formalisée en tant que telle mais regroupe différentes sciences et techniques. Elle est désormais incontournable dans les sciences de l'ingénieur et a commencé à se déployer dans d'autres champs de la connaissance comme les sciences humaines.
C'est un outil très puissant, qui a pour objectif d'être prédictif et de compléter ou de remplacer en partie les autres méthodes comme l'expérimentation ou le maquettage. On parle d'ailleurs parfois de <<maquettage numérique>> (ou \emph{digital mock-up} en anglais)

Dans ce chapitre, nous allons proposer de détailler un peu le mode de fonctionnement et les différents ingrédients dans un cadre général, faisant abstraction de l'outil (le logiciel) proprement dit.

\section{Principes du modèle numérique}
\subsection{Définition}

Un modèle numérique est la représentation \emph{informatique} d'un phénomène physique. Du fait de sa complexité, il est important de bien saisir les différentes échelles de représentation du phénomène et d'avoir en tête les différentes hypothèses et erreurs qu'on introduit dans un modèle numérique.
L'utilisation d'un logiciel de modélisation numérique, quelque soit son origine (et son prix) requiert un grand sens de l'analyse et beaucoup de précautions. Le coté quelque peu <<magique>> des représentations graphiques ne doit pas faire oublier que le logiciel est une œuvre de l'esprit humain, avec ses imperfections, ses erreurs et ses approximations. Si l'ordinateur ne se trompe pas (ou du moins, rarement en dehors des conséquences du rayonnement cosmologique !), ce n'est pas le cas ni des concepteurs du logiciel, ni de ses utilisateurs.

\subsection{Usages}

La simulation numérique a trois usages principaux:
\begin{enumerate}
    \item Prévoir un fonctionnement dans le cadre de la \emph{conception} d'un produit;
    \item Permettre l'\emph{analyse} d'un phénomène sur un produit ou un matériel existant;
    \item Identifier des paramètres;
\end{enumerate}
Si ces usages utilisent en général des logiciels identiques, il est néanmoins indispensable de procéder à des ajustements sur les modèles numériques.

\subsection{Quelques produits}

Le marché des logiciels de simulation numérique est très vaste et comporte beaucoup d'acteurs, commerciaux ou pas. Il existe des centaines de produits, plus ou moins complexes, plus ou moins complets, pour des usages très divers. En gros, on peut distinguer trois grandes catégories de logiciels:
\begin{enumerate}
    \item Les logiciels <<all-purpose>>, destinés à couvrir une gamme large de physiques et de modèles;
    \item Les outils métiers, destinés à des besoins très spécifiques;
    \item Les outils programmables pour lesquels un travail spécifique de modélisation est nécessaire;
\end{enumerate}

Tout ceci doit vous inciter à une grande prudence dans le choix des outils. Le but de ce cours est de former à avoir les bons réflexes et non pas à former à un outil spécifique. Il serait quelque peu déraisonnable de penser que l'apprentissage d'un ou plusieurs logiciels <<bien choisis>> vous dispense de comprendre leur fonctionnement. Il est très peu probable que vous ne soyez jamais amené à utiliser le même logiciel toute votre carrière, et encore moins probable que ce soit le même que vous avez manipulé durant vos années d'étudiant !

\subsubsection{Les logiciels généralistes}

Dans la première catégorie, vous trouverez un nombre considérable de logiciels, le marché mondial n'étant pas arrivé à un niveau de concentration équivalent à ce qu'on peut retrouver dans d'autres secteurs. On peut citer:

\begin{itemize}
    \item NASTRAN est un logiciel initialement conçu par la NASA en 1968. Le produit s’appuie sur la plate-forme de pré-post MSC MARC et prend alors le nom de MSC-NASTRAN. Mais plusieurs versions de NASTRAN existent via la vente de licences d’exploitation. NX-NASTRAN (racheté par Siemens) est par exemple le solveur d’Ideas.
    \item ABAQUS est originellement un produit conçu par la société HKS: Hibbit Karlson Sorensen. Puis le solveur a été racheté par Dassault Systèmes. ABAQUS peut alors s’appuyer sur le logiciel de CAO Catia, l'un des plus utilisés dans le monde. Avec d'autres produits de simulation, l'ensemble constitue la solution intégrée DS Simulia;
    \item ANSYS est un produit américain (1979) qui s'intègre dans la plateforme ANSYS Workbench avec d'autres produits de simulation dans différentes physiques. ANSYS Workbench est l'un des produits les plus complets du point des vue des physiques couvertes, à comparer à un produit comme COMSOL;
    \item SAMCEF (1962) est un produit belge issue des activités de recherche de l'université de Liège. C'est un produit indépendant qui dispose de sa propre plateforme de pré et post-traitement (BACON), traditionnellement très utilisé dans l'industrie aéronautique.
\end{itemize}

En France, on peut également citer les produits de la société ESI Group, dont la série \emph{PAM} (PAM-Stamp pour l'emboutissage, PAM-Crash pour le calcul de crash, etc.), mais également le produit de CFD\footnote{(CFD, Computational Fluid Dynamic)} OpenFOAM issu de la communauté du libre. Il y a également la suite Zébulon, issu des travaux de recherches de l’École des Mines (centre des matériaux d'Evry) et de l'ONERA. C'est aussi le code support des activités les plus plus avancées en calcul haute performances (décomposition de domaine, méthode FETI).

Il existe également une myriade de logiciels gratuits ou libres dont on peut citer les principaux:
\begin{itemize}
    \item FreeFEM+ est un produit porté par la communauté française de l'analyse numérique, autour de structures de recherches comme le laboratoire Jacques-Louis Lions à l'université Paris 6 Jussieu ou le laboratoire CMAP à l’École Polytechnique;
    \item GetFEM est un autre produit français, équivalent à FreeFEM+, très utilisé dans la communauté de la simulation du contact/frottement ou des problèmes de raccord de modèles;
    \item CAST3M est un produit du CEA utilisé dans le monde du nucléaire et largement répandu dans la communauté de la recherche en mécanique;
    \item Code-Aster est un solveur de mécanique conçu par EDF datant de la même époque que CAST3M et libéré sous licence GPL depuis 2001. Il est aussi distribué (toujours sous licence GPL) au sein de la plate-forme Salome (plate-forme co-développée par EDF, le CEA et OpenCascade) sous le nom de Salome-Meca.
\end{itemize}

\subsubsection{Les outils-métiers}

Dans la catégorie des outil-métiers, il est très fréquent que ces logiciels soient des outils anciens, issus d'une certaine tradition disciplinaire. On les réserve à un usage spécifique et il est fréquent que les utilisateurs de ces produits ne soient pas des professionnels aguerris de la simulation numérique mais plutôt des experts du métier.

\subsubsection{Les outils programmables}

Enfin, la dernière catégorie regroupe les logiciels de simulation programmables tels que Matlab (ou ses équivalents libres comme Scilab et Octave), voire des des langages de programmation standards (essentiellement le Fortran dans ce secteur d'activité).

\subsection{Les niveaux d'acceptation}

Dans le cadre de l'utilisation d'un logiciel de simulation numérique, il convient de prioriser les résultats qu'on en attend. En effet, le processus de V\&V (voir \ref{Chap_VV}) ne vous donne pas toutes les exigences qu'on attend d'un code mais seulement la manière de parvenir à ces exigences.

En premier lieu, un code de calcul doit être \emph{fiable}: les résultats du calcul doivent être conformes à la physique ou, du moins, au phénomène qu'on veut représenter. Ceci correspond effectivement à la définition habituelle du V\&V. C'est le minimum qu'on attend d'un code de calcul !

Mais cette exigence est insuffisante. Le deuxième niveau d'acceptation est la \emph{robustesse} du code de calcul: il doit donner une réponse \emph{fiable} dans toutes les conditions d'utilisation (paramètres physiques et numériques). Il ne sert à rien d'avoir un code sachant précisément donner un résultat sur un pilier en béton armé s'il en est incapable dès lors que le pilier se transforme en dalle ou en mur. C'est une condition nettement plus difficile à assurer, mais elle est souvent incluse dans le périmètre de la validation: un industriel vérifiera que le code défini comme étant fiable pour la physique considérée l'est aussi pour ses usages.

Le troisième niveau est celui des performances: une fois obtenu un résultat \emph{fiable} et \emph{robuste}, on veut qu'il soit \emph{performant}.

Cette croissance est hiérarchique: avoir un code de calcul rapide ne sert à rien si les résultats obtenus ne sont pas fiables !

Enfin, le dernier niveau est celui de l'ergonomie: l'accès facile à des résultats \emph{fiables}, \emph{robustes} et \emph{performants}. L'ergonomie est un concept qui dépasse la simple interface (graphique ou non), elle inclue également les notions de contrôle des modèles, de satisfaction aux critères de répétabilité (portabilité) et à l'aspect patrimonial (gestion des études sur de longues périodes de temps et travail collaboratif).

\subsection{Description du processus de modélisation}

Le principe de la modélisation numérique est d'avoir une représentation plus ou moins fidèle d'une réalité physique.

Le processus de modélisation est long et complexe. Il comprend plusieurs niveaux qui correspondent chacun à un ou plusieurs champs disciplinaires. La principale tache d'un expert en modélisation numérique sera de s'assurer la maîtrise des hypothèses du modèle et d’être capable d'en avoir une vision critique.
Il y a plusieurs façons de décrire le processus de modélisation numérique.
Certains auteurs séparent le processus en trois niveaux, selon le métier et/ou la discipline qu'ils adressent:
\begin{enumerate}
    \item Le premier niveau concerne le physicien (le mécanicien par exemple): il consiste à transformer le monde réel en modèle physique grâce aux mathématiques. Le modèle est alors continu (en temps et en espace), décrit par des équations aux dérivées partielles (EDP) et/ou des équations différentielles ordinaires (EDO);
    \item Le second niveau consiste à transformer ce modèle mathématique en modèle discret, apte à être traité par un ordinateur. C'est le domaine des mathématiques discrètes et de l'analyse numérique. Cela équivaut à transformer le système continu en système algébrique pour lequel des méthodes numériques de résolution peuvent être appliquées.
    \item Le troisième niveau est le codage ou la transformation en programme informatique de l'ensemble du modèle numérique. Dans cette dernière phase on peut aussi intégrer toute la problématique de mise en données (pré-traitement) et d'analyse (post-traitement).
\end{enumerate}

Cette description a pour principal inconvénient de promouvoir un raisonnement trop cloisonné. Cette démarche disciplinaire correspond en général à la spécialisation des différentes équipes et des experts dans le \emph{développement} d'un logiciel de simulation numérique. C'est donc plus une logique d'organisation qu'une logique de modélisation, ce qui constitue une vision réductrice et dangereuse pour un bon ingénieur de modélisation qui aura tendance à sous-estimer les parties de la modélisation qu'il pense ne pas lui échoir.

On préférera ici diviser la modélisation numérique en deux niveaux:
\begin{enumerate}
    \item La transformation d'une réalité physique (phénoménologique) en modèle mathématique;
    \item La transformation d'un modèle mathématique en modèle numérique;
\end{enumerate}

Cette séparation est assez artificielle car il y a beaucoup d'interactions entre les différents modèles mais elle a l'avantage de correspondre aux deux phases de création d'un modèle numérique: le choix des hypothèses de modélisation et l'analyse des résultats numériques.

\section{Le V\&V} \label{Chap_VV}

Les différents niveaux de modélisation sont associés à des processus de contrôle. On regroupe sous le terme de V\&V les processus visant à \emph{vérifier} et à \emph{valider} un logiciel de simulation numérique.

Ces deux termes ont des sens bien précis dans le contexte. Il existe un troisième niveau qu'on appelle <<certification>> ou <<acceptance>> qui sert à valider non pas le processus du logiciel mais le niveau de compétence du modélisateur.

La vérification, c'est le fait de contrôler qu'un code de calcul donne bien le résultat attendu des équations de modélisation. La vérification est donc faite à la fois sur la programmation de ces équations mais aussi sur les méthodes et algorithmes mis en œuvre dans le logiciel. En revanche elle ne dit rien sur la pertinence des modèles physiques proposés par rapport aux problèmes étudiés.

La validation est un processus plus global, visant à valider le fait que les équations représente bien la réalité dont on veut avoir une image. Elle repose en général sur une stratégie expérimentale ou sur une analyse dite <<à dire d'expert>>.


\subsection{La vérification}

En général, la vérification est de la responsabilité de l'éditeur du code de calcul. Mais la vérification doit être faite dans le cas du développement en propre d'un code de calcul ou de l'utilisation des logiciels programmables. Concernant les logiciels <<clefs-en-main>>, la confiance en l'éditeur du logiciel repose sur des éléments permettant de s'assurer que la vérification est correctement faite. Bien entendu, il est rarissime que des éditeurs de logiciel donnent accès à leur processus de vérification interne. Néanmoins, l'éditeur donne accès à plusieurs éléments de cette méthodologie:
\begin{itemize}
    \item La documentation permettant de savoir quelles hypothèses et quels modèles sont disponibles dans le code;
    \item Un certain nombre de cas-tests de vérification sont en général fournis avec le logiciel;
    \item Un service de support, qui peut parfois proposer des <<preuves>> de son processus de vérification;
\end{itemize}

Dans le cadre d'une démarche de sélection d'un logiciel de simulation numérique, la documentation et les tests de vérification sont essentiels et doivent être des éléments déterminant pour le choix.
Beaucoup de logiciels ont tendance à travailler en boite noire, sans donner accès aux paramètres fins (souvent basés sur des heuristiques). Ce n'est pas forcément rédhibitoire, par contre, il n'est pas acceptable qu'il ne soit pas possible d'avoir accès aux éléments de modélisation sélectionnés par l'éditeur: une documentation complète (avec des références \emph{récentes} issues de la communauté scientifique) et une batterie de cas-tests avec leur documentation.

\subsection{Quelques éléments de vérification}

Comment vérifie-t-on un code de calcul ? La question peut paraître hors-sujet dans le cas de l'utilisation d'un code commercial (modulo la fourniture par l'éditeur des exigences minimales que sont la documentation et les cas-tests). Mais il est intéressant de comprendre la manière de procéder pour en détecter les forces et les faiblesses. D'autant que ces méthodes pourront être utilisées pour les études.

\subsubsection{Vérification mathématique}

La première pierre de la vérification est la science: les modèles développés dans le code de calcul sont issus de publications et de travaux de recherche qu'il est indispensable de connaître, ne serait-ce que pour appréhender les limites d'usage de ces modèles. Ces travaux contiennent le développement des équations visant à reproduire le phénomène visé. Il est très souvent possible de calculer des solutions analytiquement à partir de ces équations, souvent sur des géométries simplifiées et avec quelques hypothèses simplificatrices. C'est donc le moyen privilégié de \emph{vérifier}: quantifier l'écart entre le modèle numérique et la solution analytique. Cet écart doit normalement être justifié soit par la limite de la représentation des opérations machines (précision machine) soit par des hypothèses maîtrisées sur les algorithmes utilisés.

Il existe une méthode plus élaborée de vérification qu'on appelle la méthode des <<solutions manufacturées>>. Cette méthode est plutôt issue de la communauté de la mécanique des fluides. L'idée est de construire systématiquement une solution analytique en faisant des hypothèses sur la solution finale. Par exemple, en supposant avoir un état de déformation homogène dans un cube, on peut être capable de reconstruire le modèle mathématique donnant cette solution et donc le simuler entièrement. Le principal intérêt de cette méthode est son coté systématique et aveugle. En effet, la résolution analytique traditionnel des problèmes peut engendrer un biais de sélection: solution plus facile mais peu physique ou choix restreint à la communauté d'usage du modèle.

Il est toujours possible d'élaborer une solution analytique, mais parfois au prix de simplification et d'hypothèses trop restrictives. Néanmoins, cette méthode de vérification fait partie du processus qu'un ingénieur en modélisation doit utiliser.

\subsubsection{Vérification informatique}

Pour la vérification, il existe d'autres moyens. La plus fréquente après la comparaison à des solutions analytiques est la comparaison avec d'autres codes de calcul. Cette solution doit être prise avec précaution. On peut avoir beaucoup de codes avec une solution identiquement fausse.

Par contre, c'est un excellent moyen de comparer un code par rapport à d'autres.

Un autre moyen est d'utiliser des valeurs de non-régression. C'est-à-dire qu'on s'assure simplement que le code donne la même solution (peut-être fausse ! ) d'une version à l'autre. Cette vérification <<du pauvre>> est indispensable dans un processus de développement de code. Elle peut être utile à un ingénieur de modélisation en lui permettant de détecter des écarts entre différentes versions d'un logiciel.

Enfin, la plupart des systèmes informatiques donnent accès à des outils de vérification très utiles:
\begin{itemize}
    \item Couverture des zones de codes: les compilateurs modernes permettent de dire quelles zones du code sont appelées lors d'une exécution sur une base de cas-tests;
    \item Suivi des performances: dans le cadre d'une vérification complète, le suivi des performances des cas-tests est un indicateur précieux qui peut parfois donner des indications sur des défauts de programmation;
    \item Portabilité du code: le fait de changer de machine, de système, de compilateur, voire d'options de compilation (par exemple le débrayage des options d'optimisation) est un moyen très efficace de débusquer bugs et erreurs.
\end{itemize}

Récemment, se développe également des outils de propagation des erreurs d'arrondis. Ces erreurs d'arrondis proviennent de la représentation imparfaite des nombres réels en machine (y compris selon l'architecture 32 ou 64 bits), de l'utilisation d'architectures multi-processeurs et d'algorithmes spécifiques.
Ces outils permettent de vérifier la \emph{robustesse} des codes.

\subsubsection{Vérification numérique}

La dernière manière de vérifier un code, est de vérifier sa robustesse numérique. En effet, la plupart des codes reposent sur des théories d'analyse numérique vérifiant certaines hypothèses et assurant certains résultats. On sait, par exemple, que la méthode des éléments finis vous permet d'atteindre une solution aussi proche que l'on veut de la solution analytique si l'on fait tendre la taille des éléments vers zéro. C'est une chose qui doit être vérifiée (y compris dans le cadre de la réalisation d'études). De même, certains modèles ont une dépendance à des paramètres numériques divers. Typiquement, la discrétisation temporelle par des schémas aux différences finis entraîne parfois des critères de stabilité conditionnelle\footnote{C'est un point très important lorsqu'on utilise une certaine classe de codes de calcul en mécanique: tous ceux qui font appel à des schémas de résolution temporelle de type explicite pour lesquels il n'y a pas de vérification directe des équations (dépendance conditionnelle du résultat au pas de temps par rapport au problème).} qu'il convient de tester.

\subsection{La validation}

Contrairement à la vérification, la validation est systématiquement du ressort de l'utilisateur d'un code de calcul. C'est cette expertise qui dira si le code choisi est bien pertinent pour les analyses que l'on vise. La plupart des éditeurs fournissent des éléments de validation généralistes. Pour les outils-métiers spécialisés, cette phase peut être assez bien réalisée par les éditeurs. Pour les logiciels généralistes, beaucoup moins et pour les produits programmables, pas du tout.
La validation est un processus plus informel et plus complexe que la vérification. De plus, il repose souvent sur de l'expertise ou des montages expérimentaux très coûteux.
Malheureusement cette phase de validation est souvent négligée. Elle contribue aussi à déconsidérer la qualité des codes de calcul, voire même la stratégie de modélisation numérique. Lors des phases de sélection des logiciels, la pertinence de la validation à laquelle on soumet le code de calcul est déterminante.
De plus, le choix de la simulation numérique comprend souvent un volet financier non négligeable qui se réduit souvent à dire que l'on fait trop d'essais et qu'ils coûtent trop cher. Dès lors, il est difficile de faire accepter à des décideurs qu'il est nécessaire d'avoir des références expérimentales ou des analyses d'expert très poussées.

Dans certaines industries très réglementées et très surveillées (le nucléaire, la chimie ou l'aéronautique), le processus de validation est quasiment normalisé, sous la houlette d'une agence plus ou moins prescriptive. Dans l'industrie manufacturière classique, la phase de validation est plus légère, souvent réduite à ce que peut fournir l'éditeur et au contrôle de  l'environnement de travail.

La première stratégie possible est d'utiliser des outils-métiers, ou de réduire le périmètre d'utilisation des logiciels généralistes.
La seconde est de disposer d'un dossier de validation, plus ou moins riche, qui comprend résultats expérimentaux, rapports d'experts et analyses passées.

Évidemment, une démarche complète de validation a aussi pour but de restreindre ses ambitions de modélisation. Il ne s'agit pas seulement de reproduire le réel d'une ou plusieurs expériences, mais de comprendre aussi les paramètres et les hypothèses que l'on fait.

\subsection{La certification}

La certification est un processus plus récent, issue de la communauté de l'informatique. L'idée est de valider les compétences des utilisateurs des codes. De manière informelle, cette certification est déjà faite, à la fois par des méthodes de construction de l'expertise, par les procédures d'assurance-qualité (validateur et relecteur) mais aussi par les formations suivies par les ingénieurs (formation initiale et formation continue).
Certaines communautés et certaines entreprises tentent de rationaliser cette notion de certification en organisant des processus très rigoureux, mêlant formation initiale, formation continue et recyclage.

\section{Mise en œuvre d'une simulation numérique}

La mise en œuvre d'une simulation numérique procède d'étapes rigoureuses. Nous proposons ici des éléments pour assurer une qualité contrôlée et contrôlable des résultats obtenus.

\subsection{Hypothèse, erreur ou approximation ?}

Chaque résultat numérique est à analyser à l'aune des erreurs introduites lors des différentes phases. Il convient de préciser un peu le vocabulaire et à faire un tri entre les différentes erreurs du modèle numérique.

La première séparation est la distinction entre erreur volontaire qu'on nommera hypothèse ou approximation et l'erreur involontaire résultant d'une maladresse de l'utilisateur, d'une erreur du code de calcul et d'une hypothèse mal maîtrisée.

Il existe des outils numériques de vérification de certaines erreurs mais la majorité des outils de contrôle sont à la charge de l’utilisateur et surtout de l'organisation mise en place.

Deux concepts minimisent les risques d'erreurs involontaires: la redondance et le contrôle croisé.

On ne peut pas réduire une simulation numérique à un simple calcul. Il n'est pas possible d'assurer la qualité d'un résultat avec un calcul unitaire comme il n'est pas possible d'accepter une démonstration mathématique qui n'est été relue et refaite par différentes personnes.

\subsection{La phase de préparation}

Avant de commencer une simulation numérique et se précipiter dans l'interface graphique pour réaliser un beau maillage, il faut commencer à se poser des questions élémentaires:
\begin{itemize}
    \item Sur quel objet ou quel produit vais-je travailler ?
    \item Quelle physique est en jeu ?
    \item Quels résultats dois-je obtenir ?
    \item De quels moyens je dispose pour contrôler mes résultats ?
\end{itemize}

\subsubsection{L'objet}

Il peut paraître quelque peu superflu que de se poser la question du produit ou de l'objet physique que je veux simuler puisqu'il s'agit en général de l'enjeu de la simulation !
Ce n'est cependant pas si immédiat. Les demandes sont souvent floues et il n'est pas rare que le coté magique de la simulation agisse fasse perdre de vue l'objectif à atteindre.

Par exemple, si le sujet est de savoir comment un véhicule automobile résiste à un choc, il convient de s'interroger sur la finesse de représentation qu'on veut obtenir.
Qu'entend-on par \emph{résiste à un choc} ? S'agit-il de la structure (le châssis ?), de la carrosserie ? De la survie des passagers ? De la protection du réservoir de carburant ?

Qu'entend-on par \emph{choc} ? Sur un autre véhicule ? De quel type ? Sur un mur ? Rigide ? En béton ? En briques ? En torchis ? Un arbre ? Un piéton ? A quelle vitesse ? A l'arrière du véhicule ? En choc frontal ? Latéral ?

Bien entendu, il est rare que le projet de simulation soit si flou, mais il n'est pas inutile de décrire précisément toutes ces questions: ce sont les hypothèses de modélisation. Ne pas les exprimer clairement empêchera l'analyse critique a posteriori.

Il y a une autre raison plus pragmatique à ce questionnement précis: le coût de réalisation de l'étude. Une étude numérique est coûteuse: temps de mise en œuvre, temps de calcul, temps d’analyse, mobilisation de ressources humaines et matérielles. Il n'est pas idiot de se poser la question de la pertinence de cette simulation: est-ce qu'une analyse analytique avec un modèle simplifié, une expérience facile ne serait pas plus pertinente que le modèle numérique ? Après tout, on a construit des cathédrales sans ANSYS et des centrales nucléaires sans ABAQUS.

\subsubsection{La physique}

Cette question est en général plus facile. S'agit-il de mécanique, de thermique, de neutronique ?
Ce qui est plus compliqué est d'affiner la physique. En mécanique, par exemple, on distingue les analyses statiques et les analyses dynamiques. On a même plusieurs niveaux de dynamique: dynamique lente ou dynamique rapide. Ce choix s’accompagne inévitablement par une sélection des codes de calcul qui sont rarement aussi généralistes que ça: il n'existe sur le marché aucun code de calcul à la fois efficace en dynamique lente (et statique) et en dynamique rapide.
La prise en compte d'une ou plusieurs non-linéarités dans le modèle soit être considéré très tôt. Car même si une analyse linéaire est toujours réalisée en première approximation, la simulation d'une ou plusieurs non-linéarités, suivant leur type, restreint le choix des logiciels.

\subsubsection{Les résultats}

Enfin, le dernier niveau de sélection, c'est le type et la précision des résultats à obtenir. Une simulation numérique ne peut pas avoir comme objectif d'avoir des <<résultats aussi précis que possible>>, c'est beaucoup trop flou et déraisonnablement ambitieux.
L'objectif doit être formulé précisément. Par exemple: \emph{il me faut des niveaux de déplacements sur telle partie de la structure, avec une précision de l'ordre du millimètre} ou \emph{le niveau de contrainte maximum pour vérifier la tenue de la structure à la ruine compte tenu de l'information dont je dispose pour établir ce critère de ruine}

Bien entendu, une telle précision sert à sélectionner efficacement le logiciel, mais aussi les professionnels réalisant la simulation. Par exemple faire réaliser un calcul numérique de tenue des bâtiments par des spécialistes en génie civil et non pas des spécialistes de l'emboutissage des tôles.

\subsubsection{Les moyens de contrôle}

On entend ici le contrôle dans son sens le plus général mais aussi dans le cadre des procédures de V\&V déjà évoquées. Une simulation numérique repose sur le choix d'hypothèses maîtrisées et d'un logiciel ayant un bon niveau de V\&V. Mais c'est insuffisant. Il est crucial de disposer d'éléments factuels, chiffrés, d'ordres de grandeur, voire de références voisines (d'autres calculs ou expérimentales) \emph{spécifiques} au problème traité.
Quand on réalise une simulation numérique, on doit être capable de produire un premier résultat grossier par exemple d'origine analytique. Avant de calculer la flexion d'une poutre de géométrie complexe en béton armée, ne peut-on pas tenter un calcul de \emph{coin de table} sur une poutre simplifiée équivalente ? Ceci afin de se prémunir des erreurs de modélisation les plus grossières.

\subsection{La phase de réalisation}

Pour la phase de réalisation, on se propose ici de résumer les différentes étapes et d'y attacher les différentes erreurs (involontaires) et hypothèses liées à chaque étape. On se concentrera sur un problème de mécanique des structure standard (par exemple, la déformation d'une poutre en béton armée pour le dimensionnement d'un bâtiment).

\subsubsection{La géométrie}

La géométrie doit être décrite en termes informatiques: c'est le travail des logiciels de CAO (CAD en anglais). Cette description informatique va donc nécessiter l'usage d'un logiciel spécifique (par exemple Catia) ou d'un pré-processeur intégré au logiciel de simulation. Dans tous les cas, il faut se souvenir que cette étape doit prévoir le passage dans le monde discret final: le maillage.
Bien qu'il existe des standards de description de la géométrie (formats STEP et IGES par exemple) que la plupart des modeleurs géométriques savent lire ou écrire, il est parfois difficiles d'effectuer ces transferts sans avoir besoin de faire des modifications à cause d'un standard mal respecté (souvent volontairement: un éditeur de logiciel de CAO n'a pas intérêt à ce que ses outils soient bien interfacées avec la concurrence !).

Le deuxième point crucial de cette étape est qu'il y a une grande différence entre un modèle informatique de la géométrie crée pour la conception et un modèle géométrique efficace pour la simulation. La première raison est qu'un modèle de conception va servir à des représentations graphiques (par exemple pour le design), ou à vérifier l'encombrement et le passage des pièces, voire à piloter des machines à commande numérique. Sa précision est donc conçue pour ces applications. Le modèle géométrique de conception est très souvent inadapté pour la phase de maillage. Il va donc falloir \emph{adapter} le modèle géométrique au calcul. D’autant plus que la modélisation numérique procède souvent à des hypothèses géométriques:
\begin{itemize}
    \item Utilisations des symétries du modèle;
    \item Utilisation de modèle simplifiés: hypothèse des contraintes planes pour les structures minces, hypothèse des déformations planes, axisymétrie;
    \item Utilisation d’éléments de structures issus de la résistance des matériaux: poutres, plaques, coques, tuyaux, etc.
\end{itemize}

\subsubsection{Le maillage}

Cette phase est généralement la plus difficile et la plus coûteuse dans la préparation d'un modèle numérique.
En effet, même si les logiciels de maillage sont désormais capables de faire des prouesses sur des géométries très complexes, il reste beaucoup de choses qui sont du ressort de l'utilisateur:
\begin{itemize}
    \item Choisir le bon type de maille: triangles, quadrangles, tétraèdres, hexaèdres, approximation linéaire ou quadratique;
    \item Maîtriser la densité des mailles: en mettre plus là où l'on veut un résultat plus précis, limiter la taille finale du modèle;
    \item Contrôler la qualité de maillage et être en mesure de le modifier;
\end{itemize}

Il est très important de disposer d'un mailleur capable de diagnostiquer la qualité et la conformité de ce qu'il produit. Il faut des éléments de bonne qualité, pas de trous, ni de recouvrement. Pour des géométries courbes, l’utilisation de mailles quadratiques est meilleure, mais il faut que le logiciel soit en mesure de suivre la courbure des frontières et qu'il ne se contente pas d'ajouter des nœuds au milieu des arêtes droites !

Pour les problèmes de mécanique des milieux continus, on préférera utiliser des approximations quadratiques, qui ont une meilleure précision (représentation linéaire des contraintes et des déformations). Pour la thermique, le respect du principe du maximum et la minimisation des oscillations numériques parasites incitent à plutôt utiliser des approximations linéaires.

Le choix du maillage est donc autant une hypothèse du modèle (une erreur contrôlée) qu'un risque d'erreur involontaire qu'il convient de minimiser.

\subsubsection{La discrétisation temporelle}

La discrétisation temporelle concerne logiquement en priorité les problèmes de dynamique. Il y a trois exigences:
\begin{itemize}
    \item Pour les schémas temporels avec une stabilité conditionnelle, il faut choisir un pas de temps assurant cette stabilité;
    \item Pour tous les schémas en temps, la précision est également dépendante du pas de temps;
    \item Il faut que les chargements soient correctement représentés. En particulier, par extension du théorème de Shannon, il faut discrétiser au moins deux fois plus finement pour en avoir une représentation correcte.
\end{itemize}
Mais il faut prendre garde également aux problèmes statiques non-linéaires. En effet, pour résoudre ces équations, on procède de manière incrémentale en paramétrant le chargement par un \emph{pseudo}-temps. Or, certains logiciels proposent l'intégration des EDO non-linéaires (comme les lois de comportement) par des méthodes explicites. Il est donc possible d'obtenir des résultats différents suivant la discrétisation en \emph{pseudo}-temps. De même, certains algorithme (contact, frottement ou simplement équilibre non-linéaire) ont aussi une dépendance au pas de temps.

\subsubsection{La modélisation}

On regroupe sous le terme \emph{modélisation} tout ce qui concerne le choix des éléments finis. Par exemple, le choix d'une représentation bi-dimensionnelle (contraintes ou déformations planes), le choix d'éléments de structures (pou\-tres, plaques ou coques). Mais aussi les hypothèses de symétrie et le degré d'approximation (linéaire ou quadratique).


\subsubsection{Les matériaux}

En mécanique, la première approche consiste toujours à considérer le matériau comme étant élastique linéaire. Cette hypothèses est très restrictive et il arrive assez tôt dans le processus qu'on soit obligé de la remettre en cause. Or l'identification et le choix d'un matériau non-linéaire est difficile et délicat. Qui plus est, même si ce choix parait facile, il faut disposer des paramètres matériaux, et donc souvent procéder à une campagne expérimentale d'identification, très coûteuse.
Il faut également savoir assez rapidement si les matériaux utilisés présentent une dépendance à d'autres paramètres, le plus souvent la température, mais aussi la vitesse de déformation (matériaux de type visqueux) ou d'autres paramètres comme le flux neutronique.


\subsubsection{Les conditions limites et les chargements}

Le dernier point de la modélisation concerne les conditions limites et les chargements.
Concernant les conditions limites essentielles (dite aussi conditions cinématiques ou conditions de Dirichlet), il faut prendre garde au fait qu'elles conditionnent aussi la possibilité d'obtenir une solution (problème bien posé). A partir de la connaissance de la technologie réalisant la liaison (par exemple, un point de soudure ou un assemblage boulonné), il faut être capable de choisir de manière judicieuse sa représentation mathématique.
Pour les conditions limites en chargement en force, mis à part le cas trivial de la pesanteur, il reste à évaluer les variations et établir des cas limites \emph{enveloppes}, de ces chargements. Du point de vue numérique, il ne faudra pas oublier que la représentation est \emph{discrète} et non continue. Une force nodale n'a pas de sens physique car il y a toujours une surface non-nulle sur laquelle s'appliquera le chargement. Transposé dans le monde discret, il faudra être très attentif à ne pas provoquer d'effets numériques indésirables comme des concentrations de contrainte.

\subsection{La phase de contrôle}

La phase de contrôle consiste en premier lieu à considérer a posteriori l'ensemble des hypothèses (erreurs volontaires) que l'on commet (voir paragraphes précédents) et à être capable de faire systématiquement une analyse de sensibilité. En particulier, il n'est pas acceptable de produire des résultats sur un seul maillage de la structure, la dépendance des résultats à ce paramètre doit être considéré avec beaucoup d'attentions, quelque que soit la qualité supposée du logiciel employé.
L'autre partie du contrôle est le problème des erreurs involontaires. Il existe pour cela des stratégies d'organisation (contrôles croisés et redondance) mais aussi des réflexes et quelques méthodes plus physiques:
\begin{itemize}
    \item Toujours commencer un modèle numérique par une analyse linéaire simplifiées;
    \item Disposer de calculs analytiques simplifiés pour comparer des résultats globaux;
    \item Pour contrôler la qualité du maillage, penser à <<peser>> la structure (la plupart des logiciels le permettent);
    \item Faire des chargements élémentaires et vérifier le mouvement global de la structure;
    \item Pour les problèmes de précision, penser à changer les unités;
\end{itemize}
