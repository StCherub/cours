
\chapter{La méthode des éléments finis pour la dynamique linéaire}

Dans ce chapitre, nous présentons quelques éléments théoriques pour la
modélisation en dynamique linéaire à l'aide de la
méthode des éléments finis. Nous nous pencherons plus particulièrement
sur l'analyse modale.

\section{Introduction}

Jusqu'à présent, nous n'avons considéré que des problèmes stationnaires dans lesquels les effets d'inertie étaient négligés. Dans ce chapitre, nous allons nous pencher sur le cas plus général (et plus complexe) de la dynamique.

Il y a plusieurs façons de résoudre un problème de dynamique en
mécanique. Selon les hypothèses retenues dans la description du
comportement des structures, on peut distinguer trois grandes classes
de problèmes:

\begin{itemize}
    \item l'élastodynamique qui consiste à résoudre un problème d'élasticité non-stationnaire;
    \item la dynamique des milieux continus non-linéaires dans laquelle le comportement de la structure est non-linéaire (en comportement du matériau ou en cinématique);
    \item la dynamique des corps rigides dans laquelle les éléments de la structure sont considérés comme parfaitement rigides.
\end{itemize}

Dans les trois classes, il est nécessaire de traiter un problème en
temps, ce qui implique d'utiliser des méthodes d'approximation temporelle en plus de l'approximation spatiale.

Dans ce document, nous nous intéressons qu'au problème d'élasto-dynamique (linéaire) pour lequel il existe une technologie de résolution très utilisée: la résolution par superposition modale.

Cette technique consiste tout d'abord à décrire le contenu vibratoire de la structure par une analyse du contenu spectral
du système mécanique. Puis, on utilise ce contenu vibratoire pour
réduire le problème et résoudre ainsi un système discret de dimension
réduite, ce qui permet de traiter les problèmes vibratoires avec des
sollicitations dynamiques à moindre coût et avec une excellente
précision.

\section{Le problème élasto-dynamique}
\subsection{Formulation forte}

Par application du PFD (Principe Fondamental de la Dynamique) sur le
VER, l'équilibre en force nous donne les équations
locales d'équilibre dynamique d'une
structure  $\Omega $, soumise à des forces de volume
$\boldsubformula{f}$, des déplacements imposés
${\boldsubformula{u}}^{D}$ sur une partie de sa frontière  $\Gamma
    _{D}$ et des forces imposées  $\boldsubformula{g}$ sur une partie de sa
$\Gamma _{N}$ frontière:

\begin{equation*}
    \left\{\begin{matrix}\boldsubformula{\text{div}}\boldsubformula{\sigma
        }(\boldsubformula{x},t)+\boldsubformula{f}(t)=\rho
        .\ddot{\boldsubformula{u}}(t)\text{ dans }\Omega \hfill\null
        \\\boldsubformula{u}(\boldsubformula{x},t)={\boldsubformula{u}}^{D}(\boldsubformula{x},t)\text{
            sur }\Gamma _{D}\hfill\null \\\boldsubformula{\sigma
        }(\boldsubformula{x},t).\boldsubformula{n}=\boldsubformula{g}(\boldsubformula{x},t)\text{
            sur }\Gamma _{N}\hfill\null\end{matrix}\right.
\end{equation*}


Par rapport au problème statique, nous avons ajouté les efforts
d'inertie  $\rho .\ddot{\boldsubformula{u}}$ avec
$\rho $ la masse volumique et  $\ddot{\boldsubformula{u}}$
l'accélération. Nous avons supposé que la description
du milieu dans sa configuration initiale utilise le même repère que
dans sa configuration courante. Pour certaines applications, on utilise
plutôt un repère orthonormé lié par un mouvement rigide pour la
configuration courante, ce qui introduit les notions
d'accélération relative,
d'entraînement et complémentaire (ou Coriolis).

Nous avons également négligé les effets visqueux (dépendant de la
vitesse des particules).

Cette équation n'est pas suffisante pour déterminer la
déformée de la structure. comme dans le chapitre 1, on y ajoute donc
deux séries d'équations:

\begin{enumerate}
    \item La première relation lie le tenseur des déformations
          $\boldsubformula{\varepsilon }$ aux déplacements  $\boldsubformula{u}$
          de la structure. C'est la relation cinématique. Dans
          le cadre de l'hypothèse des petites perturbations
          (HPP), cette relation est linéaire et s'écrit:
\end{enumerate}
\begin{equation*}
    \boldsubformula{\varepsilon
    }(\boldsubformula{\mathrm{u}})=\frac{1}{2}.(\nabla
    \boldsubformula{u}+\nabla ^{T}\boldsubformula{u})
\end{equation*}

\begin{enumerate}
    \item La seconde relation lie le tenseur des déformations
          $\boldsubformula{\varepsilon }$ aux contraintes
          $\boldsubformula{\sigma }$. Dans le cas de
          l'élasticité, la relation est linéaire et
          s'écrit:
\end{enumerate}
\begin{equation*}
    \boldsubformula{\varepsilon }=\frac{1}{E}.((1+\nu
    ).\boldsubformula{\sigma }-\nu .\text{tr}(\boldsubformula{\sigma
    }).\boldsubformula{1})
\end{equation*}

Le problème élastodynamqiue (\ref{seq:refText0}) n'est
pas bien posé car la dimension temporelle implique
d'ajouter des conditions initiales:

\begin{equation*}
    \left\{\begin{matrix}\boldsubformula{\text{div}}\boldsubformula{\sigma
        }(\boldsubformula{x},t)+\boldsubformula{f}(t)=\rho
        .\ddot{\boldsubformula{u}}(t)\text{ dans }\Omega \hfill\null
        \\\boldsubformula{u}(\boldsubformula{x},t)={\boldsubformula{u}}^{D}(\boldsubformula{x},t)\text{
            sur }\Gamma _{D}\hfill\null \\\boldsubformula{\sigma
        }(\boldsubformula{x},t).\boldsubformula{n}=\boldsubformula{g}(\boldsubformula{x},t)\text{
            sur }\Gamma _{N}\hfill\null
        \\\boldsubformula{u}(\boldsubformula{x},0)={\boldsubformula{u}}_{0}\text{
            dans }\Omega \hfill\null
        \\\boldsubformula{\dot{u}}(\boldsubformula{x},0)={\boldsubformula{\dot{u}}}_{0}\text{
            dans }\Omega \hfill\null\end{matrix}\right.
\end{equation*}

\subsection{Formulation intégrale faible}

Nous ne refaisons pas tout le déroulement qui permet de trouver la
formulation forte et faible à partir du principe de Galerkin. On se
contente d'écrire la forme intégrale faible (au sens
des éléments finis) de l'équation de
l'élastodynamique, ou principe des travaux virtuels:

\begin{equation*}
    \begin{gathered}\text{Trouver }\boldsubformula{u}\in E^{h,t}\text{ tel
            que }\forall \tilde {{\boldsubformula{u}}}\in E^{\text{\^h}}\hfill
        \\\text{ avec }\underset{\Omega ^{h}}{\int
        }{\boldsubformula{\varepsilon }(\tilde
            {{\boldsubformula{u}}},t)}:\boldsubformula{\sigma
        }(\boldsubformula{u},t).d\Omega ^{h}+\underset{\Omega ^{h}}{\int }\rho
        .\ddot{{\boldsubformula{u}}}(t).\tilde {{\boldsubformula{u}}}.d\Omega
        ^{h}=\underset{\Omega ^{h}}{\int }\boldsubformula{f}.\tilde
        {{\boldsubformula{u}}}.d\Omega ^{h}+\underset{\Gamma _{N}^{h}}{\int
        }\boldsubformula{g}.\tilde {{\boldsubformula{u}}}.d\Gamma ^{h}\hfill
    \end{gathered}
\end{equation*}
Par rapport au cas statique, on utilise un espace des déplacements réels
$E^{h,t}$ qui dépend du temps et on a un terme supplémentaire, le
terme d'inertie  $\underset{\Omega ^{h}}{\int }\rho
    .\ddot{{\boldsubformula{u}}}(t).\tilde
    {{\boldsubformula{u}}}(t).d\Omega ^{h}$.

On obtient donc le système discrétisé linéaire suivant:

\begin{equation*}
    \langle \tilde {u}\rangle .[A].\left\{u\right\}+\langle \tilde
    {u}\rangle .[M].\left\{\ddot{u}\right\}=\langle \tilde {u}\rangle
    .\left\{L\right\}
\end{equation*}
Avec la matrice masse  $[M]$, résultat de l'assemblage
des matrices de masses élémentaires  $[m]$:

\begin{equation*}
    \begin{matrix}[m]\hfill\null & \text{=}\underset{\Omega ^{e}}{\int
        }\left[N\right]^{T}.\rho .\left[N\right].d\Omega ^{e}\hfill\null
    \end{matrix}\hfill
\end{equation*}
Par rapport à la matrice de rigidité, la matrice masse est souvent
approximée par une version diagonale qui concentre la masse sur les
n{\oe}uds. Cette approximation est efficace du point de vue numérique,
sans trop impacter la physique (pour des faibles inerties).


\section{Cas des vibrations libres}
\subsection{Systèmes conservatifs}

Si on considère le problèmes des vibrations libres sans dissipation
d'énergie (sans amortissement), on essaie de résoudre
le système discret suivant:

\begin{equation*}
    [A].\left\{u(t)\right\}+[M].\left\{\ddot{u}(t)\right\}=\left\{0\right\}\ \ \text{avec}\ \ \left\{u\right\}_{t=0}\ \ \text{et}\ \ \left\{\dot{u}\right\}_{t=0}\ \ \text{données}
\end{equation*}

Avec les conditions initiales déjà définies. Ce système correspond à la
réponse d'un système dynamique déplacé de son état
d'équilibre et laissé libre
d'osciller. On applique le principe de séparation des
variables en écrivant:

\begin{equation*}
    \forall t>0\ \ \ \ \left\{u(t)\right\}=p(t).\left\{\phi \right\}
\end{equation*}

En réinjectant (\ref{seq:refText8}) dans (\ref{seq:refText7}), on
obtient:

\begin{equation*}
    p(t).[A].\left\{\phi \right\}+\ddot{p}(t).[M].\left\{\phi
    \right\}=\left\{0\right\}
\end{equation*}
Si les matrices ne sont pas singulières:

\begin{equation*}
    \forall t\ \ \ \ \ \frac{[A]}{[M]}.\left\{\phi
    \right\}=\frac{-{\ddot{p}(t)}}{p(t)}=\lambda
\end{equation*}
Deux cas sont à considérer:

\begin{enumerate}
    \item Si  $\lambda <0$ alors
          $p(t)=\mathit{A.}\text{cosh}\left(\sqrt{\left|{\lambda
                  }\right|}.t\right)+\mathit{B.}\text{sinh}\left(\sqrt{\left|{\lambda
                  }\right|}.t\right)$ qui tend vers l'infini quand le
          temps tend vers l'infini. Le système est donc
          instable;
    \item Si  $\lambda >0$ alors
          $p(t)=\mathit{A.}\text{cos}\left(\sqrt{\left|{\lambda
                  }\right|}.t\right)+\mathit{B.}\text{sin}\left(\sqrt{\left|{\lambda
                  }\right|}.t\right)$ qui oscille autour de zéro. Le système est donc
          stable;
\end{enumerate}
Physiquement, on retient donc la seconde solution car la première
impliquerait la création d'énergie. Si la constante
$\lambda $ existe, elle est nécessairement positive pour que le
problème soit stable. On pose  $\lambda =\omega ^{2}$ et on a donc:

\begin{equation*}
    \left([A]-\omega ^{2}.[M]\right).\left\{\phi \right\}=\left\{0\right\}
\end{equation*}

Ce système est un problème aux valeurs propres, il admet une solution
non triviale en cherchant les zéros du polynôme de degré  $n$ en
$\omega ^{2}$ :

\begin{equation*}
    \mathit{det}\left([A]-\omega ^{2}.[M]\right)=\left\{0\right\}
\end{equation*}
Le problème aux valeurs propres admet  $n$ solutions réelles et
positives en  $\omega ^{2}$ :

\begin{equation*}
    \exists \left(\omega _{j}^{2},\left\{{\phi
    }_{j}\right\}\right)_{j=1,n}\ \ \ \text{tels que}\ \ \ [A].\left\{{\phi
        }_{j}\right\}=\omega _{j}^{2}.[M].\left\{{\phi }_{j}\right\}
\end{equation*}
$\omega _{j}$ est la pulsation propre associée au mode propre
$\left\{{\phi }_{j}\right\}$. Son unité est le  $\text{rad.s}^{-1}$. On
défini également la fréquence propre (en  $\mathit{Hz}$) du mode
$\left\{{\phi }_{j}\right\}$ :

\begin{equation*}
    f_{j}=\frac{\omega _{j}}{2.\pi }
\end{equation*}
Il existe une pulsation propre nulle  $\omega _{j}=0$ seulement si la
matrice  $[A]$ est singulière, ce qui correspond à la présence de modes
rigides dans la structure (des modes à énergie nulle). Le mode
$\left\{{\phi }_{j}\right\}$ représente un état particulier de
déformation du système pour lequel les efforts de rappel élastique
équilibrent les efforts d'inertie,
c'est ce qu'on appelle le phénomène
de résonance. Pour un mode  $\left\{{\phi }_{j}\right\}$ donné, on
définit la motion de masse modale:

\begin{equation*}
    M_{j}=\left\langle {\phi }_{j}\right\rangle .[M].\left\{{\phi
        }_{j}\right\}
\end{equation*}
Et de rigidité modale:

\begin{equation*}
    A_{j}=\left\langle {\phi }_{j}\right\rangle .[A].\left\{{\phi
        }_{j}\right\}
\end{equation*}
Une propriété importante des modes propres est qu'ils
sont orthogonaux entre eux (d'où la dénomination de
mode propre). Considérons deux modes propres distincts  $\left\{{\phi
        }_{j}\right\}$ et  $\left\{{\phi }_{k}\right\}$:

$[A].\left\{{\phi }_{j}\right\}=\omega
    _{j}^{2}.[M].\left\{{\phi }_{j}\right\}$ et  $[A].\left\{{\phi
        }_{k}\right\}=\omega _{k}^{2}.[M].\left\{{\phi }_{k}\right\}$
Si on multiplie la première relation par  $\left\langle {\phi
    }_{k}\right\rangle $ :

\begin{equation*}
    \left\langle {\phi }_{k}\right\rangle .[A].\left\{{\phi
        }_{j}\right\}=\omega _{j}^{2}.\left\langle {\phi }_{k}\right\rangle
    .[M].\left\{{\phi }_{j}\right\}
\end{equation*}

Et la seconde par  $\left\langle {\phi }_{j}\right\rangle $ :

\begin{equation*}
    \left\langle {\phi }_{j}\right\rangle .[A].\left\{{\phi
        }_{k}\right\}=\omega _{k}^{2}.\left\langle {\phi }_{j}\right\rangle
    .[M].\left\{{\phi }_{k}\right\}
\end{equation*}

Les matrices  $[A]$ et  $[M]$ étant symétriques, on a :
$\left\langle {\phi }_{j}\right\rangle
    .[A].\left\{{\phi }_{k}\right\}=\left\langle {\phi }_{k}\right\rangle
    .[A].\left\{{\phi }_{j}\right\}$ et  $\left\langle {\phi
    }_{j}\right\rangle .[M].\left\{{\phi }_{k}\right\}=\left\langle {\phi
    }_{k}\right\rangle .[M].\left\{{\phi }_{j}\right\}$
En utilisant (\ref{seq:refText20}) et en soustrayant
(\ref{seq:refText19}) de (\ref{seq:refText18}), on a :

\begin{equation*}
    \left\langle {\phi }_{j}\right\rangle .[A].\left\{{\phi
        }_{k}\right\}-\left\langle {\phi }_{k}\right\rangle .[A].\left\{{\phi
        }_{j}\right\}=\left(\omega _{j}^{2}-\omega _{k}^{2}\right).\left\langle
    {\phi }_{j}\right\rangle .[M].\left\{{\phi }_{k}\right\}=0
\end{equation*}
On démontre que les modes propres sont orthogonaux :

$\left\langle {\phi }_{j}\right\rangle
    .[A].\left\{{\phi }_{k}\right\}=0$ et  $\left\langle {\phi
    }_{j}\right\rangle .[M].\left\{{\phi }_{k}\right\}=0$
En normalisant les modes et en utilisant leur propriété
d'orthogonalité (les modes étant distincts), on défini
ainsi une base (la base modale) dans laquelle il sera plus facile de
déterminer la réponse du système mécanique.

On appelle matrice modale la matrice  $[\Phi ]_{(n,n)}$ composée de tous
les  $n$ vecteurs propres d'un système de dimension
$n$ :

\begin{equation*}
    [\Phi ]=\left[\left\{{\phi }_{1}\right\}\left\{{\phi
            }_{2}\right\}...\left\{{\phi }_{n}\right\}\right]
\end{equation*}
On définit la matrice de masse modale  $[M_{\Phi }]$ :

\begin{equation*}
    [M_{\Phi }]=\left[\Phi \right]^{T}.[M].\left[\Phi \right]
\end{equation*}
Et la matrice de rigidité modale  $[A_{\Phi }]$ :

\begin{equation*}
    [A_{\Phi }]=\left[\Phi \right]^{T}.[A].\left[\Phi \right]
\end{equation*}
Ce sont deux matrices diagonales. Le vecteur réponse du système
$\left\{\boldsubformula{u}(t)\right\}$ s'écrit comme
combinaisons linéaires sur la base modale :

\begin{equation*}
    \forall t>0\ \ \ \ \left\{u(t)\right\}=\sum
    _{j=1}^{n}{r_{j}(t).\left\{\phi _{j}\right\}}=[\Phi
    ].\left\{r(t)\right\}
\end{equation*}
$r_{j}(t)$ est le vecteur des coordonnées modales,
c'est-à-dire la contribution de chaque mode
$\left\{{\phi }_{j}\right\}$ dans la réponse. On obtient un nouveau
système discret, la formulation modale :

\begin{equation*}
    [A_{\Phi }].\left\{r(t)\right\}+[M_{\Phi
        }].\left\{\ddot{r}(t)\right\}=\left\{0\right\}\ \ \text{avec}\ \ \left\{r\right\}_{t=0}\ \ \text{et}\ \ \left\{\dot{r}\right\}_{t=0}\ \ \text{données}
\end{equation*}
Avec :

\begin{equation*}
    \left\{u(t)\right\}=[\Phi ].\left\{r(t)\right\}
\end{equation*}

Comme les matrices  $[A_{\Phi }]$ et  $[M_{\Phi }]$ sont diagonales, le
système est découplé:

\begin{equation*}
    \forall j=1,n\ \ \ \ A_{j}.r_{j}(t)+M_{j}.\ddot{r}_{j}(t)=0
\end{equation*}
On trouve facilement:

\begin{equation*}
    \forall j=1,n\ \ \ \ r_{j}(t)=r_{j}(0).\text{cos}\left(\omega
    _{j}.t\right)+\frac{\dot{r}_{j}(0)}{\omega _{j}}.\text{sin}\left(\omega
    _{j}.t\right)
\end{equation*}

Pour retrouver la solution dans la base physique, on projette sur la
base modale en utilisant l'équation
(\ref{seq:refText28}). La méthode de superposition modale consiste en
trois opérations:

\begin{enumerate}
    \item Extraire les $n$ modes propres et les  $n$ pulsations propres du
          système (\ref{seq:refText11});
    \item Résoudre les  $n$ équations découplées (\ref{seq:refText30});
    \item Reconstituer la solution physique par superposition modale à
          l'aide de l'équation (\ref{seq:refText28});
\end{enumerate}

Ainsi décrit, cette nouvelle manière de résoudre le problème
d'élastodynamique paraît peu efficace puisque pour
obtenir un système diagonal découplé, nous passons par une opération
numériquement délicate et lourde: le problème aux valeurs propres.

L'intérêt de la superposition modale est de procéder à
une approximation en ne calculant que  $m$ modes propres (avec  $m<n$).
En effet, physiquement, le problème aux valeurs propres, correspond à
la réponse spectrale du système mécanique,
c'est-à-dire à la description des modes de déformation
en fonction de la fréquence des sollicitations. Dès lors, si
l'on dispose d'une information
suffisante sur la nature spectrale des sollicitations, il est possible
de prendre un  $m$ (beaucoup) plus petit que  $n$, ce qui rend la
méthode de superposition modale très efficace. De plus, le problème
étant linéaire, on peut envisager de calculer la réponse du système
pour plusieurs cas de sollicitations, pourvu que la sollicitation
«~enveloppe~» aie un contenu spectral compatible avec
l'analyse des modes propres.


\subsection{Cas des systèmes faiblement dissipatifs}

La prise en compte de l'amortissement réel des
structures mécaniques reste un sujet de recherche actif. Pour les
structures usuelles, la dissipation est faible (tant
qu'on reste dans le domaine élastique), on procède
donc souvent à une approximation très efficace (vérifiée
expérimentalement), qui consiste à écrire que la matrice
d'amortissement C est une matrice diagonale:

\begin{equation*}
    [C_{\Phi }]=\left[\Phi \right]^{T}.[C].\left[\Phi \right]
\end{equation*}
Cette matrice est proportionnelle à la masse  $M_{j}$, à la pulsation
propre  $\omega _{j}$ et un coefficient
d'amortissement critique modale  $\zeta _{j}$ :

\begin{equation*}
    [C_{\Phi }]=\left[\begin{matrix}2.\zeta _{1}.M_{1}.\omega
            _{1} & 0 & 0 \\0&2.\zeta _{2}.M_{2}.\omega
            _{2} & 0     \\0&0&...\end{matrix}\right]
\end{equation*}

Le coefficient  $\zeta _{j}$ n'a pas de sens physique
direct mais peut s'identifier facilement par analyse
modale expérimentale. On obtient un nouveau système discret :

\begin{equation*}
    [A_{\Phi }].\left\{r(t)\right\}+[C_{\Phi
        }].\left\{\dot{r}(t)\right\}+[M_{\Phi
        }].\left\{\ddot{r}(t)\right\}=\left\{0\right\}\ \ \text{avec}\ \ \left\{r\right\}_{t=0}\ \ \text{et}\ \ \left\{\dot{r}\right\}_{t=0}\ \ \text{données}
\end{equation*}

Le système est découplé:

\begin{equation*}
    \forall j=1,n\ \ \ \ \ddot{r}_{j}(t)+2.\zeta _{j}.\omega
    _{j}.\dot{r}_{j}(t)+\omega _{j}^{2}.r_{j}(t)=0
\end{equation*}
Qui est équation différentielle du second ordre. On trouve facilement
(si ! si!):

\begin{equation*}
    \begin{matrix}\forall j=1,n\ \ \ \ r_{j}(t)=\exp \left(-\zeta
        _{j}.\omega
        _{j}.t\right).\left[r_{j}(0).\left(\text{cos}\left({\bar{\omega
                }}_{j}.t\right)+\frac{\zeta _{j}}{\sqrt{1-\zeta
                    _{j}^{2}}}.\text{sin}\left({\bar{\omega
                }}_{j}.t\right)\right)\right]+\text{ } \\\exp \left(-\zeta _{j}.\omega
        _{j}.t\right).\left[\frac{{\dot{r}}_{j}(0)}{\omega
                _{j}}.\text{sin}\left({\bar{\omega }}_{j}.t\right)\right]\end{matrix}
\end{equation*}
Avec  ${\bar{\omega }}_{j}$ la pulsation amortie:

\begin{equation*}
    {\bar{\omega }}_{j}={\omega }_{j}.\sqrt{1-\zeta _{j}^{2}}
\end{equation*}

\section{Cas des vibrations forcées}


Nous allons maintenant étudier le cas où le système dynamique avec
amortissement (modal) discrétisé est soumis à une excitation extérieure
$\left\{F(t)\right\}$ qui dépend du temps :

\begin{equation*}
    [A].\left\{u(t)\right\}+[C].\left\{\dot{u}(t)\right\}+[M].\left\{\ddot{u}(t)\right\}=\left\{F(t)\right\}\ \ \text{avec}\ \ \left\{u\right\}_{t=0}\ \ \text{et}\ \ \left\{\dot{u}\right\}_{t=0}\ \ \text{données}
\end{equation*}

Nous allons considérer deux types d'excitation:

\begin{itemize}
    \item le cas où les sollicitations appliquées sont de type harmonique,
          ce qui conduit à l'analyse fréquentielle ou
          l'analyse harmonique;
    \item le cas où les sollicitations appliquées sont quelconques, ce qui
          conduit à l'analyse temporelle;
\end{itemize}
\begin{enumerate}
    \item \begin{enumerate}
              \item Domaine fréquentiel
          \end{enumerate}
\end{enumerate}

\bigskip

Dans le cas fréquentiel, on écrit que la sollicitation est de type
harmonique et s'écrit sous forme
d'exponentielle complexe:

\begin{equation*}
    \left\{F(t)\right\}=\exp \left(i\omega t\right).\left\{F(\omega
    )\right\}
\end{equation*}
Avec  $\omega $ la pulsation de la sollicitation. La solution générale
de (\ref{seq:refText37}) comporte deux termes:

\begin{equation*}
    \left\{u(t)\right\}=\left\{u_{p}(t)\right\}+\left\{u_{t}(t)\right\}
\end{equation*}
La partie  $\left\{u_{t}(t)\right\}$ est le régime transitoire, qui,
compte tenu de l'amortissement, fini par
s'annuler après quelques oscillations. La partie
$\left\{u_{p}(t)\right\}$ est le régime permanent, qui correspond au
problème des vibrations entretenues par la sollicitation extérieure. Du
fait de la nature de la force appliquée, la réponse permanente est
nécessairement de type harmonique :

\begin{equation*}
    \left\{u(t)\right\}=\exp \left(i\omega t\right).\left\{u(\omega
    )\right\}
\end{equation*}
Donc, la réponse fréquentielle est solution de
l'équation suivante:

\begin{equation*}
    \left[-\omega ^{2}.[M]+i.\omega .[C]+[A]\right].\left\{u(\omega
    )\right\}=\left\{F(\omega )\right\}
\end{equation*}
En appliquant le principe de superposition modale, on retrouve un
système équivalent à (\ref{seq:refText33}), mais exprimé en pulsation:

\begin{equation*}
    \left[-\omega ^{2}.[M_{\Phi }]+i.\omega .[C_{\Phi }]+[A_{\Phi
        }]\right].\left\{r(\omega )\right\}=\left\{F_{\Phi }(\omega )\right\}
\end{equation*}
Avec la matrice de masse modale  $[M_{\Phi }]$, la matrice de rigidité
modale  $[A_{\Phi }]$ et la matrice d'amortissement
modale  $[C_{\Phi }]$. La projection des excitations sur la base modale
$\left\{F_{\Phi }(\omega )\right\}$ s'écrit
simplement:

\begin{equation*}
    \left\{F_{\Phi }(\omega )\right\}=\left[\Phi \right]^{T}.\left\{F(\omega
    )\right\}
\end{equation*}
Le système est devenu diagonal dans la base modale et comporte  $n$
équations découplées correspondant à un oscillateur à un seul degré de
liberté:

\begin{equation*}
    \forall j=1,n\ \ \forall \omega \ \ \ \ \left[-\omega
    ^{2}.M_{j}+i.\omega .C_{j}+K_{j}\right].r_{j}(\omega )=F_{\Phi
    ,j}(\omega )
\end{equation*}
Dont la solution est:

\begin{equation*}
    \forall j=1,n\ \ \ \ r_{j}(\omega )=\frac{F_{\Phi ,j}(\omega
    )}{\left[-\omega ^{2}.M_{j}+i.\omega .C_{j}+K_{j}\right]}
\end{equation*}
On note  $H(\omega )$ et on écrit la fonction de transfert du système:

\begin{equation*}
    \forall j=1,n\ \ \ \ r_{j}(\omega )=\frac{F_{\Phi ,j}(\omega
    )}{\left[-\omega ^{2}.M_{j}+i.\omega .C_{j}+K_{j}\right]}
\end{equation*}

La réponse en fréquence s'obtient par combinaison
linéaire des modes:

\begin{equation*}
    \forall \omega \ \ \ \ \left\{r(\omega )\right\}=\sum
    _{k=1}^{n}{r_{k}(\omega ).\left\{\phi _{j}\right\}}
\end{equation*}
Cette analyse fréquentielle peut aussi s'appliquer sur
toute sollicitation périodique par application de la décomposition en
séries de Fourier. En effet, la sollicitation (sur un degré de liberté)
$F(t)$ peut se décomposer de la manière suivante:

\begin{equation*}
    F(t)=\sum _{n=-\infty }^{n=+\infty }{f_{n}.\exp \left(in\omega
    _{0}t\right)}
\end{equation*}
Avec  $f_{n}$ valant réciproquement :

\begin{equation*}
    f_{n}=\frac{1}{T}.\int _{-T/2}^{+T/2}{F(t).\exp \left(-in\omega
    _{0}t\right).\mathit{dt}}
\end{equation*}
En posant  $T=\frac{2\pi }{\omega _{0}}$.La réponse à une somme
d'excitations harmoniques est la combinaison des
réponses dues à chaque excitation :

\begin{equation*}
    u(t)=\sum _{n=-\infty }^{n=+\infty }{u_{n}.\exp \left(in\omega
    _{0}t\right)}
\end{equation*}
L'amplitude complexe  $u_{n}$
s'obtient directement à l'aide de la
fonction de transfert (\ref{seq:refText46}) :

\begin{equation*}
    \forall n\ \ u_{n}=H(\mathit{n.}\omega _{0}).f_{n}
\end{equation*}
D'où la réponse complète :

\begin{equation*}
    \forall t\ \ u(t)=\sum _{n=-\infty }^{n=+\infty }{H(\mathit{n.}\omega
    _{0}).f_{n}.\exp \left(in\omega _{0}t\right)}
\end{equation*}


\section{Domaine temporel}
\subsection{Solution continue en temps}

La réponse du système à une sollicitation  $\left\{F(t)\right\}$
quelconque (non harmonique) a pour équation :

\begin{equation*}
    [A].\left\{u(t)\right\}+[C].\left\{\dot{u}(t)\right\}+[M].\left\{\ddot{u}(t)\right\}=\left\{F(t)\right\}\ \ \text{avec}\ \ \left\{u\right\}_{t=0}\ \ \text{et}\ \ \left\{\dot{u}\right\}_{t=0}\ \ \text{données}
\end{equation*}
En projetant cette équation sur les modes (superposition modale), on
obtient :

\begin{equation*}
    \forall
    j=1,n\ \ \ \ A_{j}.r_{j}(t)+C_{j}.\dot{r}_{j}(t)+M_{j}.\ddot{r}_{j}(t)=F_{j}(t)\ \text{avec}\ r_{j}(0)\ \text{et}\ \dot{r}_{j}(0)\ \text{données}
\end{equation*}
$F_{j}(t)$ est l'excitation modale temporelle:

\begin{equation*}
    F_{j}(t)=\left\langle \Phi \right\rangle .\left\{F(t)\right\}
\end{equation*}
Pour chaque mode  $j$, la solution a pour expression:

\begin{equation*}
    \begin{gathered}\forall
        j=1,n\ \ \ \ r_{j}(t)=r_{j}(0).M_{j}.\left[{\dot{h}}_{j}(t)+2.\zeta
        _{j}.\omega _{j}.{h}_{j}(t)\right]+{\dot{r}}_{j}(0).M_{j}.h_{j}\\+\int
        _{0}^{t}{F_{j}(\tau ).h_{j}(t-\tau ).d\tau }\end{gathered}
\end{equation*}
L'intégrale  $\int _{0}^{t}{F_{j}(\tau ).h_{j}(t-\tau
    ).d\tau }$ est une intégrale de convolution entre
l'excitation  $F_{j}(t)$ et la fonction de réponse
impulsionnelle unitaire  $h_{j}(t)$ :

\begin{equation*}
    h_{j}(t)=\frac{1}{M_{j}.\omega _{j}}.\exp \left(-\zeta _{j}\omega
    _{j}t\right).\left(\sin {\bar{\omega }}_{j}t\right)
\end{equation*}
C'est l'intégrale de Duhamel. Sa
résolution est non-triviale dans le cas général. Il y a plusieurs
manières de l'évaluer: formule des trapèzes, méthode
des rectangles, règle de Simpson, etc. Toutefois, en pratique, on
préfère résoudre l'équation en temps en approximant
les opérateurs différentiels par des schémas aux différences finies.

\subsection{Solution discrète en temps}

La méthode la plus générale pour résoudre les problèmes de dynamique est
d'approximer les opérateurs différentiels de
l'équation en temps par des schémas aux différences
finies. Il en existe beaucoup, avec leurs avantages et leurs
inconvénients. Certains sont inconditionnellement stable (ne dépendant
pas de la taille du pas de temps), d'autres sont
conditionnellement stable. Certains sont très précis mais très coûteux,
d'autres introduisent de la dissipation «~numérique~»
qui peut être utile pour filtrer le contenu fréquentiel parasite. Dans
ce paragraphe nous présenterons, à titre d'exemple le
schéma $\theta ${}-Wilson, mais il en existe bien
d'autres !

le schéma $\theta ${}-Wilson part de l'hypothèse que
l'accélération est linéaire entre  $t$ et  $t+\theta
    \text{.}\Delta t$~,  $\forall \tau \in \left[0,\theta .\Delta
        t\right]$:

\begin{equation*}
    {\ddot{{u}}}_{t+\Delta t}={\ddot{{u}}}_{t}+\frac{\tau }{\theta
        \text{.}\Delta t}.\left({\ddot{{u}}}_{t+\theta .\Delta
        t}-{\ddot{{u}}}_{t}\right)
\end{equation*}

En intégrant (\ref{seq:refText58}) en fonction de la variable  $t$, on
obtient :

\begin{equation*}
    {\dot{{u}}}_{t+\Delta t}={\dot{{u}}}_{t}+\tau
    .{\ddot{{u}}}_{t}+\frac{\tau ^{2}}{2\theta .\Delta
        t}.\left({\ddot{u}}_{t+\theta .\Delta t}-{\ddot{{u}}}_{t}\right)
\end{equation*}
Puis après une deuxième intégration :

\begin{equation*}
    {u}_{t+\Delta t}={u}_{t}+\tau .{\dot{{u}}}_{t}+\frac{\tau
        ^{2}}{2}.{\ddot{{u}}}_{t}+\frac{\tau ^{3}}{6\theta .\Delta
        t}.\left({\ddot{u}}_{t+\theta .\Delta t}-{\ddot{{u}}}_{t}\right)
\end{equation*}
Si on se place en  $\tau =\theta .\Delta t$, on a finalement:

\begin{equation*}
    {\ddot{{u}}}_{t+\Delta t}={\ddot{{u}}}_{t}+\left({\ddot{{u}}}_{t+\theta
        .\Delta t}-{\ddot{{u}}}_{t}\right)
\end{equation*}
\begin{equation*}
    {\dot{{u}}}_{t+\Delta t}={\dot{{u}}}_{t}+\frac{\theta .\Delta
        t}{2}.\left({\ddot{u}}_{t+\theta .\Delta t}+{\ddot{{u}}}_{t}\right)
\end{equation*}
\begin{equation*}
    {u}_{t+\Delta t}={u}_{t}+\theta .\Delta t.{\dot{{u}}}_{t}+\frac{\theta
    ^{2}.{\Delta t}^{2}}{2}.\left({\ddot{u}}_{t+\theta .\Delta
        t}+{2\ddot{{u}}}_{t}\right)
\end{equation*}

On écrit les équations d'équilibre au temps  $t+\theta
    \text{.}\Delta t$ avec  $\theta \ge 1$ :

\begin{equation*}
    \begin{gathered}[A].\left\{u(t+\theta .\Delta
        t)\right\}+[C].\left\{\dot{u}(t+\theta .\Delta
        t)\right\}+[M].\left\{\ddot{u}(t+\theta .\Delta
        t)\right\}=\\\left\{F(t)\right\}+\theta .\left(\left\{F(t+\theta
        .\Delta t)\right\}-\left\{F(t)\right\}\right)\end{gathered}
\end{equation*}
On a supposé que le chargement extérieur varie linéairement. En
remplaçant les expression des accélérations, vitesses et déplacements
approchés par le schéma (\ref{seq:refText61}), on obtient:

\begin{equation*}
    \begin{gathered}[\tilde {A}].\left(u(t+\theta .\Delta
        t)-u(t)\right)=\theta .\left(\left\{F(t+\theta .\Delta
        t)\right\}-\left\{F(t)\right\}\right)+\\\hfill
        [M].\left(\frac{6}{\theta .\Delta
                t}.\dot{u}(t)+3.\ddot{u}(t)\right)+\\\hfill [C].\left(\frac{\theta
                .\Delta t}{2}.\ddot{u}(t)+3.\dot{u}(t)\right)\end{gathered}
\end{equation*}
Avec la matrice  $[\tilde {A}]$ qui vaut :

\begin{equation*}
    [\tilde {A}]=[A]+\frac{3}{\theta .\Delta t}.[C]+\frac{6}{\theta
    ^{2}.{\Delta t}^{2}}.[M]
\end{equation*}

Ce schéma est stable si  $\theta >1.37$. Il est optimal (stable et
précis) pour  $\theta =1.42$. Toutefois, l'hypothèse
de linéarisation des efforts extérieurs impose le choix
d'un pas de temps conforme à la représentation de ces
efforts.
