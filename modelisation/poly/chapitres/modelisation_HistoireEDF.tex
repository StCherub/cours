\chapter{Petite introduction historique: la simulation en mécanique à EDF}

Extrait de \cite{TraiteMIM}

\section{Un modèle de développement}

La réponse d'EDF R\&D à ses besoins relatifs à la
simulation numérique en mécanique des solides et des structures est le
développement et la mise à disposition de l'ingénierie
du logiciel Code\_Aster qui a donc le double objectif
d'être :

\begin{enumerate}
    \item La plate-forme d'accueil des travaux de recherche
          dans le domaine de la mécanique afin d'assurer leur
          pérennité et leur transfert.
    \item Le logiciel de simulation numérique de l'ingénierie
          pour réaliser les études et analyses sous assurance de la qualité.
\end{enumerate}


Ces deux exigences nous ont conduit à adopter un modèle de dévelop\-pement
«~non-dual\footnote{Cf. le rapport à l'académie des
    technologies «~Enquête sur les frontières de la simulation numérique~»
    de mai 2005 pour une présentation du concept de dévelop\-pement dual [où
            recherche et études opérationnelles sont conduites sur des systèmes
            différents] et des risques associés.}~» et une organisation assez
originale à l'époque dont le paragraphe suivant
rappelle rapidement la genèse.


\section{L'émergence du besoin}

Cette histoire du développement de la méthode des éléments finis à EDF est largement empruntée à l'article de J.R. Lévesque dans le numéro 60 (1998) de la revue Épure consacré à la modélisation mécanique à EDF qui est disponible sur le site code-aster.org.

Les besoins en calcul mécanique des structures ont pris, pour EDF, une
nouvelle dimension avec l'émergence de la méthode des
éléments finis, à la fin des années 1960. L'école
d'été EDF-CEA-INRIA sur les éléments finis en 1971
marquera le premier rendez-vous de la communauté française en calcul
des structures, sous la conduite des Professeurs O. C. Zienkiewicz de
l'Université de Swansea, Fraeijs de Veubeke de
l'Université de Liège et G. Duvaut de
l'Université Pierre et Marie-Curie.

Les années suivantes voient la mise en chantier de plusieurs programmes gé\-né\-ra\-lis\-tes en calcul de structures, dont deux pour les ap\-plications nu\-cléaires : le système CASTEM au CEA et SYSTUS chez Framatome (devenu AREVA).
Au début, les développeurs et les utilisateurs s'échangeaient des
programmes et les modifiaient à
chaque étude. La notion de « code » est apparue au fur et à mesure de l'émancipation des utilisateurs, qui ont
progressivement employé des programmes qu'ils
n'avaient pas écrits.

Pour EDF R\&D (qui s'appelait alors Direction des Études
et Recherches), l'heure n'est pas aux
grands chantiers : c'est le code ASKA développé par le
Professeur Argyris de l'Université de Stuttgart qui est
choisi pour les études 3D. Dans le même temps, différents outils ont
leurs partisans dans des domaines particuliers et surtout dans les
différents services.

La fin de la décennie est marquée par un effort important de
modélisation numérique dans CAVIAR, POUX et CADYRO pour
l'interprétation de la destruction du groupe
turboalternateur 600 MWe\footnote{MWe: Mégawatt électrique, puissance
    électrique produite par un centre de production} de Porcheville.

Cet accident a pour origine la propagation d'une
fissure dans l'arbre du rotor qui a provoqué
l'explosion de la turbine et du groupe alternateur à
coté.


\section{La mécanique nucléaire réclame de nouveaux outils}


Les années 80 débutent avec un coup de semonce : des défauts liés aux procédés de fabrication sont découverts dans les cuves des réacteurs 900 MWe. Les mé\-ca\-ni\-ciens d'EDF convient la DER à
s'intégrer à l'effort de la communauté
« nucléaire » : la maîtrise des matériaux doit être complétée par la
maîtrise de l'approche numérique de
l'analyse mécanique pour évaluer la nocivité des
défauts. Une structure d'accueil, Ali-Baba, voit le
jour et marque le premier effort d'industrialisation
des modèles : un outil intégré permet aux ingénieurs
d'accéder en bidimensionnel aux résultats de travaux
décisifs sur l'approche globale en mécanique de la
rupture et en thermoplasticité. A partir de 1983, les limites des codes
de première génération apparaissent : les innovations du génie logiciel
poussent à utiliser d'autres langages de programmation
et d'autres méthodes de spécification, de développement
ou de qualification. Les leaders du marché se renouvellent, souvent
sous le même nom, le CEA met en chantier CASTEM 2000 ; ASKA 80 hésite
et ne deviendra PERMAS que bien plus tard.

La DER reprend l'idée de codes généralistes autour de
deux architectures différentes : celle de SIVA, en FORTRAN
77\footnote{FORTRAN: FORmula TRANslator, est le premier langage évolué
    de l'histoire de l'informatique. Il a
    été créé en 1956 (!) par John Backus et il est encore largement utilisé
    dans les applications scientifiques, y compris par les plus grands
    logiciels scientifiques commerciaux.}, et celle de COCAINE, plus
prospective. L'effet fédérateur attendu et le partage
des ap\-proches nu\-mériques ne sont pas au rendez-vous. Ces choix ne
rapprochent pas les acteurs : on parle plus de « codotique » que de
mécanique. Dans le même temps, les besoins en analyse mécanique
s'amplifient en changeant de nature. Plusieurs paliers
nucléaires sont construits et fonctionnent : les logiciels de
conception et le savoir-faire de nos fournisseurs ont permis la
réussite de ce grand programme d'équipement. Nos
logiciels ont été sollicités sur quelques points : la prise en compte
du risque sismique sur les bâtiments et des chargements induits sur les
équipements, le comportement d'ensemble des groupes
turbo-alternateurs.

\section{Les enjeux à la fin des années 80}

Les enjeux pour EDF sont alors ceux d'un exploitant qui
doit inscrire sa démarche dans une problématique à constantes de temps
longues, enjeux qui dépassent de loin ceux de
l'ingénierie traditionnelle. Nos études pour les
structures mécaniques s'inscrivent dans deux contextes
particuliers :

\begin{enumerate}
    \item Les composants nucléaires pour lesquels un niveau
          d'exigence, lié à la sûreté, est défini par les
          autorités de contrôle et codifié par des règlements de conception et de
          construction. La responsabilité du dossier chaudière justifie la
          localisation chez Framatome de l'ensemble des données
          de conception, mais également d'une très grande partie
          des méthodologies de calcul. La responsabilité
          d'exploitant nucléaire impose à EDF de posséder les
          moyens et les compétences d'expertise des dossiers,
          notamment pour la détection de « modes communs » entre les études de
          conception et les expertises consécutives au retour
          d'expérience en exploitation ;

    \item Les composants non nucléaires pour lesquels la diversité
          des objets techniques et le contexte industriel sont plus proches des
          autres produits du marché, sauf pour les groupes turboalternateurs et
          les grands ouvrages hydrauliques ; les exigences concernant la
          disponibilité du système électrique justifient également
          l'appropriation des méthodologies
          d'expertise et des outils nécessaires à une bonne
          réactivité.
\end{enumerate}

Dans tous les cas, la maîtrise du coût des contrôles en exploitation sur
des périodes très longues et la gestion des modifications constituent
les enjeux éco\-no\-mi\-ques principaux. Les enjeux techniques sont :
\begin{enumerate}
    \item L'appréciation et la justification des marges de
          sécurité, notamment pour des analyses de conception fondées sur des
          modèles linéaires ;
    \item La qualification, par des modélisations complexes, des méthodes
          simplifiées indispensables à la maîtrise des opérations de maintenance
          des matériels en exploitation ;
    \item La mise à disposition des utilisateurs des logiciels, avec la
          meilleure réactivité possible, des travaux de la R\&D sous une forme
          permettant de bénéficier des progrès rapides des ressources
          informatiques, tout en offrant des garanties sur la gestion des
          dossiers d'étude pour une ou plusieurs décennies.
\end{enumerate}

De fait, c'était bien la généralisation de
l'accès aux modèles non linéaires tridimensionnels qui
était à l'ordre du jour.

\section{Une décision stratégique en 1988}

Le Conseil Scientifique d'EDF est sollicité pour évaluer
l'intérêt d'un nouveau code général
capable de répondre aux interrogations du moment. La première
recommandation est d'inscrire nos actions dans la durée
pour offrir aux différentes équipes une structure
d'accueil pérenne, réutiliser les différents niveaux de
mo\-dé\-li\-sa\-tions et de données accumulées. Face à un grand nombre de
produits du marché, principalement orientés vers la réalisation des
études « classiques de conception » et des modélisations « en boîte
noire », l'objectif prioritaire de cette démarche est
la maîtrise des modèles nécessaires à des études indépendantes. En
corollaire, plusieurs objectifs d'organisation sont
retenus : la maîtrise de la qualité et du coût des études,
l'intégration de l'outil dans la
configuration informatique disponible à chaque instant, la
capitalisation des investissements de R\&D. Les risques liés à
l'engagement du développement d'un code
interne ont été longuement pesés et chacun était conscient du fait que
la démarche nécessitait un effort soutenu pendant au moins dix ans et
une évaluation, à chaque étape, des acquis et des innovations des
logiciels «concurrents». Les travaux sont engagés en janvier 1989 avec
une équipe de projet répartie et un noyau central prépondérant.

Le code se développe rapidement et la première version exploitable par
l'in\-gé\-nie\-rie sera la version 2, sortie en \ 1993. La
version 3 dont le développement s'acheva en 1996
marquera la fin du mode d'organisation initial en
projet avec une équipe en charge de la réalisation d'un
cahier des charges prédéfini. En effet, suite à une analyse de notre
mode de fonctionnement, des activités concernées par le développement
de Code\_Aster et de l'ampleur prise par ce projet, la
décision fut prise à compter de cette date, d'un
nouveau mode d'organisation en réseau qui
distingue\footnote{\ Ce mode de développement, dans lequel
    n'existe pas les notions de cahier des charges ni de
    cycle en V (spécification, développement, validation, qualification)
    est aujourd'hui considéré par beaucoup comme le mode
    d'organisation le plus efficace en matière de
    développement logiciel (par la rigueur qu'il impose et
    la mutualisation des développements qu'il permet et qui
    se traduit, sur la durée, par une réduction des coûts). Il a toutefois
    une particularité «~moins favorable~» qui est la moindre visibilité
    globale (ou à minima l'absence de visibilité naturelle)
    sur ce que sera la prochaine version du logiciel et à quels grands
    enjeux mobilisateurs et communicants elle répondra. } :
\begin{enumerate}
    \item Une équipe centrale ; responsable de la cohérence des travaux menés
          au sein de la plate-forme de développement, des développements à
          caractères génériques et de la ré-ingéniérie permanente du
          code\footnote{\ Ce travail essentiel pour assurer la pérennité et la
              développabilité du code se traduit par la récriture de plus du quart de
              Code\_Aster à chaque version.} mais également de la version en
          exploitation au sein des différentes directions d'EDF
          (R\&D, Ingénierie,{\dots}) et de leurs sous traitants ;
    \item Des projets applicatifs (une quinzaine) qui, dans le cadre de leur
          travaux de R\&D, sont responsable de la décision
          d'engager tels ou tels travaux (et de leurs
          financements) et viennent développer et capitaliser dans le code de
          nouvelles possibilités de modélisation.
\end{enumerate}

Ce mode de développement s'accompagne par ailleurs de
plusieurs caractéristiques que l'on retrouve comme
carac\-té\-ris\-ti\-ques d'organisation du développement de
logiciels libres :
\begin{enumerate}
    \item La mise à disposition permanente à l'ensemble de la
          communauté des contributeurs au développement du code (\~{} 60
          personnes par version) de l'ensemble des sources du
          logiciel (de sa documentation) et des procédures de restitution des
          développements effectués ;
    \item L'absence de cahier des charges et plus généralement
          de cycle en V ; le versionnement du logiciel ne pouvant être
          qu'asynchrone vis-à-vis du phasage des différents
          projets contributeurs à son développement ;
    \item Et enfin une forte proportion (environ un tiers) des utilisateurs
          finaux du logiciel qui sont également, à différents degrés
          d'engagement, contributeurs de son développement.
\end{enumerate}

L'achèvement de la version 4 en 1998, marquera
l'engagement de la première expérience de diffusion
externe du code, en tant qu'exécutable, commercialisé
par trois concessionnaires. L'expérience
s'avérera peu concluante : le code manquait
d'un environnement intégré en pré et post-traitement
pour être réellement attractif pour un public
d'utilisateur, et la non ouverture au développement ne
permettait pas de valoriser son attrait potentiel pour des activités de
recherche et développement de nouvelles modélisations. Ce retour
d'expérience sera à la base de la décision de diffusion
en tant que logiciel libre (et gratuit) qui sera mise en oeuvre en
2001.

Le cycle de développement de la version 5 sera marquée par
l'engagement du projet Aster-Expert destiné à doter
Code\_Aster d'une interface de mise en données et
d'une plate-forme de modeleur/mailleur et post
traitement commune à Samcef-Design sur la base des technologies
produites par MatraDataVision. Malheureusement, en raison des difficultés survenues à cette
entreprise, le projet ne pourra être mené à son terme et seule la
réalisation d'un éditeur de commandes graphique
«~Eficas~» propre à Code\_Aster sera réalisé pour la diffusion de la
version 5. C'est la réalisation de cet éditeur
intelligent qui nous incitera à utiliser le langage Python pour la
définition des catalogue de commandes et par suite à étendre
l'usage de Python pour la réécriture du superviseur
d'exécution qui sera engagée avec le développement de
la version 6.

Le cycle de développement de la version 6 sera également marqué, en
octobre 2001, par la décision de diffuser Code\_Aster en licence GPL
sur laquelle nous reviendrons au paragraphe suivant.

En partie grâce à Python et à la multiplication des utilisations locales
de Code\_Aster, le cycle de développement de la version 7 verra
l'apparition d'un nouvel outil pour
gérer les profils de calculs sur une architecture multi-plateformes
(ASTK), puis le post-processeur Stanley pour faciliter le post
traitement des calculs et assurer la liaison avec des outils de
visualisation. Enfin, la version 7.4 mise en exploitation fin 2004
dispose d'une double qualification sur le serveur de
calcul central mais également sur plateforme linux.

Le cycle de développement de la version 8 est marqué par
l'engagement de la convergence entre Code\_Aster et la
plate forme Salomé afin d'offrir à
l'ensemble de nos utilisateurs un environnement
d'étude intégré de la géométrie à la visualisation en
passant par les phases de maillage, calculs et post-traitement ainsi
que la capacité à réaliser facilement des applicatifs métiers et des
schémas de couplage multiphysiques par intégration
d'autres solveurs que Code\_Aster.

En outre, grâce à la vitalité de la communauté des utilisateurs et
développeurs d'aster libre, cette version sera la
première à intégrer des fonctionnalités issues de la communauté du
libre (loi de comportement hyperélastique\footnote{\ Ce développement
    est le résultat du travail de Touraya Baranger, maître de conférences à
    l'université Lyon 1. }, liaison glissière pour le
contact\footnote{\ Un développement réalisé par Martin Guiton,
    chercheur à l'Institut Français du Pétrole (IFP).}, loi
parabole rectangle pour le Génie Civil\footnote{\ Ce développement a
    été réalisé par la société Necs.}).

La version 12 est la dernière étape de la normalisation des processus de développement avec l'adoption d'outils de forge standard (Mercurial) faisant disparaître l'historique AGLA.

\section{Code\_Aster et Salomé}

Code\_Aster est un solveur, un progiciel de calcul scientifique
résolvant les équations aux dérivées partielles de la mécanique, de
l'acoustique et de la thermique, en linéaire et en
non-linéaire, en stationnaire et en non-stationnaire. Le logiciel
dispose de plus de 80 éléments finis différents et de plus de 200 lois
de comportement pour les matériaux. Il est également à la pointe de la
R\&D en mécanique numérique (contact/frottement, applications
multi-échelles, parallélisme, méthode XFEM, etc.). Ce logiciel est
utilisé par plus de 200 personnes, ingénieurs de recherche, ingénieur
d'études pour l'ingénierie
d'EDF et ses prestataire. Depuis 2001, il est diffusé
en tant que code libre sous licence GPL (voir
http://www.code-aster.org).

Salomé est une plateforme modulaire d'accueil pour les
logiciels de calcul de tous les domaines de la physique (mécanique des
solides, mécaniques des fluides, thermique, neutronique,
électromagnétisme, etc.). Ce logiciel est également diffusé sous
licence libre.
