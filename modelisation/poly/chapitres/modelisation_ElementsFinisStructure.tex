\chapter{Éléments finis pour le calcul des structures}

Dans ce chapitre, nous présentons rapidement la modélisation des
éléments finis dits «~de structure~» (sous-entendus «~structure
mince~») qui sont issus de la théorie de la résistance des matériaux.

\section{Introduction}

Nouus avons présenté la méthode des
éléments finis appliqués aux équations de l'élasticité
linéaire. Il existe d'autres manières de procéder à
des calculs de mécanique élastique, la plus fréquente étant
d'utiliser la théorie développée en résistance des
matériaux.

La théorie de la résistance des matériaux a été développée
simultanément (ou presque) que la théorie de
l'élasticité. Le principe consiste à décrire le
comportement mécanique (déformation, contraintes) de pièces de
constructions mécaniques très fréquentes (poutres, barres, colonnes,
planchers,...) utilisés en particulier dans le cas de la construction
des bâtiments et à tirer partie de leurs particularités, comme leur
topologie.

L'idée est de pouvoir résoudre les problèmes issus
de la conception des structures. En effet, dans le cas général, le
principal fondamental de la statique pour les corps rigides ne permet
pas de résoudre le cas des systèmes hyperstatiques (ou surcontraints)
c'est-à-dire que le problème comprend plus
d'inconnues que d'équations.

On a donc introduit des équations supplémentaires qui décrivent la
manière dont se déforme les corps, en abandonnant
l'hypothèse trop forte de rigidité parfaite des
structures. En pratique, les ingénieurs et les architectes veulent à la
fois pouvoir dimensionner les ouvrages (et donc calculer la résistance
des matériaux qu'ils emploient) et accéder à des
informations plus précises, comme les déformations (flèches des
poutres).

C'est à cet effet qu'a été
développé la théorie de la résistance des matériaux que
l'on va écrire de manière succincte dans ce document.

\section{Éléments de résistance des matériaux}
\subsection{Hypothèse géométrique: les structures minces}

On abordera deux types de structures minces dans ce document:
\begin{itemize}
   \item les poutres (ou barres, ou fils) sont des structures dont deux
         dimensions sont largement inférieures à la troisième. Cette dernière
         étant appelée fibre moyenne. Une poutre est décrite par une section
         qu'on l'on extrude le long d'un chemin (la fibre moyenne).
   \item Les plaques et les coques dont des structures dont une dimension
         (l'épaisseur) est largement inférieure aux deux
         autres. Plus précisément, une plaque est une surface plane et une
         coque décrit une surface courbe.
\end{itemize}
Les anglo-saxons utilisent fréquemment la notion de dimension non-entière
de l'espace pour ces structures. Ainsi une poutre est
une structure 1D plongée dans l'espace global 3D. On
parle alors de dimension 1.5D. Une coque est une structure 2D plongée
dans l'espace global 3D, on parle donc d'espace de dimension 2.5D.

\subsection{Hypothèses cinématiques}

Les éléments de structures utilisent les propriétés géométriques
remarquables (effet de poutre ou effet de plaque) et décrivent le
comportement mécanique en intégrant une description spéciale de la
cinématique. Deux grands types d'hypothèses décrivent le comportement d'une section:
\begin{enumerate}
   \item Dans le cas de l'hypothèse Navier/Bernoulli/Kirchhoff, une section droite reste plane et normale à la fibre après déformation.
   \item Dans le cas de l'hypothèse Timoshenko/Reisnner/Mindlin, une section droite reste plane mais pas normale à la fibre après déformation.
\end{enumerate}
La première hypothèse est valable en petites perturbations (faibles rotations) et pour les structures très minces.
Ces deux modèles font l'hypothèse fondamentale que la section droite ne change pas de longueur (inextensibilité), ce qui correspond au fait que l'épaisseur ne varie pas.
Il existe des hypothèses cinémariques plus générales, qui ne fait pas l'hypothèse d'inextensibité des sections droites, comme les milieux de Cosserat.

\begin{figure}[!ht]
   \centering
   \includegraphics[scale=1.0]{./chapitres/images/PoutreEuler}
\end{figure}

La seconde hypothèse, qui ne néglige plus le cisaillement transverse, est valable pour les structures plus épaisses.

\begin{figure}[!ht]
   \centering
   \includegraphics[scale=1.0]{./chapitres/images/PoutreTimoshenko.pdf}
\end{figure}

\subsection{Hypothèses de comportement}

Le comportement sera décrit comme élastique et réversible. Par ailleurs la structure étant mince, on peut négligger la contrainte normale $\sigma_{zz}$ par rapport aux autres. On fait donc l'hypothèse des contraintes planes pour le comportement.

\subsection{Hypothèse de Barré de Saint-Venant}

Les résultats obtenus en résistance des matériaux ne s'appliquent valablement qu'à une
distance suffisante de la zone d'application des
efforts et des conditions limites cinématiques. La RdM est une
description du comportement global mécanique, les contraintes obtenues
ne sont pas valables dans ces zones critiques.

Cette hypothèse restreint le domaine d'application de
la théorie. Elle est très efficace pour avoir accès aux efforts de
liaisons (obtenus par application du principe fondamental de la
statique) et à la déformation globale de la structure. Par contre, elle
est inutilisable pour avoir une description fine des contraintes.

Moyennant l'hypothèse de Saint Venant, il est possible
de déduire les équations de la résistance des matériaux à partir de la
théorie de l'élasticité tridimensionnelle appliquée
sur la géométrie simplifiée.

\section{Les poutres}

Nous commençons par écrire les équations décrivant le comportement
des poutres pour les sollicitations les plus fréquentes:
traction-compression, flexion et torsion. Puis nous écrirons le PTV et
décrirons l'élément fini utilisé.

\subsection{Traction-compression}

La traction-compression est le mouvement de translation sur
l'axe longitudinal de la poutre paramétré par l'abscisse curviligne $s$. On considère un
segment de longueur  $dx$ soumis à un effort axial $N$ interne et une force extérieure $f_{ext}$ par unité de longueur.
\begin{figure}[!ht]
   \centering
   \includegraphics[scale=0.7]{./chapitres/images/Traction}
\end{figure}
La poutre a une section $S$ et elle est constituée d'un matériau de masse volumique  $\rho$ et de module d'Young $E$. Le principe fondamental de la statique permet d'écrire :
\begin{equation}
   \begin{matrix}-N(x)+N(x+\mathit{dx})+\int
      _{x}^{x+\mathit{dx}}{f_{\mathit{ext}}}(s).\mathit{ds}=0\end{matrix}
\end{equation}
Donc :
\begin{equation}
   \frac{N(x+\mathit{dx})-N(x)}{\mathit{dx}}+\frac{1}{\mathit{dx}}\int
   _{x}^{x+\mathit{dx}}f_{\mathit{ext}}(s).\mathit{ds}=0
\end{equation}
En passant à la limite quand  $dx \rightarrow 0$, on obtient :
\begin{equation}
   \frac{dN(x)} dx+f_{ext}=0
\end{equation}
Pour la loi de comportement, on utilise la loi de Hooke et l'hypothèse que la poutre est constituée de fibres longitudinales travaillant uniquement en traction-compression pour exprimer l'effort axial par :
\begin{equation}
   N(x)=\mathit{ES}.\frac{\partial u}{\partial x}
\end{equation}
On obtient après simplification par $dx$ :
\begin{equation}
   \frac{\partial }{\partial x}\left(\mathit{ES}.\frac{\partial u}{\partial x}\right)+f_{ext}=0
\end{equation}
Cette équation représente l'équilibre local au premier ordre d'une poutre, pour un mouvement de traction-compression.

\subsection{Flexion simple}

La flexion est le mouvement de translation et de rotation autour
d'un axe perpendiculaire à l'axe longitudinal de la poutre. Il y a donc deux mouvements de flexion possible: autour de  $\mathit{Oy}$ et autour de  $\mathit{Oz}$. On décrit l'équation de flexion dans le plan $(O,x,z)$, l'extension au plan  $(O,x,y)$ est immédiate.
\begin{figure}[!ht]
   \centering
   \includegraphics[scale=0.7]{./chapitres/images/Flexion}
\end{figure}
La translation suivant l'axe  $\mathit{Oz}$ est notée
$w$ et la rotation autour de  $\mathit{Oy}$ est notée  $\theta _{y}$.
On considère un segment de longueur  $dx$
soumis à l'effort tranchant  $V_{z}$, le moment de flexion  $M_{y}$, un effort externe $t_{z_{ext}}$ réparti uniformément par unité
de longueur, et un couple externe $m_{y_{ext}}$ réparti uniformément par unité de longueur.
L'équilibre local des forces et des moments (sur la section d'abscisse  $x+\mathit{dx}$) donne pour les forces :
\begin{equation}
   -V_{z}(x)+V_{z}\left( x+dx \right)+\int_{x}^{x+dx} t_{z_{ext}} ds=0
\end{equation}
Et pour les moments:
\begin{equation}
   -M_{y}(x)+M_{y}\left(x+dx\right)+\int
   _{x}^{x+dx}{m_{y_{ext}}.\mathit{ds}}-\int
   _{x}^{x+dx}{V_{z}\left(x\right).\mathit{ds}}=0
\end{equation}
On néglige les termes en  $dx^{2}$ (on considère une théorie linéarisée). En passant à la limite quand  $dx$ tend vers 0, on obtient :
\begin{equation}
   \frac{\partial V_{z}}{\partial
      x}+t_{z_{ext}}=0
\end{equation}
Et:
\begin{equation}
   \frac{\partial M_{y}}{\partial x}-V_{z}+m_{y_{ext}}=0
\end{equation}
On note que l'effort uniformément réparti $t_{z_{ext}}$ produit un terme qui est du second ordre dans l'équilibre des moments et est ainsi
négligé. On introduit ensuite les relations de comportement de la
résistance des matériaux. Pour le comportement en moment:
\begin{equation}
   M_{y}=\mathit{EI}_{y}\frac{\partial \theta _{y}}{\partial x}
\end{equation}
Avec $I_{y}$ est le moment d'inertie de la section:
\begin{equation}
   I_{y}=\int _{S} z^{2}.dS
\end{equation}
Pour le comportement en effort tranchant, on a:
\begin{equation}
   V_{z}=k_{z}{\mathit{SG}}\left(\frac{\partial w}{\partial x}+\theta _{y}\right)
\end{equation}
Cette expression de  $V_{z}$ est due à Timoshenko où $k_{z}$ est le coefficient de cisaillement dans la direction z et  $G$ est le module élastique de cisaillement tel que:
\begin{equation}
   G=\frac{E}{2(1+\nu )}
\end{equation}
En conséquence, on aboutit aux deux équations couplées en $w$ et $\theta_{y}$ pour la flexion dans le plan $(O,x,z)$.
\begin{equation}
   \frac{\partial }{\partial x}\left[k_{z}\mathit{SG}\left(\frac{\partial
         w}{\partial x}+\theta
      _{y}\right)\right]+t_{z_{ext}}=0
\end{equation}
Et pour les moments:
\begin{equation}
   \frac{\partial }{\partial x} \left[  \mathit{EI}_{y} \frac{\partial \theta_{y}}{\partial x} \right ] - k_{z}\mathit{SG} \left[ \frac{\partial w}{\partial x}+\theta _{y} \right ] + m_{y_{ext}}=0
\end{equation}
Lorsque la poutre est uniforme, c'est-à-dire que la section et le matériau sont constants sur l'axe longitudinal, les équations se réduisent de la manière suivante:
\begin{equation}
   k_{z}\mathit{SG}\left[\frac{\partial^{2}w}{{\partial x}^{2}}+\frac{\partial \theta _{y}}{\partial x}\right]+t_{z_{ext}}=0
   \label{FlexionTranchant}
\end{equation}
Et pour les moments:
\begin{equation}
   {\mathit{EI}}_{y}\frac{{\partial }^{2}\theta_{y}}{{\partial x}^{2}}-k_{z}\mathit{SG} \left[ \frac{\partial w}{\partial x}+\theta _{y} \right ] + m_{y_{ext}}=0
   \label{FlexionMoment}
\end{equation}
Pour réduire le système à une seule équation en $w$, on doit commencer par dériver une seule fois par rapport à l'abscisse $x$ l'équation d'équilibre des moments ~\eqref{FlexionMoment}:
\begin{equation}
   {\mathit{EI}}_{y}\frac{{\partial }^{3}\theta_{y}}{{\partial
            x}^{3}}-k_{z}\mathit{SG} \left[ \frac{\partial^{2}w}{\partial x^{2}}+\frac{{\partial }\theta _{y}}{\partial x}\right]=0
   \label{FlexionMomentDerivee}
\end{equation}
On constatera que cette manipulation élimine la présence du terme issu d'un couple extérieur $m_{y_{ext}}$ uniformément réparti (qui ne dépend donc pas de $x$). Ensuite, l'équation ~\eqref{FlexionTranchant} peut se mettre sous la forme suivante:
\begin{equation}
   \frac{\partial \theta _{y}}{\partial x}=-{\frac{\partial ^{2}w}{\partial x^{2}}}-\frac{1}{k_{z}{\mathit{SG}}t_{z_{ext}}}
   \label{FlexionTranchantDerivee}
\end{equation}

En injectant ~\eqref{FlexionTranchantDerivee} dans~\eqref{FlexionMoment} on obtient une équation différentielle du quatrième ordre:
\begin{equation}
   {\mathit{EI}}_{y}.\frac{\partial ^{4}w}{\partial x^{4}}-t_{z_{ext}}=0
\end{equation}
Le modèle de poutre de Timoshenko (\texttt{POU\_D\_T} dans Code\_Aster), prend en compte l'ensemble de ces termes, en particulier ceux qui sont
relatifs à l'effort tranchant. On peut donc modéliser des poutres d'élancement faible. Le modèle de poutre d'Euler (\texttt{POU\_D\_E} dans Code\_Aster) est une simplification puisque les déformations en effort tranchant sont négligées. Cette hypothèse est justifiée dans le cas
d'une poutre d'élancement suffisamment grand. C'est l'effort tranchant qui provoque la rotation des sections droites par rapport à l'axe neutre. Négliger cet effet revient ainsi à écrire que $V_{z}=0$ ce qui amène:
\begin{equation}
   \theta _{y}=-{\frac{\partial w}{\partial x}}
\end{equation}
Pour ce qui concerne la flexion dans le plan  $(O,x,y)$, la même démarche conduit pour la poutre uniforme de Timoshenko à :
\begin{equation}
   {\mathit{EI}}_{z}\frac{\partial^{4}v}{\partial x^{4}}-t_{y_{ext}}=0
\end{equation}

\subsection{La torsion libre de Saint-Venant}

La torsion est le mouvement de rotation autour de l'axe longitudinal de la poutre. On suppose ici que le centre de gravité est confondu avec le centre de rotation (de torsion), et on néglige le gauchissement de la section. Le cas de l'excentricité du centre de torsion par rapport au
centre de gravité ne sera pas traité.
\begin{figure}[!ht]
   \centering
   \includegraphics[scale=0.7]{./chapitres/images/Torsion}
\end{figure}
On considère un segment de longueur $\mathit{dx}$ mis en rotation sous l'action d'un moment $M_{x}$ interne et d'un couple extérieur  $\Gamma _{x}$ par unité de longueur. Le segment est tourné d'un angle  $\theta _{x}$ par rapport à la position non déformée. Nous avons ainsi, pour une poutre de section circulaire:
\begin{equation}
   -M_{x}(x)+M_{x}\left(x+dx\right)+\int_{x}^{x+dx}{}\Gamma_{x}\left(s\right).\mathit{ds}=0
\end{equation}
Comme pour la traction, on obtient après division par $dx$ et passage à la limite :
\begin{equation}
   \frac{{d}M_{x}}{dx}+\Gamma_{x}=0
\end{equation}
On introduit la loi de comportement en torsion:
\begin{equation}
   M_{x}=GI_{p}\frac{{\partial }\theta _{x}}{\partial x}
\end{equation}
Où $G$ est le module de Coulomb (ou module de cisaillement) et $I_{p}$ le moment géométrique polaire par rapport au centre de gravité de la section. Nous obtenons alors l'expression :
\begin{equation}
   \frac{\partial }{\partial x}\left(GI_{p}\frac{\partial
      {\theta }_{x}}{\partial x}\right){ +}\Gamma_{x}=0
\end{equation}
Qui représente l'équilibre local au premier ordre d'un segment de poutre de section circulaire pour un mouvement de torsion.

Pour tenir compte du gauchissement tout en restant dans l'hypothèse de torsion libre, dans le cas des sections non circulaires on est conduit à remplacer le moment $I_{p}$ par une constante de torsion  $C$ (inférieure à  $I_{p}$) dans l'équation de torsion. Lorsque le centre de gravité de la section n'est pas le centre de rotation, cette expression n'est pas valable et les mouvements de torsion et de flexion sont couplés.

\subsection{Énergies}
Dans cette section, nous allons écrire toutes les énergies. L'énergie interne de déformation en traction s'écrit :
\begin{equation}
   E_{int}^{N}=\frac{1}{2}\int_{o}^{l}{\mathit{ES}}\left(\frac{\partial{\mathrm{u}}}{\partial x}\right)^{2} {dx}
\end{equation}
L'énergie interne de déformation en torsion libre s'écrit :
\begin{equation}
   E_{int}^{T}=\frac{1}{2}\int_{o}^{l}\mathit{GI}_{p}\left(\frac{\partial \theta _{x}}{\partial t}\right)^{2} {dx}
\end{equation}
L'énergie potentielle interne pour la flexion simple autour de l'axe  $\mathit{Oy}$ vaut :
\begin{equation}
   E_{int}^{Fy}=\frac{1}{2}\int_{o}^{l}\mathit{EI}_{y}\left(\frac{{\partial }\theta_{y}}{\partial x}\right)^{2}{dx}+\frac{1}{2}\int_{o}^{l}k_{z}{\mathit{SG}}\left(\frac{\partial w}{\partial x}+\theta _{y}\right)^{2}dx
\end{equation}
L'énergie potentielle interne pour la flexion simple autour de l'axe  $\mathit{Oz}$ vaut :
\begin{equation}
   E_{int}^{Fz}=\frac{1}{2}\int_{o}^{l}\mathit{EI}_{z}\left(\frac{{\partial }\theta_{z}}{\partial x}\right)^{2}{dx}+\frac{1}{2}\int_{o}^{l}k_{y}{\mathit{SG}}\left(\frac{\partial v}{\partial x}-\theta _{z}\right)^{2}dx
\end{equation}
On a le travail de la force externe en traction donné par :
\begin{equation}
   E_{ext}^{N}=\int_{o}^{l}{f_{ext}} {u} {dx}
\end{equation}
Le travail du couple extérieur pour la torsion:
\begin{equation}
   E_{ext}^{T}=\int_{o}^{l}{\Gamma_{x}} \theta_{x} dx
\end{equation}
Le travail du couple extérieur pour la flexion simple autour de
l'axe $\mathit{Oy}$ vaut~:
\begin{equation}
   E_{ext}^{Fy}=\int_{o}^{l}{t_{z_{ext}}} w dx+\int_{o}^{l}{m_{y_{ext}}} \theta_{y} dx
\end{equation}
Le travail du couple extérieur pour la flexion simple autour de
l'axe  $\mathit{Oz}$ vaut~:
\begin{equation}
   E_{ext}^{Fz}=\int_{o}^{l}{t_{y_{ext}}} v dx+\int_{o}^{l}{m_{z_{ext}}} \theta_{z} dx
\end{equation}

\subsection{Cinématique}

Dans le cas le plus général de la théorie des poutres (modèle de Timoshenko), les sections droites restent planes mais pas normales à la fibre neutre. On se place toujours dans le cadre de la torsion libre de Saint-Venant. On suppose que le matériau reste matériellement homogène et isotrope et que la section a une symétrie géométrique de centre $O$ (point où passe l'axe neutre de la poutre), ce qui permet de confondre le centre de flexion/torsion avec le centre géométrique $O$. Avec ces hypothèses, les déplacements de la section s'écrivent~:
\begin{equation}
   \left\{\begin{matrix}u_{q}\\v_{q}\\w_{q}\end{matrix}\right\}=\left\{\begin{matrix}u\\0\\0\end{matrix}\right\}+\left\{\begin{matrix}\mathit{z.}{\theta}_{y}\\0\\w\end{matrix}\right\}+\left\{\begin{matrix}-\mathit{y.}{\theta}_{z}\\v\\0\end{matrix}\right\}+\left\{\begin{matrix}0\\-\mathit{z.}{\theta}_{x}\\\mathit{y.}{\theta }_{x}\end{matrix}\right\}
\end{equation}
La première colonne représente l'effet de membrane (traction) sur le déplacement, les deux suivantes l'effet des deux flexions sur le déplacement et la dernière, l'effet de la torsion (libre). A partie de cette cinématique, les déformations linéaires de membrane valent~:
\begin{equation}
   {\varepsilon }_{\mathit{xx}}={u}_{,x}+\mathit{z.}{\theta}_{y,x}-\mathit{y.}{\theta }_{z,x}
\end{equation}
Les déformations linéaires de cisaillement~:
\begin{equation}
   2{\varepsilon }_{\mathit{xy}}={v}_{,x}-{\theta }_{z}
\end{equation}
Et~:
\begin{equation}
   2{\varepsilon }_{\mathit{xz}}={w}_{,x}+{\theta }_{y}
\end{equation}
Toutes les autres déformations sont nulles. Les déformations virtuelles sont de la même forme.

\subsection{Expression du PTV pour la poutre}

On rappelle l'expression du PTV:
\begin{equation}
   \begin{gathered}
      \text{Trouver }\mathbf{u}\in E^{h} \text { tel que }\forall \tilde {\mathbf{u}}\in E^{\tilde{h}} \text{ avec } \\ \underset{\Omega^{h}}{\int }{\mathbf{\varepsilon}(\tilde {{\mathbf{u}}})}:\mathbf{\sigma
      }(\mathbf{u}).d\Omega ^{h}+\underset{\Omega^{h}}{\int
      }\mathbf{f}.\tilde {{\mathbf{u}}}.d\Omega
      ^{h}+\underset{\Gamma_{N}^{h}}{\int }\mathbf{g}.\tilde
      {{\mathbf{u}}}.d\Gamma^{h}=0\hfill
   \end{gathered}
\end{equation}
Dans le cas d'une poutre de Timoshenko, la section reste plane, les contraintes sont planes et donc $\sigma_{yy}$, $\sigma_{zz}$ et $\sigma_{yz}$ sont nuls. Le PTV pour les forces externes réparties vaut~:
\begin{equation}
   W_{{ext}}=\int _{o}^{l}\left[f_{x}.\tilde
   {u}+t_{z_{\mathit{ext}}}.\tilde {w}+t_{y_{\mathit{ext}}}.\tilde
   {v}+m_{y_{ext}}.{\tilde {\theta
   }}_{y}+m_{z_{ext}}.{\tilde {\theta
   }}_{z}+\Gamma _{x}.{\tilde {\theta
   }}_{x}\right].dx
\end{equation}
On pourrait aussi y ajouter les efforts et les moments concentrés. Par souci de légèreté, nous ne les écrirons pas. On rappelle l'expression de la loi de comportement (élastique):
\begin{equation}
   \sigma _{\mathit{xx}}=E.\varepsilon _{\mathit{xx}}
\end{equation}
\begin{equation}
   \sigma _{\mathit{xy}}=2G.\varepsilon _{\mathit{xy}}
\end{equation}
\begin{equation}
   \sigma _{\mathit{xz}}=2G.\varepsilon _{\mathit{xz}}
\end{equation}
Nous supposons les matériaux homogènes sur la section et constants sur la poutre et on découple les effets de traction, de flexion et de torsion, si bien qu'on obtient finalement, dans l'ordre, le travail des forces internes de traction/compression, de cisaillement suivant  $y$, de cisaillement suivant  $z$, de torsion autour de  $x$, de flexion autour de  $y$ et de flexion autour de  $z$:
\begin{equation}
   W_{{int}}^{N}=\int _{o}^{l}{{\tilde
         {u}}_{,x}.\mathit{ES}}.{u}_{,x}.\mathit{dx}
\end{equation}
\begin{equation}
   W_{{int}}^{\mathit{Cy}}=\int _{o}^{l}{\left({\tilde
      {v}}_{,x}-{\tilde {\theta
      }}_{z}\right).k_{y}.\mathit{GS}}.\left({v}_{,x}-{\theta
   }_{z}\right).\mathit{dx}
\end{equation}
\begin{equation}
   W_{{int}}^{\mathit{Cz}}=\int _{o}^{l}{\left({\tilde
      {w}}_{,x}+{\tilde {\theta
      }}_{y}\right).k_{z}.\mathit{GS}}.\left({w}_{,x}+{\theta
   }_{y}\right).\mathit{dx}
\end{equation}
\begin{equation}
   W_{{int}}^{T}=\int _{o}^{l}{{\tilde {\theta
         }}_{x,x}.\mathit{GI}_{p}}.{\theta }_{x,x}.\mathit{dx}
\end{equation}
\begin{equation}
   W_{{int}}^{\mathit{Fy}}=\int _{o}^{l}{{\tilde {\theta
         }}_{y,x}.\mathit{EI}_{y}}.{\theta }_{y,x}.\mathit{dx}
\end{equation}
\begin{equation}
   W_{{int}}^{\mathit{Fz}}=\int _{o}^{l}{{\tilde {\theta
         }}_{z,x}.\mathit{EI}_{z}}.{\theta }_{z,x}.\mathit{dx}
\end{equation}

\subsection{Élément fini de poutre}

On décrit ici l'élément de poutre droite, selon le modèle d'Euler (\texttt{POU\_D\_E}) ou de Timoshenko (\texttt{POU\_D\_T}). Une poutre est donc un segment droit à deux nœuds qui comporte six degrés de liberté au total:  $u$,  $v$,  $w$ (les déplacements aux nœuds) et  ${\theta }_{x}$,  ${\theta }_{y}$ et  ${\theta }_{z}$ (les rotations aux nœuds). Tout comme dans le cas de l'élasticité, il convient de renseigner les propriétés matériau $E$ et $\nu$ de la poutre mais aussi les caractéristiques géométriques de la section qui permettront d'évaluer la surface $S$, les moments d'inerties  $I_{y}$,  $I_{z}$ et  $I_{p}$, ainsi que les coefficients de correction de cisaillement  $k_{y}$ et  $k_{z}$.
Dans Code\_Aster, ces propriétés sont affectées via la commande \texttt{AFFE\_CARA\_ELEM}. En post-traitement, on récupère déplacements et rotations, ainsi que les contraintes mais aussi ce qu'on appelle les efforts généralisés.
Nous allons illustrer le calcul des matrices élémentaires de rigidité en considérant le travail des efforts intérieurs de traction qui s'écrit~:
\begin{equation}
   W_{{int}}^{N}=\int _{o}^{l}{{\tilde
         {u}}_{,x}.\mathit{ES}}.{u}_{,x}.\mathit{dx}
\end{equation}
Pour la discrétisation, on considère donc un segment à deux nœuds (avec les inconnues nodales $u_1$ et $u_2$ ), de longueur $L$ et on utilise une fonction linéaire pour l'approximation du déplacement suivant $x$~:

\begin{equation}
   u(x) = u_1 \left( 1 - \frac{x}{L} \right) + u_2 \left( \frac{x}{L} \right)
\end{equation}

En dérivant les deux fonctions de forme, on obtient la matrice de rigidité suivante~:
\begin{equation}
   k^N = { \frac{ES}{L} }
   \begin{bmatrix}
      1  & -1 \\
      -1 & 1
   \end{bmatrix}
\end{equation}
C'est une manière de faire strictement identique au cas de l'élasticité. La différence vient des termes faisant intervenir les rotations. Considérons donc le cas de la flexion~:
\begin{equation}
   W_{{int}}^{\mathit{Fy}}=\int _{o}^{l}{{\tilde {\theta
         }}_{y,x}.\mathit{EI}_{y}}.{\theta }_{y,x}.\mathit{dx}
\end{equation}
Dans le cas des poutres d'Euler, on a $\theta _{y}=-w_{,x}$, on pourrait donc utiliser une approximation quadratique des déplacements (pour que la dérivée seconde soit au moins constante sur l'élément). Mais cela voudrait dire qu'on devrait employer des segments à trois nœuds et des intégrations numériques plus riches. Qui plus est, cette manière de faire ne serait pas compatible avec une formulation de type Timoshenko. La solution consiste à ajouter les rotations comme inconnues nodales et des fonctions de forme de type Hermite. Nous partons de l'expression du PTV pour le mouvement de flexion simple dans le plan $Oxy$ qui prend en compte le cisaillement~:
\begin{equation}
   W_{int}^{y}=\int _{o}^{l}{\left({\tilde {w}}_{,x}+{\tilde {\theta
      }}_{y}\right).k_{z}.\mathit{GS}}.\left({w}_{,x}+{\theta}_{y}\right).\mathit{dx}+\int _{o}^{l}{{\tilde {\theta}}_{y,x}.\mathit{EI}_{y}}.{\theta }_{y,x}.\mathit{dx}
\end{equation}
On a les fonctions de forme suivante. Pour les déplacements $w$~:
\begin{equation}
   \xi_1 (x) =  {\frac{1}{1 + \phi_y}} \left[
      2 \left(\frac{x}{L} \right)^{3}-
      3 \left(\frac{x}{L} \right)^{2}-
      \phi_y \left( \frac{x}{L} \right) \left( 1 + \phi_y \right)
      \right]
\end{equation}
\begin{equation}
   \xi_2 (x) =  {\frac{L}{1 + \phi_y}} \left[
   - \left( \frac{x}{L} \right)^{3}+
   \frac{4+\phi_y}{2} \left(\frac{x}{L} \right)^{2}-
   {\frac{2 + \phi_y}{2}} \left( \frac{x}{L}\right)
   \right]
\end{equation}
\begin{equation}
   \xi_3 (x) =  {\frac{1}{1 + \phi_y}} \left[
      -2 \left(\frac{x}{L} \right)^{3}+
      3 \left(\frac{x}{L}\right)^{2}+
      \phi_y \left( \frac{x}{L} \right)
      \right]
\end{equation}
\begin{equation}
   \xi_4 (x) =  {\frac{L}{1 + \phi_y}} \left[
   - \left(\frac{x}{L} \right)^{3} +
   {\frac{2 - \phi_y}{2}} \left(\frac{x}{L} \right)^{2}-
   {\frac{\phi_y}{2}} {\frac{x}{L}}
   \right]
\end{equation}
Et pour les rotations~:
\begin{equation}
   \xi_5 (x) =  {\frac{6}{{ L (1 + \phi_y) }}}
   \frac{x}{L}
   \left[ 1 - {\frac{x}{L}} \right]
\end{equation}
\begin{equation}
   \xi_6 (x) =  ...
\end{equation}
\begin{equation}
   \xi_7 (x) =  ...
\end{equation}
\begin{equation}
   \xi_8 (x) =  ...
\end{equation}
On obtient une formulation non-isoparamétrique puisque la géométrie est toujours approchée par des fonctions de forme linéaire standards (éléments à deux nœuds) mais que les inconnues utilisent une autre approximation (polynômes d'Hermite).
