%
% ==========================================================================================
% Package pour la mécanique
% ==========================================================================================
%
% Mickaël Abbas - 2022
%
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{meca_package}

% Packages nécessaires
\RequirePackage{ifthen}
\RequirePackage{ifmtarg}
\RequirePackage{xspace}
\RequirePackage{math_package}
\makeatletter

% =================================================
% ==== Raccourcis pour la terminologie
% =================================================

% Mécanique des milieux continus
\newcommand*{\MMC}{\textsc{MMC}\xspace}

% Volume élémentaire représentatif
\newcommand*{\VER}{\textsc{VER}\xspace}

% Principe fondamental de la dynamique
\newcommand*{\PFD}{\textsc{PFD}\xspace}

% Principe fondamental de la statique
\newcommand*{\PFS}{\textsc{PFS}\xspace}

% Principe d'action-réaction
\newcommand*{\PAR}{\textsc{PAR}\xspace}

% Modèle de Kirchhoff-Love
\newcommand*{\KL}{\textsc{K-L}\xspace}

% Modèle de Reissner-Mindlin
\newcommand*{\RM}{\textsc{R-M}\xspace}

% Principe des Puissances Virtuelles
\newcommand*{\PPV}{\textsc{PPV}\xspace}

% Principe des Travaux Virtuels
\newcommand*{\PTV}{\textsc{PTV}\xspace}

% Hypothèse des Petites Perturbations
\newcommand*{\HPP}{\textsc{HPP}\xspace}

% Simulation numérique du soudage
\newcommand*{\SNS}{\textsc{SNS}\xspace}

% Cinématiquement admissible
\newcommand*{\CA}{\textsc{CA}\xspace}

% =================================================
% ==== Solide et domaine
% =================================================

% Corps solide
\newcommand*{\solide}{\mathcal{B}}

% Domaine courant
\newcommand*{\domain}{\Domain^{t}}

% Frontières initiales pour Neumann, pour Dirichlet
\newcommand*{\BoundN}{\Bound_{\textrm{N}}}
\newcommand*{\BoundD}{\Bound_{\textrm{D}}}

% Frontières courantes pour Neumann, pour Dirichlet
\newcommand*{\bound}{\Bound^{t}}
\newcommand*{\boundN}{\BoundN^{t}}
\newcommand*{\boundD}{\BoundD^{t}}

% Partie d'un domaine et de sa frontière (courant)
\newcommand*{\domainPart}{\domain_{\mathcal{P}}}
\newcommand*{\boundDomainPart}{\partial \domainPart}

% Idem mais indexés par le solide s
\newcommand*{\Domains}{\Domain_s}
\newcommand*{\Bounds}{\partial \Domains}
\newcommand*{\BoundNs}{\Bound_{s,\textrm{N}}}
\newcommand*{\BoundDs}{\Bound_{s,\textrm{D}}}
\newcommand*{\bounds}{\Bounds^{t}}
\newcommand*{\boundNs}{\BoundNs^{t}}
\newcommand*{\boundDs}{\BoundDs^{t}}

% =================================================
% ==== Géométrie
% =================================================

% Bases locales courantes
\newcommand*{\tangent} {\vecteur{t}}
\newcommand*{\normal} {\vecteur{n}}

% =================================================
% ==== Cinématique
% =================================================

% Point quelconque
\newcommand*{\point} {M}
\newcommand*{\pointSolide} {M_s}

% Fonctions de transformation entre 0 et t
\newcommand*{\funcTransfor}{\vecteur{\varphi}^t}
\newcommand*{\funcTransforInverse}{\inverse{\funcTransfor}}
\newcommand*{\funcTransforTranspose}{\transpose{\funcTransfor}}
\newcommand*{\funcTransforHistory}{\vecteur{\varphi}^\tau}

% Fonctions de transformation rigide
\newcommand*{\funcTransforRigi}{\vecteur{\varphi}^{t}_{RM}}

% Espace des déplacements de corps rigide
\newcommand*{\spaceCorpsRigide}{\bm{V}_{RM}}

% Vecteurs infinitésimaux (configurations initiale et actuelle)
\newcommand*{\dX} {\vecteur{dX}}
\newcommand*{\dx} {\vecteur{dx}}
\newcommand*{\dY} {\vecteur{dY}}
\newcommand*{\dy} {\vecteur{dy}}

% Surfaces infinitésimales (configurations initiale et actuelle)
\newcommand*{\dS} {dS}
\newcommand*{\ds} {ds}

% Volumes infinitésimaux (configurations initiale et actuelle)
\newcommand*{\dV} {dV}
\newcommand*{\dv} {dv}

% Vecteurs positions (configurations initiale et actuelle)
\newcommand*{\Position} {\vecteur{X}}
\newcommand*{\position} {\vecteur{x}}

% Espace vectoriel des vitesses
\newcommand*{\spaceVite}{\mathcal{V}}

% Vecteurs déplacement, vitesse, accélération
\newcommand*{\Disp} {\vecteur{u}}
\newcommand*{\Vite} {{\dot {\Disp}}}
\newcommand*{\Acce} {{\ddot {\Disp}}}

% Déplacement imposé sur la frontière
\newcommand*{\DispBoundary} {\Disp_D}

% Gradient de transformation
\newcommand*{\Fdef}{\matrice{F}}

% Vitesses de transformation
\newcommand*{\Ldef}{\matrice{L}}
\newcommand*{\viteDefo}{\matrice{d}}
\newcommand*{\viteRota}{\matrice{w}}

% Elongations principales
\newcommand*{\elongations}{\vecteur{\lambda}}

% Déformations (petites)
\newcommand*{\strain}{\matrice{\varepsilon}}
\newcommand*{\strainElas}{\matrice{\varepsilon}^e}
\newcommand*{\strainPlas}{\matrice{\varepsilon}^p}

% Déformations de Green-Lagrange
\newcommand*{\EGL}{\matrice{E}}

% Déformations de cauchy-Green (droite)
\newcommand*{\ECGDroit}{\matrice{C}}

% Déformations de cauchy-Green (gauche)
\newcommand*{\ECGGauche}{\matrice{b}}

% Déformations logarithmiques
\newcommand*{\Elog}{\matrice{E}^{ln}}
\newcommand*{\ElogElas}{\matrice{E}^{ln,e}}
\newcommand*{\ElogPlas}{\matrice{E}^{ln,p}}

% =================================================
% ==== Sthénique (efforts et contraintes)
% =================================================

% Espace vectoriel des forces
\newcommand*{\spaceForce}{\mathcal{F}}

% Forces
\newcommand*{\force} {\vecteur{f}}
\newcommand*{\surfacicForce} {\vecteur{g}}
\newcommand*{\volumicForce} {\vecteur{f}}

% Contraintes (Cauchy)
\newcommand*{\stressComposante}{\sigma}
\newcommand*{\stress}{\matrice{\stressComposante}}
\newcommand*{\stressNormal}{\vecteur{\stressComposante}_n}
\newcommand*{\stressTangent}{\vecteur{\stressComposante}_t}
\newcommand*{\stressNormalNormal}{\stressComposante_{nn}}

% Contraintes (Log)
\newcommand*{\stressComposanteLog}{T}
\newcommand*{\stressLog}{\matrice{\stressComposanteLog}}

% Contraintes (PK1)
\newcommand*{\stressComposantePK}{P}
\newcommand*{\stressPK}{\matrice{\stressComposantePK}}

% Contraintes (PK2)
\newcommand*{\stressComposantePKTwo}{S}
\newcommand*{\stressPKTwo}{\matrice{\stressComposantePKTwo}}

% Contraintes (Kirchhoff)
\newcommand*{\stressComposanteKirchhoff}{\tau}
\newcommand*{\stressKirchhoff}{\matrice{\stressComposanteKirchhoff}}

% =================================================
% ==== Dynamique
% =================================================

% Masse infinitésimale
\newcommand*{\dm} {dm}

% Densité initiale
\newcommand*{\Density} {\rho}

% Densité actuelle
\newcommand*{\density} {\rho^{t}}

% =================================================
% ==== Lois de comportement
% =================================================

% Comportement: fonctionnelle globale
\newcommand*{\fonctionnelleComportement}{\mathcal{F}}

% Tenseur d'élasticité de Hooke
\newcommand*{\tenseurHooke}{\tenseur{4}{A}}

% Matrice de Hooke
\newcommand*{\matriceHooke}{\matrice{A}}

% =================================================
% ==== Thermodynamique
% =================================================

% Température
\newcommand*{\temperature}{\theta}

% Énergie libre (Helmholtz)
\newcommand*{\enerFree}{\psi}

% Énergie de déformation
\newcommand*{\enerStrain}{\Psi}

% Entropie
\newcommand*{\entropy}{\mathcal{S}}
\newcommand*{\entropySpec}{s}

% Chaleurs
\newcommand*{\surfacicHeat}{\vecteur{q}}
\newcommand*{\volumicHeat}{r}

% Dissipation
\newcommand*{\diss}{\mathcal{D}}
\newcommand*{\dissTher}{\diss_{t}}
\newcommand*{\dissIntr}{\diss_{i}}


% Variables internes
\newcommand*{\variInte}{\alpha}


% =================================================
% ==== Puissance
% =================================================

% Puissance
\newcommand*{\puis}{\mathcal{P}}

% Densité de puissance
\newcommand*{\puisDens}{p}

% Forme bilinéaire de puissance
\newcommand*{\puisBilinForm}[2]{\langle #1 , #2 \rangle_{\puis}}

% Puissance de la force donnée par #1
\newcommand*{\puisForce}[1]{\puis_{#1}}

% Puissance des efforts extérieurs
\newcommand*{\puisExte}{\puis_{e}}

% Puissance des efforts intérieurs
\newcommand*{\puisInte}{\puis_{i}}

% Densité de puissance des efforts intérieurs
\newcommand*{\puisDensInte}{\puisDens_{i}}

% Puissance des efforts d'inertie
\newcommand*{\puisIner}{\puis_{a}}

% =================================================
% ==== Énergies
% =================================================


% Énergie mécanique avec #1 le type de l'énergie
\newcommand*{\ener}[1]{\mathcal{E}^{#1}}

% Énergie totale
\newcommand*{\enerTotale}{\ener{t}}

% Énergie interne
\newcommand*{\enerInterne}{\ener{i}}

% Énergie cinétique
\newcommand*{\enerCinetique}{\ener{cin}}

% Chaleur
\newcommand*{\chaleur}{\mathcal{Q}}

% Énergie mécanique spécifique (= par unité de volume) avec #1 le type de l'énergie
\newcommand*{\enerSpec}[1]{e^{#1}}

% Énergie interne spécifique
\newcommand*{\enerInterneSpec}{\enerSpec{i}}

% =================================================
% ==== Travail
% =================================================

% Travail de la force donnée par #1
\newcommand*{\trav}[1]{\mathcal{W}_{#1}}

% Travail des efforts extérieurs
\newcommand*{\travExte}{\trav{{e}}}

% Travail des efforts intérieurs
\newcommand*{\travInte}{\trav{{i}}}

% ==================================================
% ==== PPV ET PTV
% ==================================================

% Quantités virtuelles
\newcommand*{\DispVirt} {\widehat{\vecteur{u}}}
\newcommand*{\ViteVirt} {\widehat{{\dot {\Disp}}}}
\newcommand*{\ViteVirtRigi}{\ViteVirt^{RM}}
\newcommand*{\viteDefoVirt}{\widehat{\viteDefo}}
\newcommand*{\viteRotaVirt}{\widehat{\viteRota}}

% Puissances virtuelles
\newcommand*{\puisVirtInte}{\widehat{\puisInte}}
\newcommand*{\puisVirtExte}{\widehat{\puisExte}}
\newcommand*{\puisVirtIner}{\widehat{\puisIner}}
\newcommand*{\puisDensVirtInte}{\widehat{\puisDensInte}}

\newcommand*{\puisDensVirtInteInit}{\widehat{\puisDens_{i,0}}}

% ==================================================
% ==== Plaques
% ==================================================

% Corps solide comme plaque
\newcommand*{\solidePlaque}{\mathcal{B}}

% Domaines
\newcommand*{\DomainSurface}{\omega}
\newcommand*{\DomainPlaque}{\Psi}

% Repérage des surfaces inférieure, supérieure et latérale
\newcommand*{\plaqueSup}{{}^{+}}
\newcommand*{\plaqueInf}{{}^{-}}
\newcommand*{\plaqueLat}{{}^{l}}

% Frontières
\newcommand*{\BoundDomainSurface}{\partial \DomainSurface}
\newcommand*{\BoundDomainPlaque}{\partial \DomainPlaque}
\newcommand*{\BoundDomainPlaqueSup}{{\partial \DomainPlaque}\plaqueSup}
\newcommand*{\BoundDomainPlaqueInf}{{\partial \DomainPlaque}\plaqueInf}
\newcommand*{\BoundDomainPlaqueLat}{{\partial \DomainPlaque}\plaqueLat}

% Points sur la surface et dans la plaque
\newcommand*{\pointSurface} {P}
\newcommand*{\pointSup} {Q}
\newcommand*{\pointInf} {R}

% Segment (fibre normale)
\newcommand*{\Fibre} {\vecteur{F}_{\pointSurface\pointSup}}
\newcommand*{\fibre} {\vecteur{f}_{\pointSurface\pointSup}}
\newcommand*{\fibreVect} {\vecteur{fibre}}
\newcommand*{\fibreRota} {\matrice{R}}
\newcommand*{\fibreDila} {\alpha}

% Coordonnées sur la surface
\newcommand*{\coorSurfComposante}{\xi}
\newcommand*{\coorSurf}{\vecteur{\coorSurfComposante}}
\newcommand*{\coorEpaisComposante}{\xi_3}

% Épaisseur
\newcommand*{\epais}{h}


% Fin du package
\makeatother
\endinput
