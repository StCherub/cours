\section{Méthodes d'approximation}

\begin{frame}
    \frametitle{Méthodes d'approximation}
    \begin{problem}
    Considérons l'équation de Laplace-Poisson avec une condition de Dirichlet homogène:
    \begin{equation}
        \left\{ \begin{matrix} -\Delta u = f \text{ dans }\Omega \\ u =0\text{ sur } \partial \Omega  \end{matrix} \right.
    \end{equation}
    \end{problem}
\end{frame}


\begin{frame}
    \frametitle{Méthodes d'approximation}
    \begin{block}{Objectif: }
        Nous cherchons la solution sous la forme
        d'une combinaison linéaire de fonctions \og bien choisies \fg{}.
        Cette solution approchée sera notée {${\tilde{u}}({x})$:}
        \begin{equation}
            {\tilde{u}}({x})=\sum_{i=1}^{N}a_{i}.\phi_{i}({x})
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Méthodes d'approximation}
    L'utilisation de cette approximation mène à une solution approchée
    du système qui quantifiera donc l'erreur commise:
    \begin{equation}
        f + \Delta {\tilde{u}} =\varepsilon\neq0
    \end{equation}
    \begin{block}{Méthode: }
        Calculer les
        coordonnées généralisées $a_{i}$ et choisir de manière intelligente
        les éléments de la base $\phi_{i}({x})$.
    \end{block}
\end{frame}


\begin{frame}
    \frametitle{Méthodes d'approximation}
    Il existe deux grandes catégories d'approximation:
    \begin{itemize}
        \item Les formulations globales (ou variationnelles) pour lesquelles l'approximation
              se base sur une formulation variationnelle, c'est-à-dire une fonctionnelle que l'on cherchera à minimiser;
        \item Les formulations locales qui utiliseront une condition construite
              directement sur l'expression du problème aux limites;
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Approximation variationnelle: la méthode de Ritz}
    \begin{block}{Méthode: }
        Trouver une fonctionnelle  $\pi$  à minimiser (typiquement une énergie) sous forme d'une
        intégrale sur le domaine $\Omega$:
        \begin{equation}
            \pi({u})=\pi\left({u},\frac{\partial{u}}{\partial{x}},\cdots\right)=\int_{\Omega}F\left({u},\frac{\partial{u}}{\partial{x}},\cdots\right).d\Omega
        \end{equation}
    \end{block}
    \begin{block}{Résultat: }
        Avec l'approximation ${\tilde{u}}$, on obtient une fonctionnelle approchée
        $\tilde{\pi}({\tilde{u}})$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Approximation variationnelle: la méthode de Ritz}
    \begin{block}{Résolution: }
        Le problème algébrique résultant
        de la recherche du minimum de cette fonctionnelle, et donc du fait
        que ses dérivées par rapport aux coordonnées généralisées $a_{i}$
        s'annulent, soit:
        \begin{equation}
            \frac{\partial\tilde{\pi}({\tilde{u}})}{\partial a_{i}}=0 \rightarrow \left[ K \right] \lbrace a_i \rbrace = \lbrace r \rbrace
        \end{equation}
    \end{block}
\end{frame}


\begin{frame}
    \frametitle{Approximation variationnelle: la méthode de Ritz}
    \begin{block}{Limites de cette méthode: }
        \begin{itemize}
            \item Il faut trouver une fonctionnelle (ce peut-être le PTV en mécanique)  $\pi$
            \item Il faut choisir la base $\phi_{i}({x})$
            \item Il faut trouver une approximation valable sur tout le domaine $\Omega$
            \item La matrice $\left[ K \right]$ obtenue est pleine
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Approximation variationnelle: la méthode des résidus pondérés}
    \begin{block}{Méthode: }
        L'idée est de construire une fonctionnelle sous une forme intégrale en multipliant l'EDP par une fonction de pondération ${\mathrm{P}}({u})$:

        \begin{equation}
            \begin{gathered}\text{Trouver }{u}\in E_{u}\text{ tel que }\forall{P}\in E_{P}\\
                \text{ avec }W=\underset{\Omega}{\int}\left( \Delta u + f \right).{\mathrm{P}}({u}){\mathrm{.}}d\Omega=0
            \end{gathered}
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Approximation variationnelle: la méthode des résidus pondérés}
    \begin{block}{Choix des espaces: }
        Les fonctions de pondération ${\mathrm{P}}({u})$
        appartiennent à un ensemble de fonctions $E_{P}$. \\
        La solution ${u}$ appartient à l'espace $E_{u}$ des fonctions
        <<suffisamment>> régulières:
        \begin{equation}
            E_{u}=\{{u}\text{ régulière}\textrm{ telle que }C_{u}\left({u}\right)=0\text{ sur }\partial \Omega\}
        \end{equation}
        \begin{equation}
            E_{P}=\{{u}\text{ régulière}\textrm{ telle que }{u}=0\text{ sur }\partial \Omega\}
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Approximation variationnelle: la méthode des résidus pondérés}
    \begin{alertblock}{Choix des espaces: }
        Les conditions limites de Dirichlet $C_{u}\left({u}\right)=0$ sont donc directement prises en compte grâce au choix adéquat des espaces d'approximation.
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Approximation variationnelle: la méthode des résidus pondérés}
    \begin{block}{Variantes: }
        \begin{itemize}
            \item Si la fonction $P({u})$ est une distribution de Dirac, on
                  obtient la méthode de collocation par points;
            \item Si la fonction $P({u})$ est constante sur des sous-domaines,
                  on obtient la méthode de collocation par sous-domaines;
            \item Si les fonctions de pondération ${\mathrm{P}}({u})$
                  utilisent les mêmes fonctions de forme $\phi_{i}$ que l'approximation
                  de la solution, on obtient la méthode de Galerkine.
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Méthodes d'approximation}
    \begin{block}{Conclusion: }
        Que ce soit une méthode d'approximation globale qui utilise d'une fonctionnelle adéquate (l'énergie du système, l'expression du PTV en mécanique) ou une méthode qui repose sur la transformation d'une équation aux dérivées partielles, on a besoin d'exprimer une forme \emph{variationnelle} du problème aux limites.
    \end{block}
\end{frame}
