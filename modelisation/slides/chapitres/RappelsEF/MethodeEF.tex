\section{Méthode des éléments finis}

\begin{frame}
    \frametitle{Méthode des éléments finis}
    \begin{block}{Principes: }
        \begin{itemize}
            \item Formulation variationnelle: méthode de Galerkine
            \item Discrétisation spatiale de la structure (maillage)
            \item Approximation locale des inconnues (support compact)
        \end{itemize}
    \end{block}
    \begin{alertblock}{But: }
        Se placer dans le cadre du théorème de Lax-Milgram
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Choix des fonctions de forme}
    \begin{block}{Principes: }
        \begin{itemize}
            \item Polynômes de Lagrange: de degré 1 ou 2
            \item Polynômes de Hermite: avec dérivées en plus comme inconnues
            \item Fonctions d'Heaviside pour XFEM
        \end{itemize}
    \end{block}
    \begin{figure}
        \begin{center}
            \includegraphics[scale=0.5]{./chapitres/RappelsEF/images/ApproximationEF}
            \caption[*]{Éléments finis de Lagrange de degré 1 en 1D}
        \end{center}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Discrétisation spatiale}
    Découper le domaine en éléments de géométrie simple
    \begin{center}
        \includegraphics[scale=0.3]{./chapitres/RappelsEF/images/Maillage_3}
    \end{center}
    On identifie les $N_{e}$ sous-domaines (ou \emph{éléments}) $\Omega_{e}$ qui pavent l'espace $\Omega$ du solide :
    \begin{equation}
        \Omega=\sum_{e=1}^{N_{e}}\Omega_{e}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Discrétisation spatiale}
    La géométrie du sous-domaine est construite
    avec une approximation nodale, soit pour un \emph{élément} avec $N_{\mathit{nd}}$
    nœuds:
    \begin{equation}
        {x}^{e}=\sum_{i=1}^{N_{nd}}{x}_{i}^{e}.\bar{N}_{i}^{e}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Élément de référence}
    \begin{block}{Principe: }
        Fonctions génériques quelles que soient la forme et la taille de l'élément
        $\rightarrow$
        Utilisation d'un élément de \emph{référence}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Élément de référence}
    \begin{block}{Construction (en 3D): }
        \begin{itemize}
            \item Points des éléments de référence: $\xi_{\alpha=1,3}$ (coordonnées paramétriques)
            \item Points des éléments réels: $x_{\alpha=1,3}$ (coordonnées réelles)
        \end{itemize}
    \end{block}
    \begin{alertblock}{Propriété: }
        La transformation $\tau$ doit être \emph{bijective} :
        \begin{equation}
            {\tau} : \xi_{\alpha}{\leftrightarrow} x_{\alpha}
        \end{equation}
    \end{alertblock}
\end{frame}


\begin{frame}
    \frametitle{Matrice jacobienne de la transformation}
    \begin{definition}
        La jacobienne de la transformation est la matrice des dérivées partielles
        des coordonnées réelles $x_{\alpha}$ par rapport aux coordonnées
        $\xi_{\alpha}$ dans l'élément de référence :
        \begin{equation}
            J_{\alpha\beta}=\frac{\partial x_{\alpha}}{\partial\xi_{\beta}}
        \end{equation}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Matrice jacobienne de la transformation}
    \begin{definition}
        Avec l'approximation de la géométrie, on obtient cette expression de la matrice jacobienne :
        \begin{equation}
            J_{\alpha\beta}=\sum_{i=1}^{N_{\mathit{nd}}}\frac{\partial\bar{N}_{i}}{\partial\xi_{\beta}}.x_{\alpha,i}
        \end{equation}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Matrice jacobienne de la transformation}
    \begin{block}{Propriété importante}
        Le déterminant de la matrice jacobienne $J=\textrm{det} \left ( J_{\alpha\beta} \right ) $ s'appelle le jacobien de la transformation géométrique. Il est non nul lorsque la transformation $\tau$ est bijective
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Discrétisation des inconnues}
    \begin{block}{Principe: }
        On construit une approximation des inconnues de type nodal pour laquelle les coefficients $u_{i}=a_{i}$ correspondent à la solution :
        \begin{equation}
            {u}^{e}({\xi})=\sum_{i=1}^{N_{\mathit{nd}}}u_{i}^{e}.{N}_{i}^{e}({\xi})
        \end{equation}
    \end{block}
    \begin{block}{Remarque: }
        L'approximation EF est \emph{élémentaire} car la fonction ne dépend que des valeurs nodales constituant l'élément.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Discrétisation des inconnues}
    \begin{definition}
        Un élément est \emph{isoparamétrique} lorsqu'il est construit sur des interpolations identiques pour sa géométrie et ses inconnues :
        \begin{equation}
            \bar{N}({\xi})=N({\xi})
        \end{equation}
    \end{definition}
    \begin{alertblock}{Remarque: }
        Dans \emph{Code\_Aster}, tous les éléments finis de milieu continu (2D et 3D) sont isoparamétriques.
    \end{alertblock}
\end{frame}


\begin{frame}
    \frametitle{Intégration numérique}
    \begin{block}{Principe: }
        La méthode des éléments finis est une méthode \emph{intégrale}: il faut calculer ces quantités.
        Dans certains cas particuliers, on peut calculer analytiquement les intégrales, sinon il faut utiliser des \emph{schémas d'intégration numérique} ou \emph{quadrature numérique}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Intégration numérique}
    \begin{alertblock}{Remarque: }
        Par abus de langage, on appelle fréquemment les schémas d'intégration numériques \og schémas de Gauss \fg{} bien qu'il en existe de plusieurs sortes (Hammer, Gauss-Radau, Newton-Cotes, etc.).
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Intégration numérique}
    \begin{definition}
        Formule de quadrature numérique:
        \begin{equation}
            \int_{\Omega^{r}}g({\mathrm{\xi}}).d{\mathrm{\xi}}\approx\sum_{g=1}^{r}\omega_{g}.g(\xi_{g})
        \end{equation}
    \end{definition}
    \begin{definition}
        \begin{itemize}
            \item Les scalaires $\omega_{g}$ sont les \emph{poids} d'intégration
            \item Les coordonnées $\xi_{g}$ sont les coordonnées des $r$ points d'intégration dans l'élément de référence
        \end{itemize}
    \end{definition}
\end{frame}


\begin{frame}
    \frametitle{Application à l'élasticité}
    \begin{block}{Problème: }
        \begin{equation}
            \begin{gathered}
                \text{Trouver }{u}\in E^{h}\text{ tel que }\forall\tilde{u}\in E^{\text{h}}\\
                \text{ avec }a({u},\tilde{{u}})+l(\tilde{{u}})=0
            \end{gathered}
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    \begin{block}{Problème: }
        \begin{equation}
            \begin{gathered}
                a({u},\tilde{u})=\underset{\Omega^{h}}{\int}{{\varepsilon}(\tilde{u})}:{\sigma}({u}).d\Omega^{h}\\
                l(\tilde{u})=\underset{\Omega^{h}}{\int}{f}.\tilde{u}.d\Omega^{h}+\underset{\Gamma_{N}^{h}}{\int}{g}.\tilde{u}.d\Gamma^{h}
            \end{gathered}
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    La discrétisation consiste à choisir une base de l'espace $\Omega^{h}$ et à calculer numériquement les termes de la matrice $A$ et du vecteur $L$.
    Pour cela, on exprime la forme bilinéaire $a(\cdot , \cdot)$ et la forme linéaire $l(\cdot)$ comme une somme sur des éléments, définis par découpage du domaine de base:
    \begin{equation}
        \left\{
        \begin{matrix}
            a(w_{i},w_{j}) =\sum_{\Omega^{e}}\underset{\Omega^{e}}{\int}\sigma_{\mathit{kl}}(w_{i}).\varepsilon_{\mathit{kl}}(w_{j}).d\Omega^{e} \\
            l(w_{i}) = \sum_{\Omega^{e}}\underset{\Omega^{e}}{\int}f_{i}.w_{i}.d\Omega^{e}+\underset{\Gamma_{N}^{e}}{\int}g_{i}.w_{i}.d\Gamma_{N}^{e}
        \end{matrix}
        \right.
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    Les termes $A_{ij}$ sont construits en \emph{assemblant} les contributions provenant de chacun des éléments qui contiennent les noeuds correspondants.
    On procède de la même façon pour construire le vecteur second membre $L_{i}$.
    Ces contributions, appelées \emph{termes élémentaires}, sont calculées lors d'une boucle sur les éléments et ne dépendent que des seules variables de l'élément $\Omega^{e}$ :
    \begin{equation}
        \left\{
        \begin{matrix}
            a^{e} = \underset{\Omega^{e}}{\int}\sigma_{\mathit{kl}}.\varepsilon_{\mathit{kl}}.d\Omega^{e} \\
            l^{e} = \underset{\Omega^{e}}{\int}f_{i}.w_{i}.d\Omega^{e}+\underset{\Gamma_{N}^{e}}{\int}g_{i}.w_{i}.d\Gamma_{N}^{e}
        \end{matrix}
        \right.
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    La relation entre le tenseur des contraintes de Cauchy ${\sigma}$ et les déplacements $u$ est donnée par la relation de comportement, et est indépendante de l'écriture de la formulation
    variationnelle. Dans le cas élastique, on a :
    \begin{equation}
        \sigma_{\mathit{ij}}(w_{i})=\Lambda_{\mathit{ijkl}}.\varepsilon_{\mathit{ku}}(w_{i})
    \end{equation}
    $\Lambda_{ijkl}$ est le tenseur d'élasticité de Hooke.
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    On peut écrire le tenseur des contraintes en notation de Voigt:
    \begin{equation}
        \langle\sigma\rangle = \left\langle \begin{matrix}\sigma_{\mathit{xx}} & \sigma_{\mathit{yy}} & \sigma_{\mathit{zz}} & \sigma_{\mathit{xy}} & \sigma_{\mathit{xz}} & \sigma_{\mathit{yz}}\end{matrix}\right\rangle
    \end{equation}
    On peut écrire le tenseur des déformations en notation de Voigt:
    \begin{equation}
        \langle\varepsilon\rangle = \left\langle \begin{matrix}\varepsilon_{\mathit{xx}} & \varepsilon_{\mathit{yy}} & \varepsilon_{\mathit{zz}} & 2.\varepsilon_{\mathit{xy}} & 2.\varepsilon_{\mathit{xz}} & 2.\varepsilon_{\mathit{yz}}\end{matrix}\right\rangle
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    Cette notation de Voigt permet d'écrire de manière plus compact. Par exemple le produit contracté de deux tenseurs:
    \begin{equation}
        {\sigma}:{\varepsilon}=\langle\sigma\rangle.\{\varepsilon\}
    \end{equation}
    Avec cette nouvelle notation, nous avons aussi:
    \begin{equation}
        \{\sigma\}=[A].\{\varepsilon\}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    Nous repartons de l'écriture EF du champ des déplacements:
    \begin{equation}
        \left\{ {u}^{e}\right\} =\left[N_{i}^{e}\right].\left\{ {u}_{i}^{e}\right\} =\left\langle {u}_{i}^{e}\right\rangle .\left[N_{i}^{e}\right]^{T}
    \end{equation}
    Et, de manière analogue, le champ des déplacements virtuels:
    \begin{equation}
        \left\{ {\tilde{u}}^{e}\right\} =\left[N_{i}^{e}\right].\left\{ {\tilde{u}}_{i}^{e}\right\} =\left\langle {\tilde{u}}_{i}^{e}\right\rangle .\left[N_{i}^{e}\right]^{T}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    Il est nécessaire d'exprimer le tenseur des déformations (virtuelles ou réelles):
    \begin{equation}
        \left\{ \varepsilon \right\} = \left[ B \right] \left\{ {u} \right\} = \left\langle {u} \right\rangle \left[ B \right]^{T}
    \end{equation}
    \begin{equation}
        \left\{ \tilde{\varepsilon} \right\} = \left[ B \right] \left\{ {\tilde{u}} \right\} = \left\langle {\tilde{u}} \right\rangle \left[ B \right]^{T}
    \end{equation}
    La matrice $\left[ B \right] $ contient les dérivées des fonctions de forme.
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    On obtient alors pour la matrice relative à la forme bilinéaire:
    \begin{equation}
        \left[ a \right] = \underset{\Omega^{e}}{\int}\left[B\right]^{T}.[\Lambda].\left[B\right].d\Omega^{e}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    On obtient donc un système linéaire à résoudre:
    \begin{equation}
        \langle{\tilde{u}}\rangle.[A].\left\{ {u}\right\} +\langle{\tilde{u}}\rangle.\left\{ {L}\right\} =0
    \end{equation}
    Quel que soit le champ des déplacements virtuels, donc:
    \begin{equation}
        [A].\left\{ {u}\right\} =\left\{ {L}\right\}
    \end{equation}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    \begin{alertblock}{Rappel: }
        Les conditions limites de Dirichlet sont exprimées dans le champ d'approximation et non dans la formulation variationnelle.
        Il faut donc les imposer \emph{a posteriori}
    \end{alertblock}
    Le traitement des conditions aux limites cinématiques peut se faire de deux manières différente dans Code\_Aster :
    \begin{itemize}
        \item Par élimination directe de la ligne et de la colonne. Méthode rapide mais pas générique.
        \item Par dualisation en introduisant des multiplicateurs (ou paramètres) de Lagrange. Méthode moins rapide mais générique.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Application à l'élasticité}
    Le système linéaire peut être résolu par un certain nombre de méthodesqui se divisent en trois catégories:
    \begin{itemize}
        \item Les méthodes directes
        \item Les méthodes itératives
        \item Les méthodes hybrides
    \end{itemize}
    \begin{alertblock}{Rappel: }
        Les matrices issues de la méthode des éléments finis sont très creuses (elles comportent une majorité de termes nuls).
    \end{alertblock}
\end{frame}
