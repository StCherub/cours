\section{Lois de comportement}


\begin{frame}
    \frametitle{Écriture générale d'une loi de comportement}
    La loi de comportement permet de calculer les contraintes $\stress(\point, t)$ à l'instant $t$ et en chaque point $\point$ du solide $s$. On va établir les hypothèses et les propriétés de la fonctionnelle du comportement $\fonctionnelleComportement$ qui donne ces contraintes.

\end{frame}

\begin{frame}
    \frametitle{Écriture générale d'une loi de comportement}
    Les trois principes fondamentaux d'une loi de comportement
    \begin{enumerate}
        \item Principe de causalité: la cause précède l'effet
        \item Principe d'invariance temporelle: deux observateurs décalés dans le temps doivent observer le même comportement
        \item Principe d'indifférence matérielle: la loi de comportement utilisée doit être invariante par changement de référentiel. On parle aussi d'objectivité
    \end{enumerate}

\end{frame}

\begin{frame}
    \frametitle{Écriture générale d'une loi de comportement}
    Les trois hypothèses faites ici
    \begin{enumerate}
        \item Hypothèse de l'état local
        \item Hypothèse d'un milieu matériellement simple
        \item Hypothèse d'un milieu homogène
    \end{enumerate}

    \begin{block}{Remarque:}
        L'écriture du \PPV a déjà utilisé les deux premières hypothèses.
    \end{block}


\end{frame}


\begin{frame}
    \frametitle{Écriture générale d'une loi de comportement}

    La loi de comportement est donc une \emph{fonctionnelle}
    \begin{equation*}
        \stress\left( \point,t\right) =%
        \underset{\tau \leq t}{\mathcal{G}}%
        \left( \Fdef (\point,\tau) \right)
    \end{equation*}

    L'approche fonctionnelle n'est utilisée en pratique que pour la visco-élasticité (transformée de Laplace par exemple), en général, on utilise plutôt une approche par variables internes de "mémorisation" $\alpha_i$ (scalaires, vecteurs et/ou tenseurs)

    \begin{equation*}
        \stress\left( \point,t\right) =%
        {\mathcal{G}}%
        \left( \Fdef (\point) , \alpha_i (\point) \right)
    \end{equation*}

\end{frame}


\begin{frame}
    \frametitle{Écriture générale d'une loi de comportement}

    \begin{definition}[Âne-isotope-pis matérielle]
        Soit $\matrice{Q}_{s}$ une symétrie matérielle, la symétrie s'écrit

        \begin{equation*}
            \stress =%
            {\mathcal{G}}%
            \left( \Fdef  \right) \Rightarrow  \stress = {\mathcal{G}} \left ( \Fdef \matrice{Q}_{s} \right )
        \end{equation*}

        La matériau est \emph{isotrope} si $\matrice{Q}_{s}$  appartient au groupe orthogonal.
    \end{definition}


\end{frame}

\begin{frame}
    \frametitle{Approche thermodynamique}

    En \HPP, décomposition additive des déformations:
    \begin{equation*}
        \strain = \strain^e + \strain^p
    \end{equation*}

    La loi de comportement peut s'écrire sous la forme d'une énergie libre $\enerFree$ (potentiel) décomposée en sa partie élastique et sa partie inélastique (variables internes)

    \begin{equation*}
        \enerFree = \enerFree^{e} (\strainElas) + \enerFree^{p} (\alpha_i)
    \end{equation*}


\end{frame}


\begin{frame}
    \frametitle{Approche différentielle}

    En \HPP, décomposition additive des taux de déformations:
    \begin{equation*}
        \dot \strain = {\dot \strain}^e + {\dot \strain}^p
    \end{equation*}

    La loi de comportement peut s'écrire sous la forme d'un système différentiel (écriture en vitesse)
    \begin{equation*}
        {\dot \strain}^e = \inverse{\matriceHooke} : \stress
    \end{equation*}
    avec $\matriceHooke$ la matrice d'élasticité de Hooke

    \begin{block}{Remarque:}
        En \HPP, ces deux écritures sont strictement équivalentes avec $\enerFree^{e} = \frac12 \strain^e : \matriceHooke : \strain^e$
    \end{block}

\end{frame}




\begin{frame}
    \frametitle{Écriture en grandes transformations}

    Décomposition des parties élastiques et inélastiques des déformations:

    \begin{equation*}
        \EGL \neq \EGL^e + \EGL^p
    \end{equation*}

    Forme générale de la décomposition (voir \cite{BCCF:2001})

    \begin{equation*}
        \Fdef = \Fdef^e + \Fdef^p - \miden + (\Fdef^e - \miden) \matrice{H} (\Fdef^p - \miden)
    \end{equation*}

    avec $\matrice{H}$ un tenseur d'ordre six (!) isotrope
\end{frame}


\begin{frame}
    \frametitle{Écriture en grandes transformations}

    Décomposition des parties élastiques et inélastiques des déformations, cas le plus simple: décomposition multiplicative du gradient de transformation ( $\matrice{H} = \miden$ )

    \begin{equation*}
        \Fdef = \Fdef^e  \Fdef^p
    \end{equation*}

    \begin{block}{Remarque:}
        La théorie de la plasticité cristalline donne des arguments physiques et expérimentaux à cette décomposition.
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Écriture en grandes transformations}

    \begin{block}{Approche eulérienne}
        Le principe d'indifférence matérielle ne permet pas à un matériau d'être anisotrope si on utilise des variables eulériennes (Cauchy / taux de déformation par exemple).
    \end{block}

    \begin{block}{Approche lagrangienne}
        Quantités invariantes, l'écriture est possible, y compris en anisotropie, mais difficile en pratique.
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Écriture en grandes transformations}

    \begin{block}{Approche différentielle}
        La dérivation en temps n'étant pas objective, il faut utiliser une dérivée \emph{objective}, mais on a des  limitations (isotropie uniquement).
    \end{block}

\end{frame}


\begin{frame}
    \frametitle{Dérivées objectives, si moisies que ça ?}

    Les dérivées de Jaumann, Naghdi ou Truesdell présentent des pathologies, en particulier des oscillations parasites pour de fortes déformations).     Mais \cite{BCCF:2001} tempère ces résultats:
    \begin{itemize}
        \item Oscillations parasites en élasticité pour Jaumann, mais il est rare que les déformations élastiques soient importantes
        \item En plasticité, pas d'oscillations en écrouissage isotrope, présence d'oscillations en cinématique linéaire
        \item En élasto-plasticité, oscillations contrôlables en  cinématique non-linéaire, imperceptibles en écrouissage isotrope et cinématique linéaire pour des valeurs physiques des coefficients matériaux usuels(métaux)
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{En résumé}

    \begin{itemize}
        \item Pour les problèmes anisotropes privilégier une approche lagrangienne
        \item L'approche hyperélastique est en général plus robuste
        \item Les dérivées objectives sont à éviter en général (problèmes d'oscillation)
    \end{itemize}

\end{frame}
