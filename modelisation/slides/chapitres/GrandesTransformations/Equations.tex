\section{Équations}


\begin{frame}
    \frametitle{Changement de référentiel}

    Considérons un point $\point$ repéré par ses coordonnées $\Position_1(t)$ à l'instant $t$ dans un référentiel $\mathcal{R}_1$, et par ses coordonnées $\Position_2(t)$ à l'instant $t$ dans un autre référentiel $\mathcal{R}_2$.
    La  transformation dite rigide si la relation entre ces coordonnées s'écrit:
    \begin{equation*}
        \Position_2(t) = \vecteur{a(t)} + \matrice{Q}(t) \Position_1(t)
    \end{equation*}
    où $\vecteur{a(t)} \in \Rd$ et $\matrice{Q}(t) \in \spaceGroupOrthoSpecial$ sont respectivement un vecteur et une matrice de rotation donnés. La matrice de rotation étant telle que
    \begin{equation*}
        \matrice{Q} \transpose{\matrice{Q}} = \miden \text{ et } \det \matrice{Q} = 1
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Indifférence matérielle}

    \begin{definition}[Principe fondamental]
        Une loi de comportement ne doit pas dépendre du référentiel dans lequel on l'évalue $\rightarrow$ notion d'\emph{objectivité} ou d'\emph{indifférence matérielle}.
    \end{definition}

\end{frame}

\begin{frame}
    \frametitle{Objectivité}

    \begin{definition}[Objectivité]
        Une grandeur est qualifiée d'objective si elle représente une quantité physique qui ne dépend pas du référentiel (galiléen pour simplifier).
        On change de référentiel par une transformation rigide.
        Pour un vecteur:
        \begin{equation*}
            \vecteur{a}_2 = \matrice{Q} \vecteur{a}_1
        \end{equation*}
        Pour un tenseur d'ordre deux:
        \begin{equation*}
            \matrice{A}_2 = \matrice{Q} \matrice{A}_1 \transpose{\matrice{Q}}
        \end{equation*}
    \end{definition}
\end{frame}


\begin{frame}
    \frametitle{Invariance}

    Encore plus mieux que l'objectivité: l'invariance !

    \begin{definition}[Invariance]
        Quand une quantité physique scalaire, vectorielle ou tensorielle, notée respectivement $a$, $\vecteur{A}$ et $\matrice{A}$ sera invariante, on la notera, respectivement, $\mathring{a}$, $\vecteur{\mathring{A}}$ et $\matrice{\mathring{A}}$.
    \end{definition}

    \begin{block}{Remarque: }
        Une quantité scalaire est invariante (masse, énergie).
    \end{block}

\end{frame}


\begin{frame}
    \frametitle{Gradient de transformation}


    \begin{definition}[Gradient de transformation]
        Soit une transformation \emph{infinitésimale} $\dx$ qui modifie la position d'un point $\point$ du solide.
        Par définition du déplacement $\Disp_\point$ on définit le \emph{gradient de transformation} $\Fdef(\point, t)$
        \begin{equation*}
            \Fdef(\point, t) = \miden + \gradV \Disp_\point
        \end{equation*}
    \end{definition}

    \begin{block}{Remarque: }
        Cadre de la théorie mécanique du \emph{premier gradient} qui stipule que la déformation locale d'une petite zone autour d'un point matériel ne peut être influencée que par ses voisins immédiats.
    \end{block}

\end{frame}


\begin{frame}
    \frametitle{Gradient de transformation}

    Le gradient de transformation n'est pas une mesure adéquate de la \emph{déformation} d'un solide:
    \begin{itemize}
        \item contient toute l'information du mouvement, y compris la partie correspondant à une rotation \emph{rigide} $\matrice{Q}$ (sans changement de forme).
        \item dans le cas rigide, on a $\Fdef = \miden \neq \mzero$
        \item $\Fdef$ est à cheval sur la configuration lagrangienne et eulérienne.  Ce n'est pas une quantité objective
    \end{itemize}

\end{frame}


\begin{frame}
    \frametitle{Gradient de transformation}

    Le gradient de la transformation $\Fdef$ peut toujours se décomposer comme le produit d'une déformation pure suivi d'une rotation rigide

    \begin{equation*}
        {\Fdef} = \matrice{R} \matrice{U}
    \end{equation*}
    \begin{itemize}
        \item $\matrice{R}$ est orthogonal
        \item $\matrice{U}$ est symétrique et positif
    \end{itemize}

\end{frame}


\begin{frame}
    \frametitle{Vitesse de transformation}

    $\point$ initialement à la position $\Position_\point$ aura pour vitesse
    \begin{equation*}
        \Vite_\point(t) = \frac{\partial \Disp}{\partial t}
    \end{equation*}
    $\dot{\Fdef}$ la vitesse lagrangienne du gradient de transformation
    \begin{equation*}
        \dot{\Fdef}
        =
        \frac{\partial \Fdef}{\partial t}
        =
        \frac{\partial \Vite_\point}{\partial \Position}
    \end{equation*}
    $\Ldef$ la vitesse eulérienne du gradient de transformation
    \begin{equation*}
        \Ldef
        =
        \frac{\partial \Vite}{\partial \position}
        =
        \frac{\partial \Vite}{\partial \Position}
        \frac{\partial \Position}{\partial \position}
        =
        \dot{\Fdef}
        \inverse{\Fdef}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Vitesse de transformation}

    Décomposition en partie symétrique et anti-symétrique
    \begin{align}
        \left \{
        \begin{aligned}
            \viteDefo & = \frac12 \left (  \Ldef + \transpose{\Ldef}  \right ) \\
            \viteRota & = \frac12 \left (  \Ldef -\transpose{\Ldef}  \right )  \\
        \end{aligned}
        \right .
    \end{align}
    Ni $\viteRota$, ni $\Ldef$ ne sont des grandeurs objectives.

\end{frame}


\begin{frame}
    \frametitle{Mesures de déformation}

    Hypothèse des petites perturbations (\HPP) $\rightarrow$ la mesure $\strain(\Disp)$ est unique

    \begin{equation*}
        \strain(\Disp) := \frac12 (\gradV \Disp + \transpose{\gradV \Disp }) = \gradVs \Disp
    \end{equation*}

    Si pas \HPP, il existe une infinité de mesures de déformations.
    Il faut choisir une mesure en prenant en compte la bonne représentation physique liée à la loi de comportement du matériau, des résultats expérimentaux et de considérations pratiques d'implémentation.

\end{frame}

\begin{frame}
    \frametitle{Mesures de déformation}

    Pour caractériser les \emph{changements de forme} $\rightarrow$ mesure  variations de longueurs et d'angle.

    Quantification des variations du produit scalaire de deux vecteurs infinitésimaux $\dX$ et $\dY$ se transformant en vecteurs $\dx$ et $\dy$
    \begin{equation*}
        \dx \cdot \dy = \dX \left ( \transpose{\Fdef} \Fdef \right )\dY
    \end{equation*}

\end{frame}


\begin{frame}
    \frametitle{Mesures de déformation}

    \begin{definition}[Déformation de Cauchy-Green à droite]
        On appelle tenseur de Cauchy-Green à droite le tenseur $\ECGDroit$ (lagrangien)
        \begin{equation*}
            \ECGDroit(\point, t) := \transpose{\Fdef}  \Fdef
        \end{equation*}
    \end{definition}

    \begin{block}{Remarque}
        On a $ \ECGDroit = \matrice{U}^2$, c'est donc bien une mesure de changement de forme sans la rotation
    \end{block}

\end{frame}

\begin{frame}
    \frametitle{Mesures de déformation}

    \begin{definition}[Déformation de Cauchy-Green à gauche]
        On appelle tenseur de Cauchy-Green à gauche le tenseur $\ECGGauche$ (eulérien)
        \begin{equation*}
            \ECGGauche(\point, t) := \Fdef \transpose{\Fdef}
        \end{equation*}
    \end{definition}

\end{frame}

\begin{frame}
    \frametitle{Mesures de déformation}

    $\ECGDroit$ n'est pas nul quand le matériau ne se déforme pas,  on introduit le tenseur de Green-Lagrange $\EGL $
    \begin{definition}[Déformation de Green-Lagrange]
        On appelle déformation de Green-Lagrange le tenseur tel que
        \begin{equation*}
            \EGL (\point, t) := \frac12 ( \ECGDroit - \miden) = \frac12 ( \transpose{\Fdef} \Fdef - \miden)
        \end{equation*}
        Le tenseur $\EGL$ est symétrique
    \end{definition}


\end{frame}

\begin{frame}
    \frametitle{Mesures de déformation}

    On a aussi:
    \begin{equation*}
        \EGL(\point, t) = \frac12 (\gradV \Disp + \transpose{\gradV \Disp} + \color{red} \transpose{\gradV \Disp}\gradV \Disp \color{black} )
    \end{equation*}
    On voit que $\EGL$ n'est pas une quantité linéaire en fonction du déplacement $\Disp$.

    $\strain(\Disp)$ est la partie linéaire du tenseur de Green-Lagrange $\EGL$.

\end{frame}

\begin{frame}
    \frametitle{Mesures de déformation}

    Famille générique de Seth \& Hill (\cite{Hill:1968})
    \begin{equation*}
        \matrice{E}_m =
        \begin{cases}
            \frac{1}{m} \left (\matrice{C}^{m} - \miden \right )
                                     & \text{si $m \ne 0$} \\
            \frac12 \ln(\matrice{C}) & \text{sinon}
        \end{cases}
    \end{equation*}

    Green-Lagrange $\EGL$ correspond à $\matrice{E}_{m=1}$  et mesure de déformation logarithmique à $\matrice{E}_{m=0}$
    \begin{equation*}
        \Elog =\frac12 \ln(\matrice{C})
    \end{equation*}


\end{frame}


\begin{frame}
    \frametitle{Mesures de déformation}

    \includegraphics[angle=181,width=0.9\textwidth]{./images/GrandeDefo}
\end{frame}


\begin{frame}
    \frametitle{Mesures de contraintes}

    Les efforts exercés sur une partie $\domainPart$ d'un milieu continu par son complémentaire dans le solide $s$ peuvent être représentés par une densité surfacique d'efforts $\surfacicForce(\point)$ en chaque point $\point \in \boundDomainPart$.
    \begin{equation*}
        \surfacicForce(\point;\boundDomainPart) = \stressNormal(\point, \normal)
    \end{equation*}

    Ce qui permet d'établir le tenseur des contraintes de Cauchy $\stress$
    \begin{equation*}
        \stressNormal(\point, \normal) \coloneqq \stress(\point) \normal
    \end{equation*}
    La quantité $\stress$ est le tenseur des contraintes de Cauchy (aussi appelé tenseur des contraintes vraies).  Elle représente la cohésion de milieu continu du solide $s$.

\end{frame}

\begin{frame}
    \frametitle{Mesures de contraintes}

    On doit introduire d'autres contraintes.
    \begin{definition}[Tenseur des contraintes de Kirchhoff]

        \begin{equation*}
            \stressKirchhoff = J \stress
        \end{equation*}
        Le tenseur $\stressKirchhoff$ est symétrique

    \end{definition}

\end{frame}

\begin{frame}
    \frametitle{Mesures de contraintes}

    Le tenseur des contraintes de Cauchy n'a de sens que sur la déformée. Si on veut écrire sur la configuration initiale (lagrangienne)

    \begin{definition}[Tenseur des contraintes de Piola-Kirchhoff]
        Rapport entre la force appliquée $f^t$ sur la surface \emph{initiale} $s^0$:
        \begin{equation*}
            \stressComposantePK = \frac{f^t}{s^0}
        \end{equation*}
        \begin{equation*}
            \stressPK = J \stress \inverseTranspose{\Fdef} = \stressKirchhoff \inverseTranspose{\Fdef}
        \end{equation*}
        Ce tenseur n'est pas symétrique.
    \end{definition}


\end{frame}

\begin{frame}
    \frametitle{Mesures de contraintes}
    Si on transporte les efforts vers la configuration initiale.

    \begin{definition}[Tenseur des contraintes  de Piola-Kirchhoff de seconde espèce]
        \begin{equation*}
            \stressPKTwo
            =
            J \inverse{\Fdef} \stress \inverseTranspose{\Fdef}
            =
            \inverse{\Fdef} \stressKirchhoff \inverseTranspose{\Fdef}
            \Leftrightarrow
            \stressKirchhoff
            =
            \Fdef \stressPKTwo \transpose{\Fdef}
        \end{equation*}
        Ce tenseur est symétrique mais son sens physique est douteux !
    \end{definition}


\end{frame}


\begin{frame}
    \frametitle{Mesures de contraintes}

    En \HPP, les configurations déformées et initiales étant confondues, toutes ces mesures de contraintes sont identiques:
    \begin{equation*}
        \stress \approx \stressKirchhoff \approx \stressPK \approx \stressPKTwo
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Équations d'équilibre}

    Par application du \PPV sur la configuration déformée $\domain$
    \begin{equation*}
        \int_{\domain} \volumicForce \cdot \ViteVirt  \;d \domain
        +
        \int_{\boundN} \surfacicForce \cdot \ViteVirt  \;d \boundN
        -
        \int_{\domain} {\stress} : {\viteDefoVirt(\ViteVirt)}  \;d \domain
        =
        \int_{\domain} \density \Acce \cdot \ViteVirt \; d\domain
        ,\quad \forall \ViteVirt
    \end{equation*}

    avec $\viteDefoVirt(\ViteVirt)$ le tenseur (symétrique) de vitesse des déformations virtuelles $\viteDefoVirt$:
    \begin{equation*}
        \viteDefoVirt(\ViteVirt)
        =
        \frac{1}{2}
        \left (
        \gradV \ViteVirt
        +
        \transpose {\gradV \ViteVirt}
        \right )
    \end{equation*}

\end{frame}


\begin{frame}
    \frametitle{Équations d'équilibre}

    Transport sur la configuration initiale $\Domain$
    \begin{equation*}
        \int_{\Domain} \volumicForce \cdot \ViteVirt  \;d \Domain
        +
        \int_{\BoundN} \surfacicForce \cdot \ViteVirt \;d \BoundN
        -
        \int_{\Domain} \stressPKTwo : \dot{\EGL } (\ViteVirt)  \;d \Domain
        =
        \int_{\Domain} \density \Acce \cdot \ViteVirt \; d\Domain
        ,\quad \forall \ViteVirt
    \end{equation*}
    avec
    \begin{equation*}
        \dot{\EGL } (\ViteVirt)
        =
        \transpose{\Fdef} \viteDefoVirt(\ViteVirt) \Fdef
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Équations d'équilibre}

    \begin{definition}[]
        La mécanique est le lieu de naissance des espaces en dualité (J.-J. Moreau)
    \end{definition}

    On a les relations suivantes
    \begin{equation*}
        J \stress : \viteDefoVirt
        =
        \stressKirchhoff : \viteDefoVirt
        =
        \stressPK : \dot{\Fdef}
        =
        \stressPKTwo : \dot{\EGL}
        =
        \stressLog : \dot{\Elog}
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Dérivées objectives}

    Considérons $\vecteur{u}$ un vecteur \color{red} rigide \color{black} dans le référentiel $\mathcal{R}_a$:
    \begin{equation*}
        \vecteur{u}_a =\Position_2(t) - \Position_1(t)
    \end{equation*}

    on souhaite transformer ce vecteur dans un autre référentiel $\mathcal{R}_b$
    \begin{equation*}
        \vecteur{u}_b = \matrice{Q} \vecteur{u}_a
    \end{equation*}

    sa dérivée temporelle s'écrit alors

    \begin{equation*}
        \dot{\vecteur{u}_b} = \dot {\matrice{Q}} \transpose {\matrice{Q}} \vecteur{u}_a
    \end{equation*}

\end{frame}

\begin{frame}
    \frametitle{Dérivées objectives}

    Ce vecteur est rigide, on a donc
    \begin{equation*}
        \dot{\vecteur{u}_a} = \vecteur{0}
    \end{equation*}

    mais on a
    \begin{equation*}
        \dot{\vecteur{u}_b} \neq \vecteur{0}
    \end{equation*}

    \begin{definition}[Dérivée objective]
        Une dérivée objective $\mathring{a}$ d'une quantité $a$ est une dérivée qui permet de respecter le principe d'indifférence matériel.
    \end{definition}

\end{frame}


\begin{frame}
    \frametitle{Dérivées objectives}

    Si la dérivée est objective, pour un tenseur $\matrice{A}$, on aura

    \begin{equation*}
        \mathring{\matrice{A}_2} = \matrice{Q} \mathring{\matrice{A}_1} \transpose{\matrice{Q} }
    \end{equation*}


    La forme générale d'une dérivée objective est
    \begin{equation*}
        \mathring{\matrice{A}} = \varphi \frac{d}{dt} \left (   \inverse{\varphi} \matrice{A}\inverseTranspose{\varphi} \right ) \transpose{\varphi}
    \end{equation*}
    avec $\varphi$ une application du référentiel matériel sur le référentiel spatial.


\end{frame}

\begin{frame}
    \frametitle{Dérivées objectives}

    L'idée générale est de se "débarasser" de la rotation. Voici quelques dérivées utilisées:

    Dérivée de Jaumann-Zaremba (ou dérivée dans le référentiel corotationnel):
    $\mathring{\matrice{A}} ^{JZ} = \dot{\matrice{A}} + \matrice{A} \viteRota - \viteRota \matrice{A}$

    Dérivée de Truesdell-Oldroyd:
    $\mathring{\matrice{A}} ^{TO} = \dot{\matrice{A}} - \matrice{L} \matrice{A}  - \matrice{A} \transpose{\matrice{L}} + \matrice{A } \trace{\matrice{L}}$

    Dérivée de Green-Naghdi:
    $\mathring{\matrice{A}} ^{GN} = \dot{\matrice{A}} + \matrice{A} \Omega - \Omega \matrice{A}$ avec $\Omega = \dot{\matrice{R}} \transpose{\matrice{R}}$

    \begin{block}{Remarque}
        La dérivée de Jaumann est utilisée dans ABAQUS et Zebulon. La dérivée de Green-Naghdi est utilisée dans ANSYS.
    \end{block}

\end{frame}


\begin{frame}
    \frametitle{En résumé}
    \begin{itemize}
        \item Notion d'objectivité ou d'invariance
        \item En grandes déformations, plusieurs mesures de déformations possibles (à choisir !), la contrainte doit être choisie en cohérence (dualité)
        \item Les quantités lagrangiennes sont invariantes
        \item Les dérivées temporelles des quantités ne sont pas objectives
        \item Il existe des dérivées "objectives"
    \end{itemize}
\end{frame}
