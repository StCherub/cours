\section{Le maillage}\label{Para:ObjetMaillage}

Cet objet décrit le maillage sur lequel on va effectuer les calculs. C'est une description purement géométrique qui contient:
\begin{itemize}
  \item Les coordonnées des nœuds et leur nom;
  \item La connectivité des éléments et leur nom;
  \item Les groupes de nœuds (\CAGroupNode) et d'éléments (\CAGroupElem).
\end{itemize}

\begin{figure}[!ht]
  \centering
  \includegraphics[scale=0.3]{./chapitres/images/MaillageStructure}
  \caption{Contenu du fichier maillage}
  \label{Fig:MaillageStructure}
\end{figure}

\subsection{Les formats de maillage et leur lecture}\label{Para:MaillageLecture}

Le logiciel \CodeAster a son propre format de description du maillage (le format \CAFileMAIL) mais il peut lire d'autres formats: \CAFileIDEAS, \CAFileGMSH, \CAFileGIBI et surtout le format \CAFileMED qui est le plus recommandé\footnote{Ce fichier est celui utilisé par la plateforme \Salome, il est binaire (donc très compact) mais grâce à un format spécifique, il est totalement indépendant de la plateforme}. L'ensemble a été produit à l'extérieur de \CodeAster (dans le module \SAModuleMesh par exemple). Il est hautement recommandé de faire toutes les opérations de manipulation du maillage \emph{en dehors} de \CodeAster.

Pour lire un maillage au format \CAFileMAIL ou \CAFileMED, on utilise la commande \CAReadMesh:
\begin{lstlisting}
mesh = LIRE_MAILLAGE(FORMAT='MED',UNITE=20)
\end{lstlisting}

L'unite \CAFileLogicalUnit permet de donner le fichier qui va être lu.

\begin{Remark}
  La manipulation des fichiers et des unités logiques est confiée au gestionnaire d'études (\Astk ou \AsterStudy). Il existe néanmoins une commande permettant de gérer cet aspect à l'intérieur d'un fichier de commande, c'est la commande \CADefineFile dont voici un exemple d'usage:
  \begin{lstlisting}
unit = DEFI_FICHIER(ACTION='ASSOCIER',
                    FICHIER='./REPE_OUT/GF.concept',
                    ACCES='NEW')
\end{lstlisting}
  Cette commande va associer une unité logique (dont le numéro sera généré automatiquement dans la variable \texttt{unit}) au fichier de \emph{nom} décrit dans la mot-clef \texttt{FICHIER}. Dnas l'exemple donné, on utilise le répéertoire \texttt{REPE\_OUT} qui est toujours défini par le gestionnaire de calcul. En effet, l'endroit où le calcul sera réalisé dépend de l'exécution. Il s'agit donc d'un répértoire \emph{temporaire} qui sera effacé à la fin du calcul. Il ne faut donc l'utiliser que si l'on ne désire pas conserver le fichier à la fin. Sinon, il faut définir un nom de fichier dans un répertoire fixe connu de l'utilisateur.
  Quand on a défini cette unité logique et qu'on ne vaut plus l'utiliser, il faut la \emph{libérer} par la commande suivante:
  \begin{lstlisting}
DEFI_FICHIER(ACTION='LIBERER', UNITE=unit)
\end{lstlisting}
\end{Remark}

Pour lire d'autres formats que \CAFileMAIL ou \CAFileMED, il est nécessaire d'utiliser avant les commandes \CAReadMeshGMSH, \CAReadMeshGIBI et \CAReadMeshIDEAS qui vont transformer les formats \CAFileGMSH, \CAFileGIBI et \CAFileIDEAS en leur représentation \CodeAster :

\begin{lstlisting}
PRE_GMSH(UNITE_GMSH = 37, UNITE_MAILLAGE = 20 )
mesh = LIRE_MAILLAGE(FORMAT='ASTER',UNITE=20)
\end{lstlisting}

La première commande va \emph{transformer} le fichier \CAFileGMSH (contenu sur l'unité logique $37$) en fichier au format \CAFileMAIL sur l'unité logique $20$. La deuxième commande va simplement lire le maillage ainsi produit.

\begin{Remark}
  L'exemple choisi n'est pas très pertinent en pratique. En effet \CAFileGMSH sait produire des fichiers au format \CAFileMED et il est donc préconisé de choisir ce format !
\end{Remark}

\subsection{Définir des groupes}

La commande \CADefineGroup permet de définir des \CAGroupNode et des \CAGroupElem qui n'auraient pas été définis dans le mailleur externe. Elle dispose d'outils divers pour créer ces groupes. Par exemple, créer des groupes à partir d'une définition géométrique simplifiée:
\begin{lstlisting}
mesh=DEFI_GROUP(reuse =mesh,
                MAILLAGE=mesh,
                CREA_GROUP_MA=_F(
                                 OPTION = 'SPHERE',
                                 NOM    = 'MASUPTMP',
                                 POINT  = (1.0,0.5),
                                 RAYON  = 1.e-6,
                                 ),
               )
\end{lstlisting}
La commande précédente va créer \CAGroupElem de tous les éléments intersactant la sphère de centre $(1.0,0.5)$ et de rayon $1.E^{-6}$
On remarquera que cette commande est forcément ré-entrante: elle va enrichir la description d'un maillage déjà existant.
On peut également créer des groupes à partir du type de maille par exemple (ici tous es triangles à 3 noeuds dumailalge forment le \CAGroupElem de nom \texttt{ALL\_TRIA3} :
\begin{lstlisting}
mesh=DEFI_GROUP(reuse =mesh,
                MAILLAGE=mesh,
                CREA_GROUP_MA=_F(
                                 TYPE_MAILLE = 'TRIA3',
                                 NOM         = 'ALL_TRIA3',
                                 TOUT        = 'OUI',
                               ),
               )
\end{lstlisting}

La commande suivante est fréquente. Elle permet de créer un \CAGroupNode (de nom \texttt{'TOTO'}) à partir du \CAGroupElem de même nom:
\begin{lstlisting}
mesh=DEFI_GROUP(reuse=mesh,
                MAILLAGE=mesh,
                CREA_GROUP_NO=_F(NOM      = 'TOTO',
                                 GROUP_MA = 'TOTO'),
                )
\end{lstlisting}

\subsection{Modifier un maillage}

La commande \CAChangeMesh permet des manipulations importantes qui sont souvent absentes des mailleurs. En particulier, via les options \CASetMeshOrientation, elle s'assure que les faces sont bien définies avec une normale extérieure\footnote{Si la conformité des normales est généralement vérifiée au minimum dans la plupart des commandes en ayant besoin (comme l'affectation des pressions ou le contact), certains cas ne sont pas possibles pour ces commandes (l'orientation des éléments de coque par exemple), et, de plus, ces commandes avertissent du problème mais ne sont pas en mesure de le corriger, il faudra alors utiliser les options \CASetMeshOrientation}.

\begin{lstlisting}
mesh = MODI_MAILLAGE(
              reuse        = mesh,
              MAILLAGE     = mesh,
              ORIE_PEAU_2D = _F(GROUP_MA = ('ENCASTRE'),),
                    )
\end{lstlisting}
Cette commande est ré-entrante: elle va enrichir la description d'un maillage déjà existant. Elle ne modifie que la connectivité des mailles pour faire en sorte que les normales soient sortantes.

Mais on y trouvera également l'option permettant de créer des mailles de Barsoum pour la mécanique de la rupture:
\begin{lstlisting}
MODI_MAILLAGE(reuse=mesh,
              MAILLAGE=mesh,
              MODI_MAILLE=_F(
                             OPTION      = 'NOEUD_QUART',
                             GROUP_NO_FOND = 'FOND',
                            ),
              )
\end{lstlisting}

\subsection{Créer un maillage}

La commande \CACreateMesh est importante pour créer des éléments particuliers qui sont parfois impossibles à créer avec des mailleurs externes.
Par exemple, \CodeAster utilise la notion de maille de type \CAElemPOI. Ce sont des éléments finis dont le support géométrique est un nœud\footnote{\Salome sait créer ces éléments}.

\begin{lstlisting}
mesh2=CREA_MAILLAGE(MAILLAGE=mesh1,
                    CREA_POI1=_F(NOM_GROUP_MA = 'DIS',
                                 GROUP_NO     = 'NC',),)
\end{lstlisting}

On peut également transformer les mailles d'une approximation linéaire à quadratique (ou vice-versa)\footnote{\Salome sait faire ces opérations mais il n'est pas capable de créer des éléments de type \CAElemTRIAQuad, nécessaire pour utiliser des éléments finis de type \texttt{COQUE\_3D}}, par les options options \CASetMeshQuadratic et \CASetMeshLinear :

\begin{lstlisting}
mesh2=CREA_MAILLAGE(MAILLAGE=mesh1,
                    LINE_QUAD = _F(TOUT = 'OUI'),)
\end{lstlisting}

Quand on a besoin de mailles particulières, par exemple des éléments de type \CAElemTRIAQuad, nécessaires pour utiliser des éléments finis de type \texttt{COQUE\_3D}:

\begin{lstlisting}
mesh2=CREA_MAILLAGE(MAILLAGE=mesh1,
                    MODI_MAILLE=_F(OPTION = 'TRIA6_7',
                                   TOUT       = 'OUI',
                                   PREF_NOEUD = 'NY'),)
\end{lstlisting}

La plupart des commandes créant de nouvelles entités (noeud ou mailles) ont un mot-clef de type \texttt{PREF\_NOEUD} ou \texttt{PREF\_MAILLE} pour permettre à \CodeAster de créer les noms des nouvelles entités sans risquer de confusion avec celles déjà existantes.
Pour copier un maillage (dupliquer la structure de données), on peut utiliser la commande \CACopyObject. Par exemple:

\begin{lstlisting}
mesh2 = COPIER(CONCEPT= mesh1)
\end{lstlisting}

Ou même le détruire:
\begin{lstlisting}
DETRUIRE(CONCEPT=_F(NOM=mesh1))
\end{lstlisting}

\begin{Remark}
  Cette dernière commande doit être utilisée avec précaution. Dans le cas présenté, elle supprime l'objet mais pas le fichier attaché. Quand l'objet dépend d'autres objets, la commande n'est pas \emph{récursive}. Par exemple, le modèle dépend du maillage. Si vous supprimez le modèle, vous ne supprimez pas le maillage sur lequel il est défini. La commande est particulièrement utile lorsque l'on fait des boucles Python \CodeAster requiert qu'on détruise un objet avant de le recréer avec le même nom.
\end{Remark}

\subsection{Autres commandes de maillage}

Il existe d'autres commandes, plus anecdotiques comme \CAAssembleMesh pour manipuler les maillages (il est préférable de faire ces manipulations dans le mailleur) et \CADefineMesh qui sert à la définition des macro-éléments.
