\section{Les fonctions, les formules et les nappes}\label{Para:ObjetFonction}
\subsection{Définitions}

Ces objets sont très importants et sont utilisés aussi bien en entrée (accélérogramme pour le calcul sismique, courbe de traction pour la plasticité, etc.) qu'en post-traitement. Dans le vocabulaire de \CodeAster, on distingue trois objets:
\begin{itemize}
    \item La \emph{fonction} est définie sur un paramètre, elle est tabulée, c'est-à-dire que ses valeurs sont définies sur une liste de points (discrétisant le paramètre). Ce sont des fonctions au sens mathématique du terme;
    \item La \emph{nappe} est définie sur deux paramètres, elle est tabulée, c'est-à-dire que ses valeurs sont définies sur une liste de points (discrétisant les deux paramètres).
    \item La \emph{formule} est définie sur autant de paramètres que nécessaire, elle n'est pas tabulée et on la définit par une formule mathématique.
\end{itemize}

\subsection{Paramètres}

Ces trois objets sont définis par des \emph{paramètres} dont voici une liste (non exhaustive):
\begin{itemize}
    \item Les degrés de liberté (déplacements et rotations): \CAParaDispX, \CAParaDispY, \CAParaDispZ, \CAParaRotaX, \CAParaRotaY et \CAParaRotaY;
    \item Les quantités purement géométriques. L'abscisse curviligne est notée \CAParaCurvAbsc et les coordonnées géométriques sont \CAParaGeomX,\CAParaGeomY et \CAParaGeomZ;
    \item Les paramètres mécaniques comme la contrainte notée \CAParaStress et la déformation totale notée \CAParaStrain;
    \item Le temps \CAParaTime et la température \CAParaTemperature;
    \item La fréquence \CAParaFrequency et la pulsation \CAParaPulse;
\end{itemize}

\subsection{Les fonctions}

Une \emph{fonction} de \CodeAster est définie par une liste de valeurs réelles ou complexes (l'ordonnée) sur une liste de réels (l'abscisse, décrite par le \emph{paramètre}). Voir la figure \ref{Fig:Fonction}.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./chapitres/images/Fonction.png}
    \caption{Une fonction}
    \label{Fig:Fonction}
\end{figure}

Il s'agit bien d'une fonction au sens mathématique du terme. Les abscisses doivent être strictement croissantes:
\[x_1 < x_2 < ... < x_n\]
L'exemple de la figure \ref{Fig:FonctionNonFonction} n'est pas une fonction car pour une seule valeur de l'abscisse, il y a plusieurs valeurs de l'ordonnée (c'est ce qu'on appelle une application multi-valuée).

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.5]{./chapitres/images/FonctionNonFonction.png}
    \caption{Un exemple de ce qui n'est pas une fonction}
    \label{Fig:FonctionNonFonction}
\end{figure}

Toujours dans le cadre mathématique standard, une fonction a son domaine de définition (voir figure \ref{Fig:FonctionDomaineDefinition}).

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.5]{./chapitres/images/FonctionDomaineDefinition.png}
    \caption{Domaine de définition d'une fonction}
    \label{Fig:FonctionDomaineDefinition}
\end{figure}

Mais \CodeAster permet de \emph{prolonger} de domaine de définition de manière contrôlée. En effet on peut demander que la fonction soit prolongée (à droite du domaine de définition par \CAFunctionExtendRight ou à gauche \CAFunctionExtendLeft) par une valeur constante (\CAFunctionExtendConstant) ou par une extrapolation linéaire (\CAFunctionExtendLinear). Néanmoins, par défaut, la fonction n'est \emph{pas} prolongée (\CAFunctionExtendExclude). Voir sur la figure \ref{Fig:FonctionProlongement}.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.5]{./chapitres/images/FonctionProlongement.png}
    \caption{Extension du domaine de définition}
    \label{Fig:FonctionProlongement}
\end{figure}

Comme la fonction est tabulée (discrétisée), il faut être capable d'interpoler sa valeur lorsque le paramètre ne tombe pas exactement sur les valeurs définies. Pour cela, on fait soit une interpolation linéaire, soit une interpolation logarithmique. Pour une interpolation linéaire (voir figure \ref{Fig:FonctionInterpolation}):

\[y =  \frac{(y_1 - y_2)}{(x_1 - x_2)}   + y_1\]

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.4]{./chapitres/images/FonctionInterpolation.png}
    \caption{Interpolation d'une fonction}
    \label{Fig:FonctionInterpolation}
\end{figure}

Nous allons maintenant prendre l'exemple d'une fonction nommée \texttt{fonc}, à quatre valeurs réelles, dont le paramètre est le temps, le domaine de définition est $[1.,3.]$ dont on autorisera l'extension constante à droite (voir figure \ref{Fig:FonctionExemple}). Il s'agit donc de donner les couples de valeurs et d’utiliser la commande \CADefineFunction.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.5]{./chapitres/images/FonctionExemple.png}
    \caption{Exemple de définition d'une fonction}
    \label{Fig:FonctionExemple}
\end{figure}

Il y a deux façons de faire. la première consiste à donner les couples de valeurs de manière explicite:

\begin{lstlisting}
fonc=DEFI_FONCTION(NOM_PARA='INST',
		VALE=(	0. ,2. ,
			1. ,3. ,
			2. ,4. ,
			3. ,3. ,)
		PROL_GAUCHE='EXCLU',
		PROL_DROITE='CONSTANT')
\end{lstlisting}

Mais on peut utiliser également la définition par deux listes de réels:

\begin{lstlisting}
ABSC=DEFI_LIST_REEL(VALE=(1.,2.,3.,4.,))
ORDO=DEFI_LIST_REEL(VALE=(2.,3.,4.,3.,))
fonc=DEFI_FONCTION(
		NOM_PARA='INST',
		VALE_PARA=ABSC,
		VALE_FONC=ORDO,
		PROL_GAUCHE='EXCLU',
		PROL_DROITE='CONSTANT')
\end{lstlisting}

\subsection{Les formules}

Une \emph{formule} de \CodeAster est définie par une formule mathématique en fonction d'une liste de paramètres. Pour définir cette formule mathématique, on utilise le Python. On va simplement utiliser un exemple avec la commande \CADefineFormula:

\begin{lstlisting}
form = FORMULE(NOM_PARA='X',
               VALE= '''sin(X)''' )
\end{lstlisting}

La formule utilise la définition Python d'un sinus (via un \texttt{import math}). On remarque l'usage obligatoire des \emph{triple-quotes} qui permettent de définir une formule sur plusieurs lignes (standard Python). Bien entendu, on peut définir des formules aussi complexes que nécessaires, y compris en programmant une fonction Python. Par exemple:

\begin{lstlisting}
def HEAVISIDE(x) :
    if x<0.  : return 0.
    If x>=0. : return 1.
F_HVS = FORMULE( NOM_PARA = 'INST',
                 VALE     = 'HEAVISIDE(INST)')
\end{lstlisting}

Il est également possible d'encapsuler les formules:

\begin{lstlisting}
SIa = FORMULE(NOM_PARA='X',VALE='sin(X)')
SIb = FORMULE(NOM_PARA='X',VALE='X*SIa(X)')
\end{lstlisting}

Et d'avoir des formules à plusieurs paramètres:

\begin{lstlisting}
from math import pi
OMEGA = 30.
NAP = FORMULE(NOM_PARA = ('AMOR','FREQ'),
              VALE     =
     '''(1./((2.*pi*FREQ)**2 - OMEGA**2 )**2
        +(2.*AMOR*2.*pi*FREQ*OMEGA)**2)''')
\end{lstlisting}

Il faut noter que, contrairement aux fonctions et aux nappes, il n'y a pas de contrainte d'unicité ou de notion de domaine de définition. Il est possible de transformer une formule en fonction ou en nappe, grâce à l'usage de la commande \CAInterpolateFunction. Par exemple:

\begin{lstlisting}
SI   = FORMULE(NOM_PARA = 'INST',
               VALE     = '''sin(INST)''')
DEPI = 2.*pi
PAS0 = DEPI/200.
LI1  = DEFI_LIST_REEL(DEBUT = 0,
                      INTERVALLE=_F(JUSQU_A=DEPI, PAS=PAS0),)
SI1	 = CALC_FONC_INTERP(FONCTION    = SI,
                        LIST_PARA   = LI1,
                        PROL_GAUCHE ='EXCLU',
                        PROL_DROITE ='CONSTANT')
\end{lstlisting}

Bien entendu, seules les formules à un (respectivement deux) paramètre(s) peuvent être transformées en fonction (respectivement nappe). Le choix entre fonction et formule est à la discrétion de l'utilisateur, la plupart des commandes acceptant l'une ou l'autre. Il faut comprendre qu'une formule est plus précise (puisqu'elle est analytique) mais qu'elle est beaucoup moins rapide à l'usage (il faut faire une évaluation dans l'espace Python, ce qui peut être très coûteux si on le fait des milliards de fois ! ).

\subsection{Autres utilitaires}

On peut lire une fonction à partir d'un fichier par la commande \CAReadFunction, créer une fonction à partir d'un résultat par la commande \CAGetFunction, évaluer des caractéristiques (maximum, minimum, etc.) par la commande \CAInfoFunction, en faire des calculs plus compliqués (rms, FFT) par la commande \CAComputeFunction et, enfin, de les imprimer dans un fichier par la commande \CAPrintFunction.
Beaucoup d'exemples sont disponibles dans les documentations de ces commandes et dans le cas-test \texttt{zzzz100}.

\subsection{Entrer une fonction dans \AsterStudy}\label{Section:FunctionAsterStudy}

La commande \CADefineFunction permet d'entrer une fonction. Dans \AsterStudy, une petite interface graphiqeu va vous aider à définir et à visualiser la fonction.
Si on définit la fonction point par point avec \CAFunctionCoordinates, on obtient un tableau à deux colonnes: la première représente le \emph{paramètre} de la fonction et la second la \emph{valeur} de la fonction.
Les icônes \fbox{\includegraphics[scale=0.5]{./chapitres/images/icon_AsterStudy_func_add_row}}, \fbox{\includegraphics[scale=0.5]{./chapitres/images/icon_AsterStudy_func_remove_row}} et \fbox{\includegraphics[scale=0.5]{./chapitres/images/icon_AsterStudy_func_insert_row}} permettent, respectivement, d'ajouter une ligne, d'en enlever ou d'en insérer une.
L'icône \fbox{\includegraphics[scale=0.5]{./chapitres/images/icon_AsterStudy_func_plot}} donne une représentation graphique de la fonction.
