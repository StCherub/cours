\section{Les résultats}\label{Para:ObjetResultat}

Les \emph{résultats} sont des objets construits par les opérateurs de calcul et qui contiennent tous les résultats d'un calcul.

\subsection{Définition}

Les résultats sont caractérisés par leur type qui dépend de l'opérateur de calcul utilisés. Ils contiennent:
\begin{itemize}
  \item Des champs, rangés avec un numéro de rangement
  \item Des paramètres, stockant des informations en entrée du calcul, souvent aussi rangés avec un numéro de rangement;
  \item D'autres objets comme des \emph{tables} stockant des résultats sur un autre format que le champ;
\end{itemize}

\subsection{Typage des résultats}

Les résultats sont typés par l'opérateur de calcul. En voici une liste (non-exhaustive):
\begin{itemize}
  \item L'opérateur \CAStaticLinear produit un résultat de type \CAResultElastic;
  \item Tous les opérateurs de thermique (linéaires et non-linéaire) \CAThermic produisent un résultat de type \CAResultThermic;
  \item Les opérateurs de mécanique non-linéaires \CAStaticNonLinear et \CADynamicNonLinear produisent un résultat de type \CAResultMechanicNonLinear;
  \item Les opérateurs de calcul de modes produisent un résultat de type \CAResultEigen;
  \item Les opérateurs de calcul en dynamique vibratoire \CADynamicLinear produisent des résultats de type \CAResultDynamicTransient, \CAResultDynamicHarmonic ou \CAResultDynamicTransientGene;
\end{itemize}
Il n'existe pas d'opérateurs pouvant convertir les résultats d'un type à l'autre. Mais la plupart peuvent être construits à l'aide de la commande \CACreateResult.

\subsection{Le rangement des champs}

Les champs sont rangés suivant la logique de l'opérateur. Pour les opérateurs standards linéaires et non-linéaires (mécanique, thermique), on accède aux champs par l'instant (paramètre \CAResultAccessTime), sauf pour les opérateurs de calcul sur base harmonique (accès par la fréquence \CAResultAccessFrequency)
Pour les opérateurs modaux, on accède en général par le numéro du mode (paramètre \CAResultAccessMode) mais on peut aussi y accéder par la fréquence \CAResultAccessFrequency.

Dans tous les cas on peu toujours accéder au bon numéro de rangement par le paramètre \CAResultAccessStore.

Dans le cas d'un accès par un paramètre \emph{réel} comme la fréquence ou l'instant, il est parfois nécessaire d'utiliser des mots clefs comme \CAResultAccessCriterion et \CAResultAccessPrecision pour distinguer deux numéros de rangement. Certain des opérateurs manipulant ces objets sont aussi pourvus de mots-clefs d'accès à tous les numéros de rangement comme \CAResultAccessAllTime et \CAResultAccessAllStore.

A la fin d'un calcul, le fichier \texttt{.mess} contient un résumé du contenu des objets de type résultats. On y trouvera en particulier le nombre de numéros de rangement (répétés aussi à la fin des opérateurs de calcul), les champs et les paramètres stockés.

Pour enrichir un objet résultat de plusieurs champs, il suffit d'utiliser la commande \CAComputeField. Par exemple, la commande suivante calcule les déformations (champs \CAFieldStrainELNO et \CAFieldStrainELGA sur tous les numéros de rangement de l'objet résultat \texttt{EVOLNOLI}:

\begin{lstlisting}
EVOLNOLI=CALC_CHAMP(reuse =EVOLNOLI,
                    DEFORMATION = ('EPSI_ELGA','EPSI_ELNO'),
                    RESULTAT=EVOLNOLI,);
\end{lstlisting}

\subsection{Les paramètres}

Les paramètres stockés dans les résultats sont très variés et dépendent fortement du type.
A  la fin d'un calcul, le fichier \texttt{.mess} contient la liste des paramètres stockés. On peut également les imprimer en utilisant la commande \CAPrintResult au format \texttt{RESULTAT} ou \texttt{ASTER}.
On peut aussi récupérer la valeur des paramètres dans une table à l'aide de la commande \texttt{RECU\_TABLE}, par exemple:

\begin{lstlisting}
PILO = RECU_TABLE(
  CO       = RESU_NL,
  NOM_PARA = ('INST', 'ETA_PILOTAGE')
  );
\end{lstlisting}

A noter que l'accès aux données dans l'objet résultat (le rangement) utilise un paramètre. Dans notre exemple, la table \texttt{PILO} contiendra deux colonnes: la première donnera la liste des instants où sont rangés les résultats de  \texttt{RESU\TextUnderscore NL}, la seconde la valeur du coefficient de pilotage.

\subsection{Les tables}\label{Para:ResuTables}

Certains opérateurs de calcul non-linéaire produisent aussi des \emph{tables} (voir paragraphe \ref{Para:ObjetTable}). On peut les récupérer à l'aide de la commande \CAGetTable, par exemple:

\begin{lstlisting}
TABLE=RECU_TABLE(
  CO=RESU_NL,
  NOM_TABLE ='OBSERVATION',
);
\end{lstlisting}
