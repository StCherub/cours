\section{Les champs}\label{Para:ObjetChamp}

Les \emph{champs} sont des concepts fondamentaux dans \CodeAster, ils représentent la discrétisation des données sur le maillage. Ils sont donc à la fois attachés au maillage et au modèle.

\subsection{Définition}

Les champs sont caractérisés par:
\begin{itemize}
    \item Leur support de discrétisation: nœuds, éléments ou points d'intégration numérique (ou points de Gauss\footnote{Par abus de langage dans \CodeAster, on appelle \emph{points de Gauss} les points de quadrature numérique même lorsqu'il ne sont pas basés sur un schéma de Gauss !} );
    \item La quantité physique (ou \emph{grandeur}) qu'ils portent. Cette quantité pouvant être une liste de composantes (les trois déplacements ou les six contraintes en mécanique tridimensionnelle);
\end{itemize}
Par exemple, la description des coordonnées des nœuds dans le maillage est un \emph{champ} porté par les nœuds et portant trois composantes: \CAParaGeomX, \CAParaGeomY et \CAParaGeomZ.

\subsection{Règle de nommage}

En général, un champ est nommé par une règle de composition simple.
Les quatre premiers caractères désignent la grandeur (\CAFieldStress pour les contraintes,\CAFieldTemperature pour la température, \CAFieldInternalVariable pour les variables internes, \CAFieldDisplacement pour les déplacements), puis un caractère \texttt{\TextUnderscore}, puis le support de discrétisation (\CAFieldSupportNOEU, \CAFieldSupportELNO, \CAFieldSupportELEM ou \CAFieldSupportELGA).
Par exemple: \CAFieldStressELGA représente les contraintes aux points d'intégration.
Il existe des exceptions notables à ce nommage: les champs d'inconnues principaux, portés par les nœuds, sont nommés respectivement \CAFieldDisplacement (et non \CAFieldDisplacementNOEU) pour les déplacements, \CAFieldVelocity pour les vitesses (et non \CAFieldVelocityNOEU), \CAFieldAcceleration pour les accélérations (et non \CAFieldAccelerationNOEU) et \CAFieldTemperature pour les températures (et non \CAFieldTemperatureNOEU).
De même, par facilité, on rassemble dans ces champs des quantités qui n'ont pas la même nature. Par exemple, le champ \CAFieldDisplacement ne contient pas que les \emph{déplacements} des nœuds, mais aussi les rotations (pour les éléments de structure), les multiplicateurs de Lagrange (pour les conditions limites et le contact), la température et les pressions (pour les problèmes couplés en thermo-hydro-mécanique).

\subsection{Les supports physiques}

Les supports physiques sont au nombre de quatre, en deux catégories:
\begin{itemize}
    \item Les champs aux \emph{nœuds} ou \CAFieldNode, post-fixés par le terme \CAFieldSupportNOEU (sauf pour les exceptions déjà cités précédemment), donnent les valeurs aux nœuds du maillage. Ce sont donc des quantités continues d'un élément à l'autre par construction;
    \item Les champs aux \emph{éléments} ou \CAFieldElem, post-fixés, donnent les valeurs dans les éléments du maillage. Ce sont donc des quantités discontinues d'un élément à l'autre par construction;
\end{itemize}

Les champs aux éléments sont eux-mêmes de trois sous-types:
\begin{itemize}
    \item Les champs aux \emph{points d'intégration}, post-fixés par le terme \CAFieldSupportELGA sont portés par les points du schéma d'intégration utilisé dans l'élément fini;
    \item Les champs aux \emph{nœuds par élément}, post-fixés par le terme \CAFieldSupportELNO sont portés par les nœuds sur chaque élément;
    \item Les champs aux \emph{nœuds par élément}, post-fixés par le terme \CAFieldSupportELEM en général mais pas toujours, sont portés par l'élément (ils sont sont donc constants sur cet élément;
\end{itemize}

Il y a une différence fondamentale entre les champs aux nœuds \CAFieldNode et les champs aux nœuds \emph{par élément} \CAFieldELNO. Dans le premier cas, ce sont des quantités continues d'un élément à l'autre, dans l'autre ce n'est pas le cas. Les champs \CAFieldELNO sont généralement construit par interpolation via les fonctions de forme à partir des champs \CAFieldELGA.

\subsection{Principales commandes manipulant les champs}\label{Para:FieldCompute}

Les champs étant les principaux objets utilisés lors du calcul, on les retrouve dans les \emph{résultats} (voir paragraphe \ref{Para:ObjetResultat}) des opérateurs de calcul. Il est possible également de les manipuler en dehors de ces opérateurs de calcul.
La commande pour créer un champ (en dehors donc des opérateurs de calcul donc) est \CACreateField. Cette commande permet de construire quasiment n'importe quel type de champ. Elle est difficile à utiliser car elle est assez proche du code. Sa documentation regorge d'exemples.

La principale commande pour les utilisateurs est \CAComputeField. Cette commande permet de calculer facilement les quantités d'intérêts pour la modélisation, en particulier tout ce qui n'est pas nativement en sortie des opérateurs de calcul: contraintes de Von Mises, déformations, réactions d'appui, etc. Elle permet également de changer de support de discrétisation très facilement (par exemple passer d'un champ \CAFieldELGA à un champ \CAFieldNode). On en reverra l'usage dans le paragraphe \ref{Para:ObjetResultat} car cette commande travaille sur les résultats et non sur les champs \og isolés \fg{}.

De même la commande \CAFieldForStructural permet de faire du post-traitement sur les champs utilisés dans les éléments de structures (les plaques, les coques, les tuyaux, les poutres). Elles sont très utiles pour extraire les quantités sur les sous-couches d'un composite par exemple.

On peut lire un champ à partir d'un fichier (commande \CAReadField). Par contre pour l'imprimer, il faudra utiliser \CAPrintResult (il n'existe pas de commande \CAPrintField).

Enfin, la commande \CAProjectField permet de passer d'une discrétisation spatiale à l'autre, mais avec changement de topologie. Par exemple, d'un maillage grossier à un maillage fin.
