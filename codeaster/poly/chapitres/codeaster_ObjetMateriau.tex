
\subsection{Le matériau}\label{Para:ObjetMateriau}

Il faut prendre garde à ce que la définition des paramètres matériaux soit cohérente avec les calculs qui seront faits. Par exemple, la mécanique linéaire élastique a besoin du module de Young et du coefficient de Poisson. Si on réalise un calcul thermo-mécanique, il faudra ajouter le coefficient de dilatation thermique. La définition du matériau étant faite bien avant son affectation et son utilisation, les vérifications de cohérence sont tardives et les erreurs ne peuvent se voir qu'à l'exécution de la commande de calcul.
La commande suivante crée un matériau élastique (linéaire):
\begin{lstlisting}
steel=DEFI_MATERIAU(ELAS=_F(
                            E  = 2.1E5,
                            NU = 0.3,
                           ),
                   )
\end{lstlisting}
Cet opérateur est un exemple d'application des \emph{règles} qui sont définies dans le catalogue (et qu'on peut retrouver dans la documentation des commandes):
\begin{itemize}
        \item Il n'est possible de définir qu'un seul phénomène élastique: les mots-clefs facteurs \texttt{ELAS}, \texttt{ELAS\_ORTH}, \texttt{ELAS\_HYPER}, etc. s'excluent mutuellement. La commande suivante provoquera une erreur à l'analyse du fichier de commande:
              \begin{lstlisting}
steel=DEFI_MATERIAU(ELAS=_F(
                            E  = 2.1E5,
                            NU = 0.3,
                           ),
                    ELAS_HYPER=_F(
                            C01  = 1.0,
                            C10  = 1.0,
                            C20  = 1.0,
                            K    = 1.0,
                           ),
                   )
\end{lstlisting}
        \item Les paramètres élastiques respectent des domaines de définition physique: le coefficient de poisson \texttt{NU} est nécessairement compris dans l'intervalle $[-1,0.5]$ et le module de Young \texttt{E} est strictement positif.  La commande suivante provoquera une erreur à l'analyse du fichier de commande:
              \begin{lstlisting}
steel=DEFI_MATERIAU(ELAS=_F(
                            E  = -2.1E5,
                            NU = 0.8,
                           ),
                   )
\end{lstlisting}
        \item Quand on définit un matériau élastique isotrope linéaire, la théorie précise qu'il faut donner deux constantes physiques. Dans \CodeAster, c'est le coefficient de Poisson et le module de Young (ce pourrait être les deux coefficients de Lamé $\lambda$ et $\mu$). Ces deux mots-clefs sont donc obligatoires.  La commande suivante provoquera une erreur à l'analyse du fichier de commande:
              \begin{lstlisting}
steel=DEFI_MATERIAU(ELAS=_F(
                            NU = 0.8,
                           ),
                   )
\end{lstlisting}
              En effet, il manque la définition du module de Young \texttt{E}. Par contre, d'autres mots-clefs sont facultatifs et vont dépendre de la physique considérée. Par exemple, si on vaut faire un calcul thermo-élastique et donc considérer la dilatation thermique du matériau, il faut définir le coefficient de dilatation (\texttt{ALPHA} dans \CodeAster). On aura donc:
              \begin{lstlisting}
steel=DEFI_MATERIAU(ELAS=_F(
                            E  = 2.1E5,
                            NU = 0.3,
                            ALPHA = 1.6E-5,
                           ),
                   )
\end{lstlisting}
              C'est une situation où l'absence du paramètre ne peut provoquer d'erreur lors de l'analyse syntaxique du fichier. En effet, lors de la lecture de la commande \CADefineMaterial, on ne peut pas savoir si un calcul sera mécanique ou thermo-mécanique. L'erreur n'aura donc lieu qu'à l'exécution.
\end{itemize}

\begin{Remark}
        Il faut bien comprendre la différence entre le \emph{matériau} et le \emph{comportement}. En effet, ce n'est pas parce que vous avez défini une courbe de traction dans votre matériau que le calcul (non-linéaire) sera élasto-plastique par exemple. Il sera nécessaire de choisir le bon comportement. Il n'y a pas bijection stricte entre le matériau et le comportement. Des comportements différents utilisent parfois les mêmes données matériaux. La situation la plus courante étant que la définition des caractéristiques élastiques est systématiquement nécessaire, même lorsque le comportement est non-linéaire.
\end{Remark}
