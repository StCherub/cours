\section{Les tables}\label{Para:ObjetTable}

Les tables sont des objets très utiles en post-traitement. Elles sont conçues de manière à avoir des milliers de lignes pour quelques colonnes (attention à ne pas faire l'inverse, ce serait beaucoup moins efficace). Chaque colonne permet de définir une valeur, elle est donc typée. Elle peut aussi ne pas comporter de valeur pour certaines lignes (ce qui constitue alors une case vide).
Il existe trois types de tables différentes:
\begin{itemize}
    \item Les tables \og simples \fg{} dans lesquelles on peut avoir des valeurs de type réel, entier ou des chaînes de caractères (de longueur $8$, $16$ ou $24$);
    \item Les tables de \og fonctions \fg{} dans lesquelles les valeurs sont des fonctions;
    \item Les tables de \og containers \fg{} dans lesquelles les valeurs sont d'autres objets de \CodeAster ;
\end{itemize}

\subsection{Manipulations de base}

Les tables peuvent être créés ex-nihilo, à partir d'autres objets ou en sortie de certaines commandes.
Pour créer une table ex-nihilo, on emploie la commande \CACreateTable:

\begin{lstlisting}
table=CREA_TABLE(LISTE=(
                    _F(PARA='ABSC_CURV',
                        LISTE_R=(1.0,2.0,3.0,4.0,),),
                    _F(PARA='--',
                        LISTE_K=('AA','BB','CC','DD',),),
                    _F(PARA='DZ-CFP-TMOY',
                        LISTE_R=(1.0,2.0,3.0,4.0,),),),);
\end{lstlisting}

Dans cet exemple, on crée une table à trois colonnes et quatre lignes. Chaque colonne est typée (mot-clef \texttt{LISTE\_*}) et nommée (mot-clef \texttt{PARA}).
On peut également, avec la même commande, créer une table à partir d'un objet résultat:

\begin{lstlisting}
table = CREA_TABLE(RESU=_F(RESULTAT  = EVOL,
                          INST      = 2.,
                          NOM_CHAM  = 'SIEF_ELGA',
                          NOM_CMP   = 'SIZZ',
                          GROUP_MA  = 'MRELEVE',),)
\end{lstlisting}
qui extrait du résultat \texttt{EVOL}, la liste des contraintes (composante \texttt{SIZZ}) sur un groupe de maille déterminé et pour un instant donné. Le champ étant de type \CAFieldSupportELGA, la table contiendra autant de ligne que de points de Gauss et trois colonnes. Dans notre cas, si le groupe de maille ne contient qu'un triangle, il n'y aura qu'un point de Gauss.

On peut enfin créer une table à partir d'un objet fonction.

Les commandes \CAReadTable et \CAPrintTable gèrent les tables en entrée/sortie par des fichiers.

Par exemple, la commande suivante lit un fichier (sur l'unité logique $38$, voir \ref{Para:MaillageLecture}). On a fixé les séparateurs de colonnes par le caractère tabulation:
\begin{lstlisting}
table = LIRE_TABLE(UNITE     = 38,
                   FORMAT     = 'LIBRE',
                   SEPARATEUR = '\t')
\end{lstlisting}

\subsection{Récupération à partir d'autres objets}

La commande \CAGetTable permet d'extraire une table d'un objet résultat (voir l'exemple au paragraphe \ref{Para:ResuTables} ).

Enfin, la commande \CAExtractTable permet d'extraire une table à partir d'une autre (en filtrant ligne et colonne) mais surtout permet la récupération des tables container produites par certaines commandes.
Par exemple, la commande \CACompute permet d'intégrer une loi de comportement non-linéaire et sort, entre autres, contraintes, variables internes et matrices tangente dans une table container. Pour récupérer ces objets, on utilise donc \CAExtractTable:
\begin{lstlisting}
SIGM=EXTR_TABLE(TYPE_RESU='CHAM_GD_SDASTER',
                TABLE=CONT,
                NOM_PARA='NOM_SD',
                FILTRE=_F(NOM_PARA='NOM_OBJET',
                           VALE_K='SIEF_ELGA'),)
\end{lstlisting}
Cette commande extrait le champ des contraintes calculées.

\subsection{Post-traitement de résultats}

On peut produire une table par extraction de valeurs dans un résultat de calcul avec l'opérateur \CAPostTable. Par exemple:

\begin{lstlisting}
SIGM=POST_RELEVE_T(ACTION=_F(OPERATION='EXTRACTION',
                             INTITULE='table A',
                             RESULTAT=RESU,
                             NOM_CHAM='SIGM_NOEU',
                             GROUP_NO='A',
                             NOM_CMP='SIXX',),);
\end{lstlisting}

Cette commande va extraire la composante \texttt{SIXX} du tenseur des contraintes \texttt{SIGM\TextUnderscore NOEU} sur le \CAGroupNode de nom \texttt{A} dans le résultat \texttt{RESU}. C'est une table avec plusieurs lignes si le \CAGroupNode contient plusieurs noeuds et/ou que le résultat contient plusieurs pas de temps.

\begin{Remark}
    Il va de soit que le champ \texttt{SIGM\TextUnderscore NOEU} doit avoir été créé dans la structure de données résultats (voir \ref{Para:FieldCompute})
\end{Remark}

\subsection{Autres commandes}

La commande \CAComputeTable permet de manipuler les tables, les trier, renommer ou extraire des colonnes, en supprimer, faire des statistiques. Cette commande constitue un véritable tableur en Python !
