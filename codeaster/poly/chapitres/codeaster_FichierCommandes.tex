\section{Le fichier de commandes}\label{Para:FichierCommandes}

\subsection{Principes généraux}

Le fichier de commande définit les différents paramètres de calcul et les options. Parmi les différentes actions nécessaires au calcul, on peut citer :
\begin{itemize}
        \item La lecture du maillage;
        \item La définition des éléments finis: choix de la physique à traiter (mécanique, thermique, acoustique), équations et degrés de liberté associés;
        \item La définition des propriétés des matériaux (comme le module d'élasticité ou la masse volumique par exemple) puis leur assignation sur une partie du maillage;
        \item La définition des conditions limites et des chargements;
        \item La sélection de l'algorithme de résolution;
        \item Les post-traitements;
\end{itemize}
Pour définir l'ensemble de ces actions, \CodeAster utilise la notion de \emph{fichier de commandes}. Ce fichier est au format texte (donc lisible par n'importe quel éditeur de texte) et il est une succession de commandes \CodeAster. L'ensemble est à la syntaxe Python et permet de mélanger commandes de calcul proprement dites et commandes Python.
La syntaxe des commandes de \CodeAster peut être très complexe. Ainsi, même s'il est toujours possible de rédiger le fichier de commandes \og à la main \fg{} avec un éditeur de texte quelconque, \CodeAster et surtout \SalomeMeca vous proposent des utilitaires plus ergonomiques.

\subsection{Syntaxe}

Le fichier de commandes de \CodeAster est au format Python. Environ 220 commandes spécifiques à \CodeAster sont ajoutées au Python et permettent de définir et de réaliser un cas de calcul.
Il existe quatre grandes catégories de commandes:
\begin{itemize}
        \item Les \emph{opérateurs} produisent des \emph{données} qui sont ensuite réutilisables dans d'autres commandes, ces données sont appelées \og concepts \fg{}. Chaque opérateur ne peut produire qu'un seul concept;
        \item Les \emph{procédures} manipulent des données qui ne sont pas réutilisables par la suite dans d'autres commandes mais dans d'autres formats (typiquement: des fichiers externes). Une procédure ne crée aucun concept;
        \item Les commandes qui ne produisent \emph{aucune donnée} utilisable directement par l'utilisateur mais qui servent à structurer le fichier de commandes;
        \item Les \emph{macro-commandes} sont des opérateurs qui permettent de reproduire une succession de commandes \CodeAster, avec d'éventuelles opérations supplémentaires en Python. Ce sont les seules commandes qui peuvent produire plusieurs concepts en sortie.
\end{itemize}

\subsection{Concepts}

Les \og concepts \fg{} représentent des données que l’utilisateur peut manipuler. Ces concepts sont typés au moment de leur création et ne pourront être utilisés comme argument que si le type correspond. Cette vérification de la cohérence (typage des concepts) est faite au moment de l'analyse syntaxique du fichier de commande.
Les procédures et les opérateurs échangent donc les informations nécessaires et des valeurs par l’intermédiaire des concepts nommés.
Les noms de concepts ne doivent pas dépasser huit caractères. Les caractères alphanumériques sont licites (lettres minuscules et majuscules, chiffres non placés en première position) ainsi que le underscore \_. La casse est importante : les concepts \texttt{MAIL} et \texttt{Mail}  pourront être utilisés dans un même fichier de commande et seront considérés comme différents.

La réutilisation d'un concept existant comme concept produit, n'est possible que pour les opérateurs qui le permettent. Lorsque l'on utilise cette possibilité (concept dit \emph{réentrant}), la commande utilise alors le mot-clé réservé \texttt{reuse}.
L'analyse syntaxique du fichier (la conformité des commandes à la description du langage) se fait à l'excution.

Sauf de très rares exceptions, les concepts ne sont pas manipulables depuis l'espace Python et sont des objets structurés en Fortran, dans le formalisme interne de \CodeAster. La seule manière de manipuler ces données est donc d'utiliser les commandes de \CodeAster.

\subsection{Commandes}

Pour expliquer la notion de commande, on va utiliser un exemple. Cette première commande permet de lire un maillage sauvegardé au format MED et de le sauvegarder dans un concept nommé \texttt{mail}:
\begin{lstlisting}
mesh = LIRE_MAILLAGE(FORMAT='MED')
\end{lstlisting}
Le concept \texttt{mesh} en sortie de la commande est nommé à gauche du signe \texttt{=}, puis on trouve la commande (\CAReadMesh) avec des options. Dans notre exemple, l'option \texttt{FORMAT='MED'} est ce qu'on appelle un \emph{mot-clef simple}. Ce mot-clef est typé (il attend des données dont la nature est précisée dans la documentation).

Voici un deuxième exemple de commande un peu plus complexe:
\begin{lstlisting}
steel = DEFI_MATERIAU(ELAS=_F(E=205000.E6, NU=0.3))
\end{lstlisting}

Cette commande défini un matériau \emph{élastique} grâce à un \emph{mot-clef facteur} défini par la chaîne \texttt{\_F (...)}. Le mot-clef facteur permet de rassembler plusieurs \emph{mots-clefs simples} sous un taxon unique. Dans notre cas, un matériau élastique (isotrope) est nécessairement défini par son module de Young (mot-clef simple \texttt{E=205000.E6} ) et son coefficient de Poisson (mot-clef simple \texttt{NU=0.3}).

Toutes ces commandes ont des règles syntaxiques précises décrites dans la documentation. Par exemple, on impose qu'un matériau élastique soit défini par le module de Young et le coefficient de Poisson. Si on oublie un des deux mots-clefs, il y aura une erreur lors de l'analyse syntaxique. D'autres règles sont applicables: le module de Young doit être un réel strictement positif et le coefficient de Poisson est compris entre $-1$ et $0,5$.

Nous pouvons avoir des mots-clefs facteurs répétables. Par exemple:
\begin{lstlisting}
chmat = AFFE_MATERIAU(AFFE=(
                            _F(GROUP_MA='bas',MATER = steel1 ),
                            _F(GROUP_MA='haut',MATER = steel2 ),
                           ),
                     )
\end{lstlisting}

Cette commande \emph{affecte} deux matériaux: le premier (\texttt{steel1}) sur un groupe de mailles \CAGroupElem défini dans le maillage sous le nom \texttt{'bas'} et le second (\texttt{steel2}) sur un groupe de mailles \CAGroupElem défini dans le maillage sous le nom \texttt{'haut'}. On voit que le mot-clef facteur \CAAffect est répété deux fois (prenez garde aux parenthèses avant la chaîne \texttt{\_F (...)} .
Par cet exemple, on a également introduit d'autres nouveautés:
\begin{itemize}
        \item Un mot-clef simple de type texte (\CAGroupElem), le nom du groupe de mailles);
        \item Un mot-clef simple de type concept (\texttt{MATER}), qui attend un concept créé par une autre commande (dans notre cas, venant de \CADefineMaterial );
\end{itemize}

Un mot-clef simple n'est pas répétable. Par contre, il peut admettre une série de valeurs. Par exemple, un vecteur:
\begin{lstlisting}
weight =AFFE_CHAR_MECA(MODELE=model,
            PESANTEUR = _F(
                 GRAVITE=9.81,
                 DIRECTION=(0.,0.,-1.,),
                        ),
        )
\end{lstlisting}

La commande \CAAffectLoad permet de définir un chargement. Dans notre cas, on défini un champ de \texttt{PESANTEUR} dont la gravité vaut $9,81$\footnote{On n'a pas précisé volontairement l'unité de l'accélération de la pesanteur car nous verrons au paragraphe §\ref{Para:UnitesPhysiques} que \CodeAster ne gère pas les unités lui-même.} et dont la direction est donnée par le vecteur \texttt{DIRECTION} qui a trois composantes $(0.,0.,-1.,)$ indiquant que la verticale est suivant l'axe $OZ$ et la direction vers le bas.

Nous finissons ce paragraphe par trois commandes. La première illustre les opérateurs ne produisant pas de concept:
\begin{lstlisting}
IMPR_RESU(FORMAT='MED', RESU = _F(RESULTAT=RESU))
\end{lstlisting}
Cette commande va écrire les résultats dans un fichier au format MED.

Les deux suivantes sont très importantes: elles définissent le début et la fin des commandes de \CodeAster dans un fichier:
\begin{lstlisting}
DEBUT()
\end{lstlisting}
\begin{lstlisting}
FIN()
\end{lstlisting}
Toute commande de \CodeAster utilisée en dehors de ces bornes provoquera une erreur car elles ne seront pas comprises puisque le fichier est à la syntaxe Python. Ce sont deux commandes qui ne produisent aucun concept.

Il est très important de comprendre le concept de \emph{séquentialité} des commandes. Pour pouvoir utiliser un concept, il faut qu'il ait été défini \emph{avant} qu'il soit utilisé. Par exemple le fichier suivant va provoquer une erreur:

\begin{lstlisting}
DEBUT()
model = AFFE_MODELE(MAILLAGE=mesh,
                    AFFE=_F(TOUT='OUI',
                            PHENOMENE='MECANIQUE',
                            MODELISATION='C_PLAN',),);
mesh  = LIRE_MAILLAGE(FORMAT='MED',);
FIN()
\end{lstlisting}

En effet, le concept \texttt{mesh} que l'on veut utiliser dans la commande \CAAffectModel est défini \emph{après} cette dernière !

\subsection{Règle de surcharge}\label{Para:RegleSurcharge}

Une règle de surcharge utilisable, notamment pour toutes les opérations d’affectation, a été ajoutée aux règles d’utilisation d’un mot-clé facteur avec plusieurs listes d’opérandes :
\begin{itemize}
        \item Les affectations se font en superposant les effets des différents mots-clés;
        \item En cas de conflit, le dernier mot-clé l’emporte sur les précédents.
\end{itemize}

Exemple : on souhaite affecter différents matériaux \texttt{MAT1}, \texttt{MAT2} et \texttt{MAT3} à certaines mailles :

\begin{lstlisting}
mater = AFFE_MATERIAU(
           MAILLAGE = MAIL
           AFFE     = (_F( TOUT = 'OUI', MATER = MAT1 ),
                       _F( GROUP_MA = 'mail2', MATER = MAT2 ),
                       _F( GROUP_MA = 'mail1', MATER = MAT3 ),
                       _F( MAILLE = ( 'm7','m8' ), MATER = MAT3 )
                      )
)
\end{lstlisting}

La suite des opérations est la suivante:
\begin{itemize}
        \item On commence par affecter le matériau \texttt{MAT1} à toutes les mailles.
        \item On affecte ensuite le matériau \texttt{MAT2} au groupe de mailles \texttt{mail2} qui contient les mailles \texttt{m8}, \texttt{m9} et \texttt{m10}.
        \item On affecte le matériau \texttt{MAT3} au groupe de mailles \texttt{mail1} (\texttt{m5}, \texttt{m6} et \texttt{m7}).
        \item On affecte le matériau \texttt{MAT3} aux mailles \texttt{m7} et \texttt{m8}, ce qui est source de conflit puisque la maille \texttt{m7} fait déjà partie du groupe \texttt{mail1}.
\end{itemize}
La règle de surcharge sera alors appliquée et on obtiendra finalement le champ de matériau suivant :

\begin{lstlisting}
MAT1 : mailles m1 m2 m3 m4
MAT2 : mailles m9 m10
MAT3 : mailles m5 m6 m7 m8
\end{lstlisting}

\subsection{Règle de rémanence}\label{Para:RegleRemanence}

La règle de surcharge précédente doit être complétée par une autre règle pour préciser ce qui se passe lorsqu'on peut affecter plusieurs quantités pour chaque occurrence d'un mot-clé facteur.

Soit par exemple :
\begin{lstlisting}
CHMEC1=AFFE_CHAR_MECA(
        MODELE=MO,
        FORCE_INTERNE=(
                 _F( TOUT = 'OUI', FX = 1. ),
                 _F( GROUP_MA = 'GM1', FY = 2.),
 ))
\end{lstlisting}
La règle de surcharge nous dit que la deuxième occurrence de \texttt{FORCE\_INTERNE} surcharge la première.
Mais que vaut \texttt{FX} sur une maille appartenant à \texttt{GM1} ? A-t-il été effacé par la deuxième occurrence ?
Si la seule règle de surcharge est appliquée, \texttt{FX} n'est pas défini sur \texttt{GM1}.
La règle de rémanence permet de conserver la valeur de \texttt{FX}.
Si la règle de rémanence est appliquée, \texttt{FX} conserve la valeur affectée au préalable. Tous les éléments du modèle ont une valeur pour \texttt{FX} et les éléments de \texttt{GM1} pour une valeur pour\texttt{FX} et \texttt{FY}.
