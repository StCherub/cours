
\chapter{Calcul IFS d'un réservoir parallélépipédique}

Ce TP propose de calculer les modes propres d'un réservoir parallélépipédique en prenant en compte l'interaction entre le fluide et la structure. Le maillage est fourni.

\section{Définition du problème}
\subsection{Géométrie}

L'étude concerne un réservoir mince en acier rempli d'eau. Sa géométrie est simple. Il s'agit d'un simple parallélépipède de hauteur $h=\unit{5}{\meter}$ et de coté $l=\unit{10}{\meter}$. L'épaisseur du réservoir est $e=\unit{2.5}{\centi\meter}$. Le réservoir est fixé (encastré) sur le sol (voir la figure \ref{Fig:DefinitionGeometrie}).

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.4]{./chapitres/ReservoirCube_IFS/images/ReservoirCube_IFS_GeometrieDefinition.png}
    \caption{Définition de la géométrie du problème}
    \label{Fig:DefinitionGeometrie}
\end{figure}

Comme d'habitude en dynamique, on ne fait pas d'hypothèses de symétrie car il est difficile de prévoir \emph{a priori} les symétries des modes calculés.

\subsection{Propriétés des matériaux}

On considère un matériau élastique linéaire isotrope. les propriétés du réservoir sont celles de l'acier $A42$:
\begin{itemize}
    \item Module d’Young $E=\unit{200}{\giga\pascal}$;
    \item Coefficient de Poisson $\nu=0,3$;
    \item Masse volumique $\rho_b=\unit{8000}{\kilogrampercubicmetre}$.
\end{itemize}
L'eau a les caractéristiques suivantes:
\begin{itemize}
    \item Vitesse des ondes dans l'eau $c=\unit{1400}{\meter\per\second}$;
    \item Masse volumique $\rho=\unit{1000}{\kilogrampercubicmetre}$.
\end{itemize}

\section{Réalisation des calculs}
\subsection{Réalisation du calcul sans eau}

Dans un calcul complexe, il est toujours préférable de vérifier la mise en données étape par étape. Donc, pour commencer, on va réaliser le calcul du réservoir sans eau, simplement encastré en bas.

Lancer le module \AsterStudy de la plate-forme \SalomeMeca. Puis en colonne gauche, cliquer sur l'onglet (à gauche) \SAAWCaseView et l'onglet (en haut) \SAAWDataSettings.
On peut céer une nouvelle étape de calcul \emph{Stage} avec \SAKeyRightMouse sur le cas de calcul (nommé \texttt{CurrentCase} par défaut), puis \SAAMOperationsAddStage. On définit le fichier de commandes du cas de calcul.
Certaines commandes ne seront accessibles que par l'icône \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_ShowAllCommands}}. La fenêtre \ref{Fig:AllCommands} s'ouvre alors et vous pouvez chercher les commandes nécessaires.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./images/win_AsterStudy_AddCommand}
    \caption{Liste de toutes les commandes}
    \label{Fig:AllCommands}
\end{figure}

\subsubsection{Données globales du calcul}

Dans un premier temps, on définit les données globales du calcul:
\begin{itemize}
    \item Lire le maillage : commande \CAReadMesh. Dans le menu définissant le maillage d'origine, il suffit d'aller choisir celui correspondant au maillage de \emph{tous} les objets dans le module \SAModuleMesh ou de lire le fichier;
    \item Créer le matériau pour le réservoir: commande \CADefineMaterial. Matériau élastique linéaire et isotrope sur le réservoir;
    \item Affecter le matériau pour le réservoir: commande \CAAffectMaterial. Même matériau sur tout le réservoir;
    \item Définir les éléments finis utilisés : commande \CAAffectModel. Ce sont des éléments DKT (soit \CAModelPlateDKT), sur le groupe \texttt{Reservoir}. N'appliquez aucun modèle sur le reste du maillage pour l'instant;
    \item Définir les caractéristiques élémentaires (épaisseur de la plaque) avec la commande \CAAffectElemProperties puis \CAAffectElemPropertiesShell;
\end{itemize}

\subsubsection{Vérification}

On va faire une vérification utile et aisée à mettre en œuvre: "peser" la structure pour vérifier que tout est correct. Le réservoir est constitué de quatre rectangles de $\unit{50}{\square\metre}$ et d'un rectangle de $\unit{100}{\square\metre}$. Les plaques sont en acier et d'épaisseur $e=\unit{2.5}{\centi\meter}$, ce qui fait un poids total de: $M_r = \unit{60000}{\kilogram}$.

Voici comment faire cette vérification.
\begin{itemize}
    \item Ajouter un post-traitement avec la commande \CAComputeElem: choisir l'option \CAComputeElemMass, puis faire le calcul sur \emph {tout le maillage}. Nommer la table \texttt{tablmass} (par exemple);
    \item  Ne pas oublier de renseigner le \CAModel, le champ de matériau \CAFieldMaterial et les caractéristiques élémentaires \CAFieldElemProperties;
    \item Ajouter la commande \CAPrintTable pour créer le fichier \texttt{masse.txt} (par exemple).
\end{itemize}

Pour lancer le calcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}.

Si tout va bien, un fichier \texttt{masse.txt} sera créé, vous pouvez l'éditer:
\begin{lstlisting}
##ASTER 13.04.00 CONCEPT masse CALCULE LE 03/01/2018 A 18:55:01 DE TYPE
#TABLE_SDASTER
LIEU                     ENTITE   MASSE        CDG_X        CDG_Y        CDG_Z        IX_G         IY_G         IZ_G         IXY_G        IXZ_G        IYZ_G        IX_PRIN_G    IY_PRIN_G    IZ_PRIN_G    ALPHA        BETA         GAMMA
Plaque                   GROUP_MA  6.00000E+04  5.00000E+00  5.00000E+00  1.66667E+00  1.00000E+06  1.00000E+06  1.66667E+06 -9.11768E-11 -1.88379E-10 -3.37081E-11  1.00000E+06  1.00000E+06  1.66667E+06  9.00000E+01 -0.00000E+00  1.80000E+02
\end{lstlisting}
Il est un peu difficile à lire car il contient beaucoup de colonnes: la masse, les moments principaux d'inertie et d'autres choses. Ce qui nous intéresse est le premier chiffre: $6.0000E+04$. C'est bien la masse du réservoir.

\subsubsection{Calcul des modes}

\begin{itemize}
    \item Définir les conditions limites : commande \CAAffectLoad et \CAEnforceDOF. Il y a un encastrement sur le groupe de mailles \texttt{Encas}. Ne pas oublier qu'il y a six degrés de liberté par nœud;
    \item Construire les matrices élémentaires de masse : commande \CACalcElemMatr. Pour la masse, il faut choisir \CAOptionMass dans \CAOption. Ne pas oublier de renseigner le \CAModel, le champ de matériau \CAFieldMaterial et les caractéristiques élémentaires \CAFieldElemProperties;
    \item Construire les matrices élémentaires de rigidité : commande \CACalcElemMatr. Pour la rigidité, il faut choisir \CAOptionRigidity dans \CAOption. Ne pas oublier de renseigner le \CAModel, le champ de matériau \CAFieldMaterial et les caractéristiques élémentaires \CAFieldElemProperties et les chargements de Dirichlet (pour calculer la matrice $[B]$);
    \item Créer la numérotation avec la commande \CANumbering. Les conditions limites étant dualisées, il faut numéroter les équations correspondant aux multiplicateurs de Lagrange, utilisez donc le mot-clef \CANumberingMatr et non \CANumberingModel;
    \item Assembler la matrice de masse : commande \CAAsseElemMatr sur les matrices élémentaires de masse et la numérotation précédemment créée;
    \item Assembler la matrice de rigidité : commande \CAAsseElemMatr sur les matrices élémentaires de rigidité et la numérotation précédemment créée;
    \item Calcul des vecteurs et valeurs propres avec \CAEigenSolver: renseignez \CAMatrixMass et \CAMatrixRigidity. Laisser les autres options par défaut;
    \item Impressions des résultats au format \CAFileMED: commande \CAPrintResult. Indiquez l'emplacement du fichier.
\end{itemize}

Pour lancer le calcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}.
Il faut penser à rafraîchir l'état du calcul par le bouton \SAAWHVRefresh (vous pouvez aussi passer en mode automatique en sélectionnant la fréquence de rafraîchissement dans le bouton \SAAWHVAutoRefresh ).
Pour avoir les fréquences propres, vous devez ouvrir le fichier \texttt{message} en cliquant sur le bouton \emph{Show message file}.
Déroulez le fichier jusqu'à trouver la commande \CAEigenSolver. Vous trouverez les fréquences (voire tableau \ref{Tab:FrequencesReservoir}).

\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.2\textwidth}|}
        \hline
        Numéro d'ordre & Fréquence              \\
        \hline
        1              & \unit{1.37046}{\hertz} \\
        \hline
        2              & \unit{1.57773}{\hertz} \\
        \hline
        3              & \unit{1.57773}{\hertz} \\
        \hline
        4              & \unit{1.86719}{\hertz} \\
        \hline
        5              & \unit{3.04554}{\hertz} \\
        \hline
        6              & \unit{3.58451}{\hertz} \\
        \hline
        7              & \unit{3.58451}{\hertz} \\
        \hline
    \end{tabular}
    \caption{\label{Tab:FrequencesReservoir} Premières fréquences}
\end{table}

\subsection{Réalisation du calcul avec eau}

On va maintenant ajouter l'eau.
Il suffit de compléter le fichier précédent en définissant le fluide et l'interface fluide-structure:

\begin{itemize}
    \item Créer un matériau pour le fluide : commande \CADefineMaterial. Matériau fluide \CADefineMaterialFluid, la vitesse du son est \CADefineMaterialFluidSpeed (pas \CADefineMaterialFluidSpeedC !) et l'accélération de la pesanteur (pour la surface) est donnée dans \CADefineMaterialFluidGravity;
    \item Affecter le matériau pour le fluide: compléter la commande \CAAffectMaterial précédente en affectant les groupes pour le fluide et pour l'interface fluide-structure avec le matériau précédent;
    \item Compléter les éléments finis utilisés : commande \CAAffectModel. Pour le fluide, il faut affecter \CAModelFluidThreeDim sur le groupe de mailles du fluide et \CAModelFSIThreeDim pour le groupe \texttt{Interface};
    \item Le modèle ayant changé , il faut redéfinir les conditions limites et les caractéristiques des coques;
    \item Construire les matrices élémentaires de rigidité : commande \CACalcElemMatr. Pour la rigidité, il faut choisir \CAOptionRigidity dans \CAOption. Ne pas oublier de renseigner le \CAModel, le champ de matériau \CAFieldMaterial et les caractéristiques élémentaires \CAFieldElemProperties et les chargements de Dirichlet (pour calculer la matrice $[B]$);
    \item Créer la numérotation avec la commande \CANumbering. Les conditions limites étant dualisées, il faut numéroter les équations correspondant aux multiplicateurs de Lagrange, utilisez donc le mot-clef \CANumberingMatr et non \CANumberingModel;
    \item Assembler la matrice de masse : commande \CAAsseElemMatr sur les matrices élémentaires de masse et la numérotation précédemment créée;
    \item Assembler la matrice de rigidité : commande \CAAsseElemMatr sur les matrices élémentaires de rigidité et la numérotation précédemment créée;
    \item Calcul des vecteurs et valeurs propres avec \CAEigenSolver: renseignez \CAMatrixMass et \CAMatrixRigidity. Laisser les autres options par défaut;
    \item Impressions des résultats au format \CAFileMED: commande \CAPrintResult. Indiquez l'emplacement du fichier.
\end{itemize}

\begin{mdframed}[outerlinecolor=black,outerlinewidth=2pt,middlelinewidth=3pt,roundcorner=10pt]
    \emph{ATTENTION !} Il faut bien sélectionner le bon modèle, le bon champ de matériau (ceux avec l'eau et non celui du réservoir vide) et donc tous les concepts en découlant: caractéristiques de coque, chargement, nuémrotation et matrices.
\end{mdframed}

Le solveur modal échoue. Pourquoi ?

Relancer le calcul en changeant les options du solveur modal pour calculer une bande de fréquences (par exemple entre 1 et 5 Hz). Dans la commande \CAEigenSolver:
\begin{itemize}
    \item Renseignez \CAMatrixMass et \CAMatrixRigidity;
    \item Choisir l'option \CAEigenSolverCalcBand, puis renseignez les valeurs de l'intervalle dans \CAEigenSolverCalcFreq.
\end{itemize}

Comment sont modifiées les fréquences propres ?

\subsection{Réalisation du calcul avec eau et surface pesante}

Enfin, on peut ajouter l'effet de la gravité pour calculer les modes de ballottement du fluide. Pour cela, les modifications sont minimes:
\begin{itemize}
    \item Affecter le matériau pour la surface: compléter la commande \CAAffectMaterial précédente en affectant le groupe \texttt{Surface} avec le matériau défini pour le fluide;
    \item Compléter les éléments finis utilisés : commande \CAAffectModel. Pour la surface, il faut affecter \CAModelFluidGravity sur le groupe de mailles \texttt{Surface};
    \item Calcul des vecteurs et valeurs propres avec \CAEigenSolver: renseignez \CAMatrixMass et \CAMatrixRigidity;
    \item Impressions des résultats au format \CAFileMED: commande \CAPrintResult. Indiquez l'emplacement du fichier.
\end{itemize}

\begin{mdframed}[outerlinecolor=black,outerlinewidth=2pt,middlelinewidth=3pt,roundcorner=10pt]
    \emph{ATTENTION !} Il faut bien sélectionner le bon modèle, le bon champ de matériau (ceux avec l'eau et non celui du réservoir vide) et donc tous les concepts en découlant: caractéristiques de coque, chargement, nuémrotation et matrices.
    Ne pas oublier de renseigner l'accélération de la pesanteur par \CADefineMaterialFluidGravity dans le matériau de l'eau.
\end{mdframed}

Comment sont modifiées les fréquences propres ?
