\chapter{Calcul modal}

On propose de calculer les modes propres de la plaque encastrée à une extrémité.

\section{Spécificités du calcul}

Dans \CodeAster, la plupart des commandes de calcul (\CAStaticLinear, \CAStaticNonLinear, \CADynamicNonLinear, \CAThermic, \CAThermicLinear, ...) sont des commandes \emph{monolithiques}, c'est-à-dire que l'ensemble des opérations de calcul (préparation des vecteurs et matrices élémentaires, choix de la numérotation des inconnues, assemblage des éléments finis, résolution du système linéaire, etc.) sont réalisées dans la commande de manière automatique.
Il est possible de procéder de manière plus élémentaire, en traitant ces opérations de manière indépendante, par des commandes spécifiques. C'est ce qu'on appelle les commandes \emph{éclatées}. La dynamique fait grand usage de ce mode car il arrive fréquemment qu'on doive modifier les matrices de manière indépendante.
Il y a sept grandes commandes qui permettent de réaliser ces opérations:
\begin{itemize}
    \item \CACalcElemVect: calcule les vecteurs élémentaires, en particuliers ceux relatifs aux chargements;
    \item \CACalcElemMatr: calcule les matrices élémentaires (rigidité, masse, amortissement);
    \item \CANumbering: prépare la numérotation des inconnues. Cette opération est nécessaire pour \emph{assembler} les vecteurs et matrices élémentaires;
    \item \CAAsseElemVect: assemble des vecteurs élémentaires avec une numérotation donnée;
    \item \CAAsseElemMatr: assemble des matrices élémentaires avec une numérotation donnée;
    \item \CAEigenSolver: calcule valeurs et vecteurs propres;
    \item \CAFactor: factorise une matrice assemblée (c'est-à-dire la décomposer en matrices triangulaires et  diagonale;
    \item \CASolve: résout le système linéaire après factorisation de la matrice.
\end{itemize}

\section{Réalisation du calcul}
\subsection{Définir le calcul mécanique: préparation}

Lancer le module \AsterStudy de la plate-forme \SalomeMeca. Puis en colonne gauche, cliquer sur l'onglet (à gauche) \SAAWCaseView et l'onglet (en haut) \SAAWDataSettings.
On peut céer une nouvelle étape de calcul \emph{Stage} avec \SAKeyRightMouse sur le cas de calcul (nommé \texttt{CurrentCase} par défaut), puis \SAAMOperationsAddStage. On définit le fichier de commandes du cas de calcul. Dans un premier temps, on défini les données globales du calcul:

\begin{itemize}
    \item Lire le maillage : commande \CAReadMesh. Dans le menu définissant le maillage d'origine, il suffit d'aller choisir celui correspondant au maillage dans le module \SAModuleMesh;
    \item Définir le matériau : commande \CADefineMaterial. Matériau élastique linéaire et isotrope;
    \item Affecter le matériau : commande \CAAffectMaterial. Même matériau sur toute la structure;
    \item Choisir les éléments finis : commande \CAAffectModel. Ce sont des éléments tridimensionnels (soit \CAModelThreeDim);
    \item Définir les conditions limites : commande \CAAffectLoad. Il y a un encastrement sur le groupe de mailles \texttt{Encas};
\end{itemize}

\subsection{Définir le calcul mécanique: commandes éclatées}

On va préparer les matrices $[K]$ et $[M]$ nécessaires au calcul des vecteurs et valeurs propres. La difficulté est que ces commandes ne font pas partie de celles déjà triées dans le haut de la fenêtre \AsterStudy.

Il faut donc afficher toutes les commandes par l'icône \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_ShowAllCommands}}. La fenêtre \ref{Fig:AllCommands} s'ouvre alors et vous pouvez chercher les commandes nécessaires.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./images/win_AsterStudy_AddCommand}
    \caption{Liste de toutes les commandes}
    \label{Fig:AllCommands}
\end{figure}

Voici ces commandes:
\begin{itemize}
    \item Construire les matrices élémentaires de masse : on utilise la commande \CACalcElemMatr. Pour la masse, il faut choisir \CAOptionMass. Ne pas oublier de renseigner le \CAModel et le champ de matériau \CAFieldMaterial ;
    \item Construire les matrices élémentaires de rigidité : on utilise la commande \CACalcElemMatr. Pour la rigidité, il faut choisir \CAOptionRigidity. Ne pas oublier de renseigner le \CAModel, le champ de matériau \CAFieldMaterial et surtout les chargements \CALoad en provenance de l'encastrement (pour évaluer la matrice $[B]$);
    \item Créer la numérotation avec la commande \CANumbering en donnant la matrice de rigidité \CANumberingMatr et non le modèle pour prendre en compte les multiplicateurs de Lagrange pour la matrice $[B]$;
    \item Assembler la matrice de masse : commande \CAAsseElemMatr sur les matrices élémentaires de masse et la numérotation précédemment créée;
    \item Assembler la matrice de rigidité : commande \CAAsseElemMatr sur les matrices élémentaires de rigidité et la numérotation précédemment créée;
\end{itemize}

Enfin, on réalise le calcul des vecteurs et valeurs propres avec la commande \CAEigenSolver:
\begin{itemize}
    \item Calcul des vecteurs et valeurs propres avec \CAEigenSolver: renseignez \CAMatrixMass  et \CAMatrixRigidity à partir des matrices \emph{assemblées}. Laisser les autres options par défaut;
    \item Impressions des résultats au format \CAFileMED: commande \CAPrintResult. Indiquez l'emplacement du fichier de nom \texttt{modes.rmed}.
\end{itemize}

\subsection{Lancer le calcul}

Pour lancer le calcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}.
Il faut penser à rafraîchir l'état du calcul par le bouton \SAAWHVRefresh (vous pouvez aussi passer en mode automatique en sélectionnant la fréquence de rafraîchissement dans le bouton \SAAWHVAutoRefresh ).
Pour avoir les fréquences propres, vous devez ouvrir le fichier \texttt{message} en cliquant sur le bouton \emph{Show message file}.
Déroulez le fichier jusqu'à trouver la commande \CAEigenSolver. Vous trouverez les 10 fréquences:
\begin{lstlisting}
numero    frequence (HZ)     norme d'erreur
    1       1.78945E+01        1.22608E-08
    2       2.36339E+01        7.44147E-09
    3       1.12432E+02        6.85836E-10
    4       1.19589E+02        9.68542E-10
    5       1.19979E+02        6.49859E-10
    6       1.81830E+02        6.00573E-10
    7       2.63868E+02        3.68134E-10
    8       3.18562E+02        6.86264E-10
    9       3.20794E+02        7.31486E-10
   10       3.25735E+02        9.37847E-10
\end{lstlisting}

On peut trouver facilement dans la littérature les fréquences propres analytiques d'une plaque simplement encastrée: $6,3Hz$, $13,4Hz$ $38,6Hz$ et $49Hz$.

\begin{mdframed}[outerlinecolor=black,outerlinewidth=2pt,linecolor=cccolor,middlelinewidth=3pt,roundcorner=10pt]
    Question : Proposer une modélisation qui donne de meilleurs résultats !
    Astuce: les modes propres sont en flexion et que la contrainte est donc linéaire dans l'épaisseur.
\end{mdframed}

\section{Pour aller plus loin ...}

\begin{mdframed}[outerlinecolor=black,outerlinewidth=2pt,linecolor=cccolor,middlelinewidth=3pt,roundcorner=10pt]
    Ce cas permet d'obtenir un bonus de points pour l'examen.
\end{mdframed}

Faites le même problème en utilisant des éléments de plaque. Il y a deux différences:
\begin{itemize}
    \item La géométrie de base n'est plus une cube (3D) mais la surface moyenne (un carré). Le maillage utilise des quadrangles ou des triangles \emph{linéaires};
    \item Il ne faut pas oublier de définir l'épaisseur de la plaque par la commande \CAAffectElemProperties. Il sera nécessaire de renseigner ces paramères lors du calcul des matrices élémentaires en plus des caractéristiques du matériau.
\end{itemize}
