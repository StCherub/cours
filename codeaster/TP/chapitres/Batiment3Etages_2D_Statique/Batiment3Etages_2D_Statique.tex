\chapter{Modélisation statique d'un bâtiment à trois étages}

Ce TP permet une prise en main de la plate-forme \SalomeMeca sur un cas simple en élasticité linéaire (en statique).

\section{Définition du problème}
\subsection{Géométrie}

On considère ici le cas d'un bâtiment constitué de trois étages, de deux murs verticaux et d'un radier (fondation), voir \ref{Fig:BAT_Batiment}.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=2]{./chapitres/Batiment3Etages_2D_Statique/images/Batiment3Etages}
    \caption{Bâtiment (représentation tridimensionnelle)}
    \label{Fig:BAT_Batiment}
\end{figure}

Les dimensions sont données entre les surfaces moyennes pour les murs et les planchers (à mi-épaisseur donc).
Entre le milieu de la fondation et le milieu du toit, on a une hauteur $H = \unit{12}\meter$. Chaque plancher (et la fondation) est un carré d'épaisseur $e_p=\unit{0,4}\meter$ et de côté $L=8m$ entre le milieu des deux murs d'épaisseur $e_m=\unit{0,18}\meter$

On va commencer par faire une hypothèse en modélisant une \emph{coupe} verticale du bâtiment. Les épaisseurs seront représentées explicitement par la géométrie et par le maillage.
O obtient un ensemble de six objets: la fondation, les deux planchers, le toit et les deux murs comme on le voit sur la figure \ref{Fig:BAT_Building}.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./chapitres/Batiment3Etages_2D_Statique/images/Batiment3Etages_Ensemble_2D}
    \caption{Bâtiment obtenu par une coupe verticale}
    \label{Fig:BAT_Building}
\end{figure}

\subsection{Conditions aux limites}

Deux types de conditions limites seront considérées:
\begin{itemize}
    \item Un encastrement complet de la surface inférieure du radier;
    \item Un raccord permettant un encastrement \emph{moyen} de la surface inférieure du radier.
\end{itemize}

\subsection{Chargements}

On considère deux chargements:
\begin{itemize}
    \item Le poids propre du bâtiment (pesanteur);
    \item Une pression répartie sur tous les planchers pour représenter les chapes de $\unit{20}\milli\meter$ (ou bien complexe d’étanchéité en terrasse) : $p_{Floor}=\unit{66}{\deca\newton\cdot\rpsquare\metre}$.
\end{itemize}

\subsection{Propriétés des matériaux}

Les caractéristiques matériaux sont :
\begin{itemize}
    \item Module d’Young : $E=\unit{32}{\giga\pascal}$;
    \item Coefficient de Poisson: $\nu=0,3$;
    \item Masse volumique du béton $\rho_b=\unit{2500}{\kilogrampercubicmetre}$.
\end{itemize}

Par ailleurs, on proposera également une modélisation qui consiste à modifier la masse volumique du béton pour simuler la présence d'un équipement sur les planchers.
Par exemple, sur le plancher 1, on suppose qu'on a une masse d'équipements $m_{Floor1} = \unit{3000}{\kilogram}$ et sur le plancher 2 qu'on a une masse d'équipements $m_{Floor2} = \unit{4000}{\kilogram}$.
Si on considère le volume d'un plancher, on a (pour une épaisseur unitaire car nous sommes dans l'hypothèse des déformations planes):
\begin{equation}
    v_{Floor} = 0,4 \times 7,82 \times 1.0
\end{equation}
alors on peut estimer la masse volumique équivalente à lui appliquer :
\begin{equation}
    \rho_{Floor}^{eq} = {m_{Floor} \over v_{Floor}} + \rho_b
\end{equation}

\section{Premier calcul statique}
\subsection{Données du problème}

Récupérer le maillage du bâtiment (\texttt{building\TextUnderscore 2D\TextUnderscore TP2\TextUnderscore base.med}) en 2D (le maillage est en mètres) et chargez-le dans le module \SAModuleMesh avec \SAMMFileImportMED.

Dans ce premier calcul, on considère le cas du bâtiment sous poids propre, encastré à sa base et avec une pression répartie sur tous les étages (chape et étanchéité).

\subsection{Ouvrir le module \AsterStudy}

Vous pouvez ouvrir le module \AsterStudy en sélectionnant \fbox{\includegraphics[width=0.3cm,height=0.3cm]{./images/icon_ModuleASTERSTUDY}} dans la barre de sélection des modules.

\subsection{Les catégories de commande}

La plupart des commandes sont classées dans dix catégories (voire tableau \ref{Tab:CatagoriesCommands}).

\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.6\textwidth}|}
        \hline
        Catégorie                    & Description                                                                                                                                                                  \\
        \hline
        \SAAOCategoryMesh            & Manipulation du maillage et des groupes                                                                                                                                      \\
        \hline
        \SAAOCategoryModel           & Définition du modèle, des caractéristiques élémentaires pour les éléments de structure, commandes de réduction de modèle, ...                                                \\
        \hline
        \SAAOCategoryMaterial        & Définition et affectation des matériaux                                                                                                                                      \\
        \hline
        \SAAOCategoryFunctionList    & Manipulation des fonctions, formules et listes                                                                                                                               \\
        \hline
        \SAAOCategoryBCLoad          & Définition des chargements, des conditions limites, du contact, ...                                                                                                          \\
        \hline
        \SAAOCategoryPreAnalysis     & Quelques opérations de manipulation sur les bases propres, la manipulation des quantités élémentaires, le calcul métallurgique, les calcul des câbles de pré-contrainte, ... \\
        \hline
        \SAAOCategoryAnalysis        & Commandes d'analyse                                                                                                                                                          \\
        \hline
        \SAAOCategoryPostProcessing  & Commandes pour le calcul de quantités de post-traitement                                                                                                                     \\
        \hline
        \SAAOCategoryFractureFatigue & Commandes spécifiques pour la mécanique de la rupture et la fatigue                                                                                                          \\
        \hline
        \SAAOCategoryOutput          & Sorties des données (dans des fichiers)                                                                                                                                      \\
        \hline
    \end{tabular}
    \caption{\label{Tab:CatagoriesCommands} Liste des catégories pour les commandes}
\end{table}

\subsection{Définir le calcul mécanique: préparation}

Sur la gauche de la fenêtre, cliquez sur l'onglet \SAAWCaseView. On ajoute un \SAAOStage à l'aide de \SAAMOperationsAddStage, on peut le renommer (par \SAKeyRightMouse sur le  \SAAOStage, puis \SAAMBrowserRename).

\subsubsection{Préparer le maillage}

On commence par lire et préparer le maillage qu'on va utiliser.
\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.21\textwidth}|p{0.39\textwidth}|}
        \hline
        Opération                                & Commande      & Description                                                                                                                                                                                                                                                                                                                                                                                                     \\
        \hline
        Lecture du maillage                      & \CAReadMesh   & Dans le menu définissant le maillage d'origine, il suffit d'aller choisir celui correspondant au maillage dans le module \SAModuleMesh avec \CAReadMeshFile (voir \ref{Fig:CommandReadMesh})                                                                                                                                                                                                                    \\
        \hline
        Réorientations des normales aux éléments & \CAChangeMesh & On utilise \CASetMeshOrientation pour orienter tous les éléments de la même façon sur les groupes correspondant à l'application des pressions (\texttt{floor\TextUnderscore 0\TextUnderscore load}, \texttt{floor\TextUnderscore 1\TextUnderscore load}, \texttt{floor\TextUnderscore 2\TextUnderscore load} et \texttt{roof\TextUnderscore load}), avec une normale tournée vers l’\emph{intérieur} du domaine \\
        \hline
    \end{tabular}
    \caption{\label{Tab:Maillage} Préparation du maillage}
\end{table}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./images/win_AsterStudy_ReadMesh}
    \caption{Commande \CAReadMesh}
    \label{Fig:CommandReadMesh}
\end{figure}

\newpage

\subsubsection{Préparer le matériau}

Pour sélectionner le matériau, on procède toujours en deux étapes:
\begin{itemize}
    \item Décrire le matériau (commande \CADefineMaterial);
    \item Affecter le matériau sur le maillage (commande \CAAffectMaterial).
\end{itemize}

\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.2\textwidth}|p{0.4\textwidth}|}
        \hline
        Opération               & Commande          & Description                                                                              \\
        \hline
        Définition du matériau  & \CADefineMaterial & Matériau élastique linéaire et isotrope avec \CADefineMaterialELAS de nom \texttt{Beton} \\
        \hline
        Affectation du matériau & \CAAffectMaterial & Même matériau \texttt{Beton} sur toute la structure avec \CAEverywhere                   \\
        \hline
    \end{tabular}
    \caption{\label{Tab:Materiau} Préparation du matériau}
\end{table}

Attention à ne pas oublier de renseigner la masse volumique du béton pour la prise en compte de la pesanteur.

\subsubsection{Préparer le modèle}

Le \emph{modèle} est l'étape qui permet de choisir les équations à résoudre et les inconnues qu'on va appliquer sur les nœuds. Il repose nécessairement sur le maillage.
\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.2\textwidth}|p{0.4\textwidth}|}
        \hline
        Opération                     & Commande       & Description                                                                                                                                                                                \\
        \hline
        Définition des éléments finis & \CAAffectModel & Ce sont des éléments en mécanique de type déformations planes (soit \CAMechanic pour \CAPhenomenon et  \CAModelPlaneStrain pour \CAModelisation) sur toute la structure avec \CAEverywhere \\
        \hline
    \end{tabular}
    \caption{\label{Tab:Modele} Préparation du modèle}
\end{table}

\subsubsection{Préparer les conditions limites et les chargements}

Il y a deux conditions de Dirichlet permettant de définir les axes de symétrie et un chargement de type Neumann pour la force appliquée sur l'arête supérieure.

\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.2\textwidth}|p{0.4\textwidth}|}
        \hline
        Opération                             & Commande      & Description                                                                                                                                                                                                                                                                                                                                                   \\
        \hline
        Définition des conditions aux limites & \CAAffectLoad & Encastrement: déplacement \texttt{DX} nul et \texttt{DY} nul sur le groupe de mailles \texttt{liai\TextUnderscore foundation}                                                                                                                                                                                                                                 \\
        \hline
        Définition de la pesanteur            & \CAAffectLoad & Pesanteur \CAGravity avec la direction \CAGravityDire donnée par un vecteur $(0, -1, 0)$ et l'accélération de la pesanteur \CAGravityAcce qui vaut $\unit{9,81}\metrepersquaresecond$                                                                                                                                                                         \\
        \hline
        Définition des charges des planchers  & \CAAffectLoad & Chargement de pression \CASetPressure avec \CASetPressurePres donnée par $p_{Floor}$ sur les groupes de mailles (linéiques) de la surface des planchers (\texttt{floor\TextUnderscore 0\TextUnderscore load}, \texttt{floor\TextUnderscore 1\TextUnderscore load}, \texttt{floor\TextUnderscore 2\TextUnderscore load} et \texttt{roof\TextUnderscore load}).

        \\
        \hline
    \end{tabular}
    \caption{\label{Tab:BC} Préparer les conditions limites et les chargements}
\end{table}


\subsection{Définir le calcul mécanique: réaliser le calcul statique}

On ajoute un \SAAOStage à l'aide de \SAAMOperationsAddStage. Les principales étapes pour l'analyse mécanique et le post-traitement sont dans le tableau \ref{Tab:RealisationCalcul}.

\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.2\textwidth}|p{0.4\textwidth}|}
        \hline
        Opération                        & Commande        & Description                                                                                                                                                                                                                                                                                                          \\
        \hline
        Résolution du problème élastique & \CAStaticLinear & Définir le modèle par \CAModel, le matériau par \CAFieldMaterial, les conditions limites et chargements par \CALoad                                                                                                                                                                                                  \\
        \hline
        Post-traitement                  & \CAComputeField & Calcul du champ de contraintes par éléments aux nœuds (option \CAFieldStressELNO dans \CAComputeFieldStress). Enrichir le résultat provenant du calcul statique.\footnote{il suffit de donner le même nom au concept en sortie de  \CAComputeField et de \CAStaticLinear ou de cliquer sur le bouton \SAAWEditReuse} \\
        \hline
        Post-traitement                  & \CAComputeField & Calcul du champ de contraintes équivalentes par éléments aux points de Gauss (option \CAFieldStressELGA dans \CAComputeFieldStress). Enrichir le résultat provenant du calcul statique.                                                                                                                              \\
        \hline
        Post-traitement                  & \CAComputeField & Calcul des déformations par éléments aux points de Gauss (option \CAFieldStrainELGA dans \CAComputeFieldStrain). Enrichir le résultat provenant du calcul statique.                                                                                                                                                  \\
        \hline
        Post-traitement                  & \CAComputeField & Calcul du champ de contraintes équivalentes par éléments aux nœuds (option \CAFieldStressEquiELGA dans \CAComputeFieldCriteria). Enrichir le résultat provenant du calcul statique.                                                                                                                                  \\
        \hline
        Sorties                          & \CAPrintResult  & Impression des résultats au format \CAFileMED (format par défaut). Il faut indiquer l'emplacement du fichier. Ne pas oublier de préciser le résultat que l'on veut sortir dans \CAPrintResultResults puis \CAPrintResultResultsResult.                                                                               \\
        \hline
    \end{tabular}
    \caption{\label{Tab:RealisationCalcul} Commandes pour le calcul}
\end{table}


\subsection{Lancer le calcul mécanique}

Pour lancer le calcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}.

\subsection{Post-traiter le résultat}

Retournez dans  l'onglet \SAAWCaseView, en sélectionnant dans \SAAWDataFiles la ligne correspond à l'impression des résultats (\CAPrintResult), puis \SAAMDataFilesPostProcess avec le bouton droit de la souris et le fichier s'ouvre dans l'interface graphique légère. Aucune indication ! A vous de jouer !

\section{Second calcul statique}
\subsection{Données du problème}

On travaille sur le même maillage du bâtiment en 2D que dans le cas précédent.

On considère le cas du bâtiment sous poids propre et avec une pression répartie sur tous les étages (chape et étanchéité), plus le poids des équipements en modifiant la masse volumique du béton.

Par ailleurs, pour la condition limite, on propose de remplacer l'encastrement par une liaison discrète. L'idée est de contraindre \emph{en moyenne} l'encastrement.

\subsection{Préparation du maillage}

Pour cette nouvelle condition limite, est nécessaire de modifier le maillage pour introduire un groupe de maille de type \CAElemPOI. Cette maille a pour support un nœud du maillage et comprendra trois (translations) ou six (translations et rotations) degrés de liberté selon l'élément fini qu'on affectera ensuite dans \codeaster (dans la commande \CAAffectModel).

Il faut d'abord modifier le maillage, et donc on passe dans le module \SAModuleMesh.

On crée d'abord un nouveau nœud grâce à \SAMMModificationAddNode puis en sélectionnant le nœud au milieu de l'arête inférieure du radier. Grâce à la préparation du maillage (la division en deux parties de tous les planchers), vous avez nécessairement un nœud exactement à cet emplacement.
La fenêtre \ref{Fig:BAT_AddNode} s'ouvre. Les coordonnées du nœud dépendront bien évidemment de la manière dont vous avez créé la géométrie. Ajouter ce nouveau nœud dans un groupe (ici \texttt{NodeRadier}) grâce à \SAMWAddNodeAddToGroup.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./chapitres/Batiment3Etages_2D_Statique/images/Batiment3Etages_Ensemble_2D_WindowAddNode}
    \caption{Fenêtre d'ajout d'un nœud}
    \label{Fig:BAT_AddNode}
\end{figure}
\newpage
Nous allons maintenant créer la  maille de type \CAElemPOI.
Après sélection du menu \SAMMModificationAddPElementOnNodes, la fenêtre \ref{Fig:BAT_Add0DElementOnNodes} s'ouvre.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./chapitres/Batiment3Etages_2D_Statique/images/Batiment3Etages_Ensemble_2D_WindowAdd0DElementOnNode}
    \caption{Fenêtre de création d'un élément ponctuel}
    \label{Fig:BAT_Add0DElementOnNodes}
\end{figure}


Dans cette fenêtre:
\begin{itemize}
    \item Sélectionnez \SAMWAddPElementOnNodesMesh;
    \item Récupérer le groupe de nœud précédent (ici \texttt{NodeRadier}) dans \SAMWAddPElementOnNodesName;
    \item Ajouter dans un groupe par \SAMWAddPElementOnNodesAddToGroup (ici \texttt{ElementRadier});
    \item Validez et fermez avec \SAKeyApplyClose.
\end{itemize}

Un nouveau groupe \texttt{ElementRadier} doit apparaître dans l'arbre d'études sous la rubrique \SAMMBrowserGroupsPElements

Cet élément étant créé sur le maillage actuel, dès qu'on change les hypothèses, l'algorithme ou qu'on supprime les données de ce maillage en général (via \SAMMModificationRemoveClearMeshData), cet élément sera supprimé. Il faudra donc le recréer.

\begin{Remark}
    Attention ! Supprimer l'objet \SAMOGroup contenant l'élément ponctuel ne supprimera pas l'élément lui-même !

    Pour vous en assurer, vous pouvez utiliser la fonctionnalité \SAMMMeshMeshInformation, à la ligne \SAMOElementP, on a le nombre total d'éléments ponctuels.
\end{Remark}

\subsection{Définir le calcul mécanique: préparation}

Sur la gauche de la fenêtre, cliquez sur l'onglet \SAAWCaseView. On ajoute un \SAAOStage à l'aide de \SAAMOperationsAddStage, il est préférable de le renommer (par \SAKeyRightMouse sur le  \SAAOStage, puis \SAAMBrowserRename).

\subsubsection{Modification du matériau}

Il va falloir créer deux nouveaux matériaux, correspondant aux nouvelles densités volumiques pour simuler la présence d'équipement sur les planchers. Il faut donc également changer l'affectation des matériaux puisqu'on va mettre des matériaux différents sur les planchers.
\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.3\textwidth}|p{0.2\textwidth}|p{0.4\textwidth}|}
        \hline
        Opération                                              & Commande          & Description                                                                                                                                                                                                                                                      \\
        \hline
        Définition du matériau général (hors planchers 1 et 2) & \CADefineMaterial & Matériau élastique linéaire et isotrope avec \CADefineMaterialELAS et la masse volumique $\rho_b$ de nom \texttt{Beton}                                                                                                                                          \\
        \hline
        Définition du matériau pour le plancher 1              & \CADefineMaterial & Matériau élastique linéaire et isotrope avec \CADefineMaterialELAS et la masse volumique $\rho_{Floor1}^{eq}$    de nom \texttt{BetonEqFloor1}                                                                                                                   \\
        \hline
        Définition du matériau pour le plancher 2              & \CADefineMaterial & Matériau élastique linéaire et isotrope avec \CADefineMaterialELAS et la masse volumique $\rho_{Floor2}^{eq}$   de nom \texttt{BetonEqFloor2}                                                                                                                    \\
        \hline
        Affectation du matériau                                & \CAAffectMaterial & Même matériau \texttt{Beton}  sur toute la structure avec \CAEverywhere , puis matériau de nom \texttt{BetonEqFloor1}  sur le groupe \texttt{floor\TextUnderscore 1}    et matériau de nom \texttt{BetonEqFloor2}  sur le groupe \texttt{floor\TextUnderscore 2} \\
        \hline
    \end{tabular}
    \caption{\label{Tab:Materiau2} Préparation du matériau}
\end{table}

\subsubsection{Modification du modèle}

Le modèle doit être modifié. En effet, il faut affecter l'élément fini sur le groupe d'élément ponctuel.
Dans l'affectation des éléments finis par la commande \CAAffectModel il faut définir deux groupes d'éléments finis:
\begin{itemize}
    \item Sur tout le maillage \CAEverywhere comme précédemment;
    \item Sur le groupe \texttt{ElementRadier}: \CAMechanic pour \CAPhenomenon et \CAModelPonctualTRTwoDim \footnote{ \texttt{T} et \texttt{R} signifient respectivement degrés de liberté en translation et en rotation. En 2D, deux translations et une rotation} pour \CAModelisation;
\end{itemize}

\begin{Remark}
    Attention ! On a changé le modèle et le matériau, prenez bien garde à choisir les bons dans les commandes qui en ont besoin (chargements, calcul) !
\end{Remark}

\subsubsection{Préparer les caractéristiques élémentaires}

Dans \codeaster, les éléments de type  \CAElemPOI doivent être affectés d'une raideur, d'une masse et/ou d'un amortissement. Même si les degrés de liberté seront ensuite complètement bloqués pour cet élément, il est nécessaire \emph{a minima} de définir leur rigidité.\footnote{En dynamique, l'absence de définition d'une masse sur ces éléments se traduira par une alarme du code qui vous signalera qu'on a choisi une masse nulle.}

Pour affecter ces caractéristiques, c'est la commande \CAAffectElemProperties. Puis sélectionner \CAAffectElemPropertiesDiscreteTwoDim. Sur le groupe \texttt{ElementRadier}, choisir\footnote{Vocabulaire: \texttt{K} pour rigidité, \texttt{TR} pour translation et rotation, \texttt{D} pour diagonal et \texttt{N} pour nodal (élément \CAElemPOI), voir la documentation de la commande} \texttt{K}\TextUnderscore \texttt{TR}\TextUnderscore \texttt{D}\TextUnderscore \texttt{N} dans \texttt{CARA}, cocher \texttt{SYME}, vous aurez trois valeurs à renseigner dans \texttt{Value}: les rigidités en translation suivant \texttt{X} et \texttt{Y} et en rotation autour de \texttt{Z}.
Comme cet élément va être bloqué, ces valeurs n'ont pas d'importance mais on voit que c'est ici qu'on pourrait définir une rigidité si nécessaire.

La commande \CAAffectElemProperties produit un objet que l'on utilisera dans les commandes de calcul dans le mot-clef \CAFieldElemProperties.

\subsubsection{Modification des conditions limites}

On reprend les chargements précédents (pesanteur et pression réparties sur les planchers). Mais on supprime la condition d'encastrement pour la remplacer par notre nouvelle liaison

Ensuite, on doit encastrer l'élément \CAElemPOI, attention, il y a \emph{trois} degrés de liberté à considérer sur cet élément !
Enfin, on réalise la liaison entre l'élément \CAElemPOI et le bas du radier \texttt{liai\TextUnderscore foundation}  avec la commande \CAGlueModels dans la commande \CAAffectLoad:
\begin{itemize}
    \item Choisir  \CAGlueModelsTwoDBeam dans \CAGlueModelsOption, ce qui permet de relier un domaine 2D (contraintes planes, déformations planes ou axisymétrique) avec un élément de poutre ou un élément discret \CAElemPOI;
    \item Le groupe \CAGlueModelsFirstGroup est le groupe qui représente la ligne sur laquelle on fait le raccord et le groupe \CAGlueModelsSecondGroup est l'élément\footnote{à noter que cet élément doit être au centre de gravité de l'arête avec laquelle on fait le raccord, sous peine de message d'erreur} \CAElemPOI  (ou le nœud du bout de la poutre si on utilise une poutre).
\end{itemize}

\subsection{Définir le calcul mécanique: réaliser le calcul statique}

On ajoute un \SAAOStage à l'aide de \SAAMOperationsAddStage. Les principales étapes pour l'analyse mécanique et le post-traitement sont les mêmes que dans le cas précédent.

Attention à bien sélectionner le bon matériau, le bon modèle et les bonnes conditions limites. Ne pas oublier également de renseigner \CAFieldElemProperties.

\subsection{Lancer le calcul mécanique}

Pour lancer le calcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}.

\subsection{Post-traiter le résultat}

Retournez dans  l'onglet \SAAWCaseView, en sélectionnant dans \SAAWDataFiles la ligne correspond à l'impression des résultats (\CAPrintResult), puis \SAAMDataFilesPostProcess avec le bouton droit de la souris et le fichier s'ouvre dans l'interface graphique légère. Aucune indication ! A vous de jouer !
