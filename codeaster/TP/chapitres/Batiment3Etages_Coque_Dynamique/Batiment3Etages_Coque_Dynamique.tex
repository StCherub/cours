Ce TP permet une prise en main de la plate-forme \SalomeMeca sur un cas simple en dynamique linéaire.

\section{Définition du problème}
\subsection{Géométrie}

On considère ici le cas d'un bâtiment constitué de trois étages, de deux murs verticaux et d'un radier (fondation), voir \ref{Fig:BAT_Plaque_GeomInit}.

\begin{figure}[!ht]
    \centering
    \fbox{\includegraphics[scale = 0.5]{./chapitres/Batiment3Etages_Coque_Dynamique/images/Batiment3Etages_Ensemble_Plaque_GeomInitiale}}
    \caption{Géométrie surfacique initiale du bâtiment}
    \label{Fig:BAT_Plaque_GeomInit}
\end{figure}

\subsection{Conditions aux limites}

On considère un encastrement complet du radier et un chargement dynamique de pression sur le toit.
L'idée est de représenter l'effet d'une foule de personnes dansant en rythme (pogo !) sur le toit.
On appliquera donc une pression par une la formule suivante:

\begin{equation*}
    p(t) = {{(F_{f} sin(2 \pi f_{f} t)-abs(F_{f} sin(2 pi f_{f} t)))} \over {2} }
\end{equation*}

avec $F_{f} =\unit{1.0 \times 10^5}{\pascal}$ la pression correspondant à la masse de la foule et $f_{f} = \unit{1.0}{\hertz}$ la fréquence.

\subsection{Propriétés des matériaux}

Les caractéristiques matériaux sont :
\begin{itemize}
    \item Module d’Young : $E=\unit{32}{\giga\pascal}$;
    \item Coefficient de Poisson: $\nu=0,2$;
    \item Masse volumique du béton $\rho_b=\unit{2500}{\kilogrampercubicmetre}$.
\end{itemize}

\section{Calcul dynamique transitoire sur base physique}
\subsection{Définir le calcul mécanique: préparation}

Reprendre le fichier du calcul des modes propres précédent, nous allons compléter le calcul précédent.

Lancer le module \AsterStudy de la plate-forme \SalomeMeca. Puis en colonne gauche, cliquer sur l'onglet (à gauche) \SAAWCaseView et l'onglet (en haut) \SAAWDataSettings. La définition du problème précédent est reprise intégralement.

\subsection{Orientation des coques}

Le fait d'utiliser une pression répartie nécessite de définir le repère sur chaque coque (plaque).
Pour définir les normales, il faut utiliser la commande \CAChangeMesh  avec l'option \CASetMeshOrientationShell.
Pour les coques (ou les plaques), on définit la normale sur l'un des noeuds du mur ou du plancher à orienter, \codeaster définira alors la normale en la propageant.On doit donc donner trois paramètres:
\begin{itemize}
    \item Le \CAGroupElem donne le groupe d'éléments à orienter (mur, plancher, etc.);
    \item Un \CAGroupNode donne un noeud de référence pour définir la normale;
    \item Le mot clef \CASetMeshOrientationShellNorm est un vecteur à trois composantes donnant la normale.
\end{itemize}


Il y a ainsi six groupes à orienter (voir tableau \ref{Tab:OrientationNormales}).

\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.17\textwidth}|p{0.17\textwidth}|p{0.25\textwidth}|p{0.2\textwidth}|}
        \hline
        Entité     & \CAGroupElem                       & \CAGroupNode                           & \CASetMeshOrientationShellNorm \\
        \hline
        Mur gauche & \texttt{Wall\TextUnderscore Left}  & \texttt{NodeWall\TextUnderscore Left}  & (-1, 0, 0)                     \\
        \hline
        Mur droit  & \texttt{Wall\TextUnderscore Right} & \texttt{NodeWall\TextUnderscore Right} & (1, 0, 0)                      \\
        \hline
        Toit       & \texttt{Roof}                      & \texttt{NodeRoof}                      & (0, 0, 1)                      \\
        \hline
        RDC        & \texttt{Floor\TextUnderscore 0}    & \texttt{NodeFloor\TextUnderscore 0}    & (0, 0, 1)                      \\
        \hline
        Étage 1    & \texttt{Floor\TextUnderscore 1}    & \texttt{NodeFloor\TextUnderscore 1}    & (0, 0, 1)                      \\
        \hline
        Étage 2    & \texttt{Floor\TextUnderscore 2}    & \texttt{NodeFloor\TextUnderscore 2}    & (0, 0, 1)                      \\
        \hline
    \end{tabular}
    \caption{\label{Tab:OrientationNormales} Orientation des normales des coques.}
\end{table}

\begin{Remark}
    Même si l'on applique l'effort que sur le toit, c'est une bonne habitude de prendre le temps de définir toutes les normales.
\end{Remark}

Par ailleurs, il y a une difficulté supplémentaires. Pour les coques, il faut également définir la première tangente (la secodne sera calculée par produit vectoriel). On utilisera pour cela la commande \CAAffectElemProperties. Le mot-clefs \CAAffectElemPropertiesShell permet de définir l'épaisseur des plaques (\CAAffectElemPropertiesShellThickness) mais aussi l'orientation de la première tangente par \CAAffectElemPropertiesShellTangentVector.

On orientera donc la tangente de nos deus murs pour éviter une erreur fatale (voir tableau \ref{Tab:OrientationTangentes}). En effet, par défaut, \codeaster considère que la première tangente est dirigée suivant l'axe $Ox$. Or il se trouve que nos deux murs sont normaux à cet axe !
\begin{table}[!ht]
    \centering
    \begin{tabular}{|p{0.17\textwidth}|p{0.25\textwidth}|p{0.25\textwidth}|}
        \hline
        Entité     & \CAGroupElem                       & \CAAffectElemPropertiesShellTangentVector \\
        \hline
        Mur gauche & \texttt{Wall\TextUnderscore Left}  & (0, 0, 1)                                 \\
        \hline
        Mur droit  & \texttt{Wall\TextUnderscore Right} & (0, 0, 1)                                 \\
        \hline
    \end{tabular}
    \caption{\label{Tab:OrientationTangentes} Orientation des tangentes des coques.}
\end{table}

\subsection{Les nouvelles entités nécessaires}

Pour faire ce calcul, des nouvelles commandes doivent définir: le chargement et la discrétisation temporelle. Si vous ne trouvez pas les commandes suivantes dans les menus prédéfinis, pensez à afficher toutes les commandes par l'icône \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_ShowAllCommands}}.

\begin{itemize}
    \item Créer la discrétisation temporelle : commande \CADefineListReal. On propose de discrétiser de $0$ à $\unit{10}{\second}$ avec un pas de $\unit{0.01}{\second}$. On choisit donc \CADefineListRealBegin=0, puis \CADefineListRealInterval avec \CADefineListRealStep=0.01 et \CADefineListRealUntil=10.;
    \item Créer la formule définissant le chargement: commande \CADefineFormula. La formule est donnée dans \CADefineFormulaValue et vaut $(1E5*sin(2*pi*INST)-abs(1E5*sin(2*pi*INST)))/2.0$, le paramètre est le temps, donc \CADefineFormulaParameter vaut \CAParaTime (et non \CAParaTemperature !)
    \item Créer le chargement: on utilise la commande \CAAffectLoad. Le chargement est de type \CAForceShell. Ensuite, il suffit de définir l'amplitude \CASetPressurePres qui vaut $1.0$ (l'amplitude est donnée par la fonction) et on applique sur le \CAGroupElem de nom \texttt{Roof};
\end{itemize}

\subsection{Préparation du calcul}

On utilise la commande \CADynamicLinear avec les paramètres suivants:
\begin{itemize}
    \item Pour le type de calcul (dynamique transitoire sur coordonnées physiques), il faut choisir \CADynamicLinearBase qui vaut \CADynamicLinearBasePhys et \CADynamicLinearType qui vaut \CADynamicLinearTypeTransient;
    \item Définir la discrétisation temporelle dans \CATimestepping puis renseigner la liste définie par \CADefineListReal dans \CATimeList;
    \item Matrices de rigidité et de matrice précédentes dans \CAMatrixMass et \CAMatrixRigidity ;
    \item Choisir un schéma de type \CADynamicLinearSchemeNewmark dans \CADynamicLinearScheme;
    \item Ajouter le chargement dans \CALoads. Le chargement est constitué du chargement de pression dans \CALoad et la fonction multiplicatrice de la formule dans \CAMultiplierFunction;
    \item Ajouter le \CAModel;
    \item Ajouter le champ de matériau dans le champ \CAFieldMaterial;
    \item Ajouter les caractéristiques élémentaires dans le champ \CAFieldElemProperties.
\end{itemize}
On propose de nommer le résultat \texttt{phystran}.

\subsection{Sortie des résultats et exécution du calcul}

Aajouter la commande permettant de sortir les résultats de \CADynamicLinear au format \CAFileMED: commande \CAPrintResult, résultat \texttt{phystran}. Indiquez l'emplacement du fichier de nom \texttt{phystran.rmed} (par exemple).

Pour lancer le calcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}. La fenêtre qui s'ouvre permet de régler les options du calcul (temps, mémoire, etc.). Laissez les valeurs par défaut.


\section{Réalisation du calcul dynamique sur base modale}

Nous allons maintenant réaliser le même calcul mais en utilisant la base modale précédemment évaluée.

\subsection{Les nouvelles entités nécessaires}

Pour faire ce calcul, des nouvelles commandes doivent définir la projection sur base modale des différentes quantités. Si vous ne trouvez pas les commandes suivantes dans les menus prédéfinis, pensez à afficher toutes les commandes par l'icône \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_ShowAllCommands}}.

\begin{itemize}
    \item Créer une numérotation sur \emph{base généralisée}: on utilise la commande \CANumberingGene. Utilisez la base modale issue du calcul modal précédent (commande \CAEigenSolver);
    \item Projeter les deux matrices (rigidité et masse) sur la base modale: commande \CAProjectMatrix. On obtient les deux nouvelles matrices (sur base modale);
    \item Construire les vecteurs élémentaires du chargement : commande \CACalcElemVect. Il faut choisir \CAOptionLoad. Ne pas oublier de renseigner le champ de matériau \CAFieldMaterial et les caractéristiques élémentaires \CAFieldElemProperties;
    \item Assembler le vecteur de chargement : commande \CAAsseElemVect sur les vecteurs élémentaires et la même numérotation que les matrices;
    \item Projeter le vecteur issu du chargement : on utilisera la commande \CAProjectVector;
\end{itemize}

\subsection{Préparation du calcul}

On utilise la commande \CADynamicLinear avec les paramètres suivants:
\begin{itemize}
    \item Pour le type de calcul (dynamique transitoire sur coordonnées \emph{généralisées}), il faut choisir \CADynamicLinearBase qui vaut \CADynamicLinearBaseGene  et \CADynamicLinearType qui vaut \CADynamicLinearTypeTransient;
    \item Définir la discrétisation temporelle dans \CATimestepping puis renseigner la liste définie par \CADefineListReal dans \CATimeList;
    \item Matrices de rigidité et de matrice précédentes dans \CAMatrixMass et \CAMatrixRigidity. Attention à bien prendre la version \emph{projetée} de ces matrices ! ;
    \item Choisir un schéma de type \CADynamicLinearSchemeNewmark dans \CADynamicLinearScheme;
    \item Ajouter le chargement dans \CALoads. Le chargement est constitué du vecteur projeté précédemment dans \CAVectAsseGene et la fonction multiplicatrice de la formule dans \CAMultiplierFunction.
\end{itemize}
On propose de nommer le résultat \texttt{resugene}.

Ce résultat est peu lisible: il contient les coordonnées généralisées associées à chaque mode. On peut le reprojeter dans l'espace physique avec la commande \CAProjectGeneToPhys. Par défaut, cette commande ne construit pas tous les champs (déplacements, vitesses, accélérations). Il faut sélectionner \CAComputeAllFields. On propose de nommer les résultats issus de la commande par \texttt{genephys}.
Vous pouvez sortir les résultats de \CAProjectGeneToPhys au format \CAFileMED: commande \CAPrintResult, résultat \texttt{genephys}. Indiquez l'emplacement du fichier de nom \texttt{dynagenephys.rmed} (par exemple).

\subsection{Sortie des résultats et exécution du calcul}

Pour lancer le cas decalcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}. La fenêtre qui s'ouvre permet de régler les options du calcul (temps, mémoire, etc.). Laissez les valeurs par défaut.


\subsection{Troncature de la base modale}

Le cas considéré dans cet exercice est bien choisi: la représentation modale en terme de fréquence et de modes propres correspond bien au chargement appliqué. En réduisant le nombre de modes avant la projection, la qualité des résultats s'en ressent.
Pour ça, la commande \CANumberingGene permet de réduire la base grâce au mot-clef \CATruncationBase\. Ce nombre doit être inférieur ou égal au nombre total de modes calculés.

Regarder l'effet sur le déplacement quand vous diminuer le nombre de vecteurs propres.
