Ce TP permet une prise en main de la plate-forme \SalomeMeca sur un cas simple en élasticité linéaire (en modal).

\section{Définition du problème}
\subsection{Géométrie}

On considère ici le cas d'un bâtiment constitué de trois étages, de deux murs verticaux et d'un radier (fondation), voir \ref{Fig:BAT_Plaque_GeomInit}.

\begin{figure}[!ht]
    \centering
    \fbox{\includegraphics[scale = 0.5]{./chapitres/Batiment3Etages_Coque_Dynamique/images/Batiment3Etages_Ensemble_Plaque_GeomInitiale}}
    \caption{Géométrie surfacique initiale du bâtiment}
    \label{Fig:BAT_Plaque_GeomInit}
\end{figure}

\subsection{Conditions aux limites}

On considère un encastrement complet du radier.

\subsection{Propriétés des matériaux}

Les caractéristiques matériaux sont :
\begin{itemize}
    \item Module d’Young : $E=\unit{32}{\giga\pascal}$;
    \item Coefficient de Poisson: $\nu=0,2$;
    \item Masse volumique du béton $\rho_b=\unit{2500}{\kilogrampercubicmetre}$.
\end{itemize}

\section{Calcul modal}
\subsection{Rappels de quelques éléments théoriques}

En appliquant la méthode des éléments finis sur l'équation de l'élastodynamique, on obtient le système discrétisé linéaire suivant:
\begin{equation*}
    \langle \tilde {u}\rangle .\left[K\right].\left\{u\right\}+\langle \tilde
    {u}\rangle .\left[M\right].\left\{\ddot{u}\right\}=\langle \tilde {u}\rangle
    .\left\{L\right\}
\end{equation*}
Avec la matrice de rigidité $\left[K\right]$, résultat de l'assemblage des matrices de rigidité élémentaires $\left[k\right]$:
\begin{equation*}
    \left[k\right] = \underset{\Omega ^{e}} {\int }\left[B\right]^{T}. \left[C\right] .\left[B\right].d\Omega ^{e}
\end{equation*}
Et la matrice masse  $\left[M\right]$, résultat de l'assemblage des matrices de masses élémentaires  $\left[m\right]$:
\begin{equation*}
    \left[m\right] = \underset{\Omega ^{e}} {\int }\left[N\right]^{T}.\rho .\left[N\right].d\Omega ^{e}
\end{equation*}
Les matrices $\left[N\right]$, $\left[B\right]$ et $\left[C\right]$ sont, respectivement, la matrice des fonctions de forme, la matrice des dérivées des fonctions de forme et la matrice d'élasticité de Hooke.
On essaie de résoudre le problème aux valeurs propres suivant:
\begin{equation*}
    \left([K]-\omega ^{2}.[M]\right).\left\{\phi \right\}=\left\{0\right\}
\end{equation*}
$\omega _{j}$ est la pulsation propre associée au mode propre $\left\{{\phi }_{j}\right\}$. Son unité est le  $\text{rad.s}^{-1}$. On défini également la fréquence propre (en  $\mathit{Hz}$) du mode $\left\{{\phi }_{j}\right\}$ :
\begin{equation*}
    f_{j}=\frac{\omega _{j}}{2.\pi }
\end{equation*}

\subsection{Les commandes éclatées dans \codeaster}

Dans \CodeAster, la plupart des commandes de calcul (\CAStaticLinear, \CAStaticNonLinear, \CADynamicNonLinear, \CAThermic, \CAThermicLinear, ...) sont des commandes \emph{monolithiques}, c'est-à-dire que l'ensemble des opérations de calcul (préparation des vecteurs et matrices élémentaires, choix de la numérotation des inconnues, assemblage des éléments finis, résolution du système linéaire, etc.) sont réalisées dans la commande de manière automatique par la commande, sans que l'utilisateur n'ait besoin de les spécifier.


Il est possible de procéder de manière différente, en traitant ces opérations de manière indépendante, par des commandes spécifiques. C'est ce qu'on appelle les commandes \emph{éclatées}. La dynamique fait grand usage de ce mode de fonctionnement car il arrive fréquemment qu'on doive modifier les matrices de manière indépendante.
Il y a sept grandes commandes qui permettent de réaliser ces opérations:
\begin{itemize}
    \item \CACalcElemVect: calcule les vecteurs élémentaires, en particuliers ceux relatifs aux chargements;
    \item \CACalcElemMatr: calcule les matrices élémentaires (rigidité, masse, amortissement);
    \item \CANumbering: prépare la numérotation des inconnues. Cette opération est nécessaire pour \emph{assembler} les vecteurs et matrices élémentaires;
    \item \CAAsseElemVect: assemble des vecteurs élémentaires avec une numérotation donnée;
    \item \CAAsseElemMatr: assemble des matrices élémentaires avec une numérotation donnée;
    \item \CAFactor: factorise une matrice assemblée (c'est-à-dire la décompose en matrices triangulaires et  diagonale;
    \item \CASolve: résout le système linéaire après factorisation de la matrice;
    \item \CAEigenSolver: permet de calculer les modes propres $f_{j}$ et les vecteurs propres $\left\{\phi \right\}$.
\end{itemize}

\subsection{Différentes manières d'imposer des conditions limites}

Pour imposer des conditions limites en déplacement (appelées aussi conditions limites de Dirichlet), il y a deux manières de faire dans \CodeAster. La plus courante est d'utiliser des \emph{multiplicateurs de Lagrange}. Cette méthode est très générique et permet d'imposer toutes les conditions mais elle introduit des inconnues supplémentaires et peut poser des problèmes de conditionnement lors de la résolution (en particulier pour les opérations numériquement délicates comme le calcul des valeurs propres). Dans \CodeAster, on utilise pour ça la commande \CAAffectLoad et \CAEnforceDOF.

La seconde est d'\emph{éliminer} les conditions en manipulant matrice et second membre lors de la résolution du système. Cette méthode est plus restreinte en usage que la précédente, mais elle n'introduit pas d'inconnues supplémentaires et donne des systèmes numériquement plus <<robustes>>. Dans \CodeAster, on utilise pour ça la commande \CAAffectBC.

La méthode de dualisation introduit une matrice des conditions limites (notée souvent $[B]$ dans \CodeAster) qu'il faut calculer explicitement en mode <<commandes éclatées>>.


\subsection{Données du problème}

Récupérer le maillage du bâtiment en coque et chargez-le dans le module \SAModuleMesh avec \SAMMFileImportMED.

\subsection{Ouvrir le module \AsterStudy}

Vous pouvez ouvrir le module \AsterStudy en sélectionnant \fbox{\includegraphics[width=0.3cm,height=0.3cm]{./images/icon_ModuleASTERSTUDY}} dans la barre de sélection des modules.

\subsection{Préparation des données}

Sur la gauche de la fenêtre, cliquez sur l'onglet \SAAWCaseView. On ajoute un \SAAOStage à l'aide de \SAAMOperationsAddStage, on peut le renommer (par \SAKeyRightMouse sur le  \SAAOStage, puis \SAAMBrowserRename).

On utilise les commandes classiques pour définir le maillage, les éléments finis, le matériau, les épaisseurs des plaques et les conditions limites:
\begin{itemize}
    \item Lire le maillage : commande \CAReadMesh. Dans le menu définissant le maillage d'origine, il suffit d'aller choisir celui correspondant au maillage dans le module \SAModuleMesh;
    \item Définir le matériau : commande \CADefineMaterial. Matériau élastique linéaire et isotrope. Attention à ne pas oublier la masse volumique;
    \item Affecter le matériau : commande \CAAffectMaterial. Même matériau sur toute la structure;
    \item Définir les éléments finis : commande \CAAffectModel. Ce sont des éléments DKT (soit \CAModelPlateDKT);
    \item Définir les caractéristiques de la plaque : commande \CAAffectElemProperties, mot-clef \CAAffectElemPropertiesShell sur les groupes de mailles \texttt{Roof}, \texttt{Floor\TextUnderscore 0}, \texttt{Floor\TextUnderscore 1} et \texttt{Floor\TextUnderscore 2} pour une épaisseur $e_p=\unit{0,4}\meter$. Sur les groupes de mailles \texttt{Wall\TextUnderscore Left} et \texttt{Wall\TextUnderscore Right} pour une épaisseur $e_m=\unit{0,18}\meter$;
    \item Définir les conditions limites : commande \CAAffectLoad. Il y a un encastrement sur le groupe de mailles \texttt{Floor\TextUnderscore 0}. Ne pas oubliez de bloquer aussi les \emph{rotations} car c'est un élément de structure.
\end{itemize}

\subsection{Préparation des opérateurs}

On va préparer les matrices et les vecteurs nécessaires au calcul. La difficulté est que ces commandes ne font pas parties de celles déjà triées dans le haut de la fenêtre \AsterStudy (voir \ref{Fig:CommandesTri}).

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.29]{./images/win_AsterStudy_SortedCommand}
    \caption{Commandes triées}
    \label{Fig:CommandesTri}
\end{figure}
Il faut donc afficher toutes les commandes par l'icône \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_ShowAllCommands}}. La fenêtre \ref{Fig:AllCommands} s'ouvre alors et vous pouvez chercher les commandes nécessaires.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./images/win_AsterStudy_AddCommand}
    \caption{Liste de toutes les commandes}
    \label{Fig:AllCommands}
\end{figure}

Voici ces commandes:
\begin{itemize}
    \item Construire les matrices élémentaires de masse : on utilise la commande \CACalcElemMatr. Pour la masse, il faut choisir \CAOptionMass. Ne pas oublier de renseigner le \CAModel, le champ de matériau \CAFieldMaterial et les caractéristiques élémentaires \CAFieldElemProperties ;
    \item Construire les matrices élémentaires de rigidité : on utilise la commande \CACalcElemMatr. Pour la rigidité, il faut choisir \CAOptionRigidity. Ne pas oublier de renseigner le \CAModel, le champ de matériau \CAFieldMaterial, les caractéristiques élémentaires \CAFieldElemProperties et surtout les chargements \CALoad en provenance de l'encastrement (pour évaluer la matrice $[B]$);
    \item Créer la numérotation avec la commande \CANumbering en donnant la matrice de rigidité \CANumberingMatr et non le modèle pour prendre en compte les multiplicateurs de Lagrange pour la matrice $[B]$;
    \item Assembler la matrice de masse : commande \CAAsseElemMatr sur les matrices élémentaires de masse et la numérotation précédemment créée;
    \item Assembler la matrice de rigidité : commande \CAAsseElemMatr sur les matrices élémentaires de rigidité et la numérotation précédemment créée;
\end{itemize}

Si on utilise des conditions limites éliminées \CAAffectBC, il n'y a pas de matrice $[B]$. La nuémrotation n'a donc pas besoin de prendre en compte ces conditions limites et on utilise la commande \CANumbering avec le mot-clef \CAModel.
Dans ce cas, les conditions limites doivent être introduites au moment de l'assemblage de la matrice de rigidité, dans la commande \CAAsseElemMatr, on précise le chargement dans le mot-clef \CAAsseElemMatrBC.

\subsubsection{Vérification}

On va faire une vérification utile et aisée à mettre en œuvre: peser la structure pour vérifier que tout est correct.

Voici comment faire cette vérification.
\begin{itemize}
    \item Ajouter un post-traitement avec la commande \CAComputeElem: choisir l'option \CAComputeElemMass, puis faire le calcul sur \emph {tout le maillage}. Nommer la table \texttt{tablmass} (par exemple);
    \item Ajouter le modèle, la matériau et les caractéristiques élémentaires. Faites-le puis éditer le mot-clef \CAComputeElemMass mais choisissez cette fois-ci le groupe d'éléments définissant le réservoir. Vous avez créé une nouvelle structure de données appelée \emph{table}. Il nous faut l'imprimer;
    \item Ajouter la commande \CAPrintTable pour créer le fichier \texttt{masse.txt} (par exemple).
\end{itemize}

\subsection{Calcul des modes}

Enfin, on réalise le calcul des vecteurs et valeurs propres avec la commande \CAEigenSolver:
\begin{itemize}
    \item Calcul des vecteurs et valeurs propres avec \CAEigenSolver: renseignez \CAMatrixMass  et \CAMatrixRigidity à partir des matrices \emph{assemblées}. Laisser les autres options par défaut;
    \item Impressions des résultats au format \CAFileMED: commande \CAPrintResult. Indiquez l'emplacement du fichier de nom \texttt{modes.rmed}.
\end{itemize}

\subsection{Lancer le calcul}

Pour lancer le calcul, en colonne gauche, cliquer sur l'onglet \SAAWHistoryView. Le cas de calcul (nommé \texttt{Case\TextUnderscore 1} par défaut) apparaît en haut de la colonne de gauche. Au milieu, on retrouve les différentes exécutions. Cliquer sur l'icône de manière à avoir la croix verte \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseGreen}}. Puis lancez le calcul avec \fbox{\includegraphics[scale=0.5]{./images/icon_AsterStudy_CaseRun}}.

\subsection{Post-traiter le résultat}

On a créé deux fichiers: un fichier texte simple \texttt{masse.txt} contenant la masse du bâtiment  et le fichier de résultat contenant les résultats (les modes propres)?
Si tout va bien, un fichier \texttt{masse.txt} sera créé, vous pouvez l'éditer:
\begin{lstlisting}
    #TABLE_SDASTER
    LIEU                     ENTITE   MASSE        CDG_X        CDG_Y        CDG_Z        IX_G         IY_G         IZ_G         IXY_G        IXZ_G        IYZ_G        IX_PRIN_G    IY_PRIN_G    IZ_PRIN_G    ALPHA        BETA         GAMMA
    00000001                 TOUT      3.42400E+05  4.00000E+00  4.00000E+00  6.00000E+00  7.98635E+06  8.90818E+06  4.57410E+06  6.51426E-11 -2.81034E-10  9.49512E-10  4.57410E+06  7.98635E+06  8.90818E+06  0.00000E+00 -9.00000E+01 -9.00000E+01
\end{lstlisting}
Il est un peu difficile à lire car il contient beaucoup de colonnes: la masse, les moments principaux d'inertie et d'autres choses. Ce qui nous intéresse est le premier chiffre.

Pour les résultats (modes propres), retournez dans  l'onglet \SAAWCaseView, en sélectionnant dans \SAAWDataFiles la ligne correspond à l'impression des résultats (\CAPrintResult), puis \SAAMDataFilesPostProcess avec le bouton droit de la souris et le fichier s'ouvre dans l'interface graphique légère. Aucune indication ! A vous de jouer !
