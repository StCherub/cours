\section{Modélisation volumique}\label{chap:Modele3DVolu}

On peut représenter le bâtiment complet en 3D, avec les épaisseurs des murs et des planchers.

\subsection{Réalisation de la géométrie}

Il y a deux manières de procéder (au moins !). la première est de faire comme dans la \autoref{chap:Modele2DSurf}, sauf qu'on créera des cubes au lieu des rectangles.
La deuxième est beaucoup plus directe et elle repose sur l'extension du modèle bidimensionnel surfacique créé dans la \autoref{chap:Modele2DSurf}.

En effet, un modeleur CAO comme \salome peut générer des géométries par des opérations qui s'apparentent à un processus de fabrication comme l'extrusion le long d'une ligne ou la révolution autour d'un axe (on peut aussi <<usiner>> un volume).
Topologiquement, quand on extrude un rectangle suivant un vecteur qui lui est normal, on obtient directement un parallélélipdès rectangle.

Pour la génération de l'objet, on sélectionne \SAGMNewEntityGenerationExtrusion. La fenêtre \ref{Fig:BAT_ConstructionExtrusion} s'ouvre.
\begin{figure}[!ht]
    \centering
    \fbox{\includegraphics[scale = 0.5]{./chapitres/Batiment3Etages/images/Batiment3Etages_Ensemble_3D_FenetreExtrusion}}
    \caption{Fenêtre de création par extrusion}
    \label{Fig:BAT_ConstructionExtrusion}
\end{figure}
Il suffit de choisir le premier mode de construction, puis on sélectionne la structure bidimensionnelle dans \SAGWExtrusionBase, le vecteur \texttt{Oz} dans \SAGWExtrusionVector (les trois vecteurs du repère principal sont toujours construits par défaut par \salome) et d'entrer $8$ dans \SAGWExtrusionHeight.
On obtient la géométrie de la figure \ref{Fig:BAT_3D_GeomInit}.
\begin{figure}[!ht]
    \centering
    \fbox{\includegraphics[scale = 0.5]{./chapitres/Batiment3Etages/images/Batiment3Etages_Ensemble_3D_GeomInitiale}}
    \caption{Géométrie 3D initiale du bâtiment}
    \label{Fig:BAT_3D_GeomInit}
\end{figure}
On voit bien que les surfaces ont été extrudées en volumes et que les arêtes dans le plan ont été extrudées en surfaces normales au plan d'extrusion. Ce qui nous permet d'obtenir une géométrie conforme contenant 22 volumes (vérifier avec \SAGMInspectionWhatIs). Tous ces volumes étant isomorphes à ces cubes (six faces), on pourra directement les mailler en hexaèdres.
Par contre, on aimerait disposer d'un nœud au milieu de la fondation. Par l'extrusion on a bien la ligne médiane, mais forcément, pas le nœud au milieu !
La solution est simple: il suffit de partiionner ce volume avec un plan normal au vecteur \texttt{Oz}, à une distance de $4$ m du fond.
On commence par créer ce plan avec \SAGMNewEntityBasicPlane. La dernière icône du menu de création de plan \fbox{\includegraphics[scale=0.5]{./images/icon_PlaneWorkingOrigin}} permet de définir de les définir par rapport au repère principal. Vous pouvez changer la taille pour que ce soit plus facile à voir en changeant la valeur à $30$ mais attention à ne pas faire de plans trop petits ! Tout comme la partition dans le cas bidimensionnel, il est préférable de prendre un outil (le plan) plus grand que la structure à partitionner.
Ce plan est crée en $z=0$ il faut donc le translater par \SAGMOperationsTransformationTranslation. La fenêtre de création \ref{Fig:BAT_CTranslation} s'ouvre. Choisissez le premier mode \fbox{\includegraphics[width=0.3cm,height=0.3cm]{./images/icon_TranslationGlobalCoordinatesSystem}} (translation dans le repère principal). On sélectionne l'objet à translater (le plan) par \SAGWTranslationObject et la valeur du vecteur de translation par \SAGWTranslationDZ.
On translate avec $DZ=4$. Attention ! Il faut décocher la case \SAGWTranslationCreateCopy pour éviter de créer un deuxième plan.
Il suffit finalement de faire une partitionavec  \SAGMOperationsPartition. La fenêtre \ref{Fig:BAT_WindowOperationPartition} s'ouvre.
Cliquez dans la case \texttt{Object} pour donner le volume 3D initial et \texttt{Tool Objects} pour le plan qui partitionne.
On obtient la géométrie de la figure \ref{Fig:BAT_3D_GeomFinal}.
\begin{figure}[!ht]
    \centering
    \fbox{\includegraphics[scale = 0.5]{./chapitres/Batiment3Etages/images/Batiment3Etages_Ensemble_3D_GeomFinale}}
    \caption{Géométrie 3D finale du bâtiment}
    \label{Fig:BAT_3D_GeomFinal}
\end{figure}
On a désormais un nœud géoémtrique au centre de la fondatation.
Il ne reste plus qu'à créer des \SAGOGroup.

\subsection{Réalisation du maillage}

Il est préférable de réaliser un maillage réglé (avec des hexaèdres), en effet, vu le rapport de dimension entre l'épaisseur des murs, des planchers et les dimensions globales du bâtiment, pour avoir plusieurs mailles dans l'épaisseur avec un maillage libre (tétraèdres), il faudra nécessairement choisir de petits éléments et donc créer beaucoup de nœuds.

Sélectionner \SAMMMeshCreateMesh. La fenêtre \ref{Fig:BAT_CreateMesh} s'ouvre. Il suffit de sélectionner la géométrie (icône \fbox{\includegraphics[width=0.3cm,height=0.3cm]{./images/icon_SelectObject}}) dans l'arbre d'étude. \Salome détecte automatiquement la dimension topologique (ici 3D).

Si on clique sur le bouton \SAMWCreateMeshAuto puis que l'on sélectionne \SAMWCreateMeshAutoMappedVH dans le menu déroulant qui apparaît, alors \Salome propose une combinaison d'hypothèses et d'algorithmes pour faire le maillage. En l’occurrence:
\begin{itemize}
    \item Pour les hypothèses: \SAMOHypoLNumber que \Salome vous invite à modifier lors de la création (prenez 3 par exemple);
    \item Pour les algorithmes, en 1D: \SAMOAlgoLRegularOneD pour les arêtes;
    \item Pour les algorithmes, en 2D: \SAMOAlgoSQuadrangleTwoD pour les faces;
    \item Pour les algorithmes, en 3D: \SAMOAlgoVHexahedron pour les volumes.
\end{itemize}
On obtient le maillage de la figure \ref{Fig:BAT_3D_MeshRegle}

\begin{figure}[!h]
    \centering
    \fbox{\includegraphics[scale = 0.4]{./chapitres/Batiment3Etages/images/Batiment3Etages_Ensemble_3D_MeshRegle}}
    \caption{Maillage régulier en hexaèdres}
    \label{Fig:BAT_3D_MeshRegle}
\end{figure}
