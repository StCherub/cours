\chapter{Modélisation d'un réservoir parallélépipédique}

Ce TP propose de préparer un modèle (géométrie et maillage) pour faire un calcul d'interaction fluide-structure (IFS).

\section{Définition du problème}
\subsection{Géométrie}

L'étude concerne un réservoir mince en acier rempli d'eau. Sa géométrie est simple. Il s'agit d'un simple parallélépipède de hauteur $h=\unit{5}{\meter}$ et de coté $l=\unit{10}{\meter}$. L'épaisseur du réservoir est $e=\unit{2.5}{\centi\meter}$. Le réservoir est fixé (encastré) sur le sol (voir la figure \ref{Fig:DefinitionGeometrie}).

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.4]{./chapitres/ReservoirCube/images/ReservoirCube_GeometrieDefinition}
    \caption{Définition de la géométrie du problème}
    \label{Fig:DefinitionGeometrie}
\end{figure}

\section{Réalisation de la géométrie}

La géométrie est assez complexe à mettre en œuvre. En effet, il faut construire le volume du fluide, les parois du réservoir et la pseudo-paroi nécessaire pour l'interface fluide-structure. De plus, nous avons des contraintes sur le maillage: les nœuds doivent être coïncidents entre le réservoir la surface du fluide et l'interface fluide-structure, mais les éléments seront distincts.
Pour construire un tel objet, il y a (au moins) trois stratégies possibles:
\begin{itemize}
    \item Travailler sur trois objets géométriques distincts puis les rassembler dans un seul maillage de type \SAGOCompound;
    \item Travailler sur un seul objet géométrique en créant un objet de type \SAGOCompound dans le module \SAModuleGeom, puis le mailler;
    \item Ne générer que le maillage du fluide dans \Salome, puis dupliquer les maillages à l'aide d'opérateurs dédiés dans \CodeAster.
\end{itemize}
Dans ce TP, nous allons explorer uniquement la première stratégie.

\subsection{Création des géométries}

Dans le module \SAModuleGeom, on crée un objet \SAGOBox (\SAGMNewEntityPrimitivesBox) de coté $10m$ (suivant $Ox$ et $Oy$) et de hauteur (suivant $Oz$) $5m$. Ce premier objet représente le \emph{fluide}. Nous recommandons de l'appeler \texttt{Fluide} dans l'arbre d'études.
Pour créer les parois et l'interface, nous allons utiliser une petite astuce en dupliquant les rectangles du fluide.
Dans un premier temps, à l'aide de \SAGMNewEntityExplode (voir figure \ref{Fig:ExplodeBox}), on demande à sélectionner les objets de type \SAGOFace, puis on clique dans la case \texttt{Select Sub-shapes}. Ensuite, dans la fenêtre graphique, on peut sélectionner les cinq rectangles (les quatre côtés et le fond) qui serviront à la fois d'interface fluide-structure mais aussi de parois au réservoir.

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./chapitres/ReservoirCube/images/ReservoirCube_ExplodeBox}
    \caption{Sélection des faces du fluide}
    \label{Fig:ExplodeBox}
\end{figure}

Ces cinq faces constituent le réservoir, on crée un \SAGOShell avec
\SAGMNewEntityBuildShell que l'on appellera  \texttt{Reservoir}.
Si on duplique cet object avec \SAGMOperationsTransformationTranslation tout en gardant la case \texttt{Create a copy}, on obtient l'interface fluide-structure que l'on nomme \texttt{Interface}. On obtient ainsi les trois parties (voir figure \ref{Fig:Geometry}).

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.1]{./chapitres/ReservoirCube/images/ReservoirCube_Geometrie}
    \caption{Les trois parties de la géométrie}
    \label{Fig:Geometry}
\end{figure}

On peut faire un \SAGOCompound de ces trois objets par \SAGMNewEntityBuildCompound: on sélectionne les trois objets à l'aide de la touche \keys{Ctrl} et on appelle cet objet \texttt{Tout}.

A la fin, nous obtenons l'arbre d'études sur la figure \ref{Fig:TreeGeometry}. Notez la petite icône devant chaque objet:
\begin{itemize}
    \item L'objet \texttt{Fluide} est un objet de type \SAGOSolid: \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_Solid}}
    \item Les objets \texttt{Interface} et \texttt{Reservoir} sont des objets de type \SAGOShell: \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_Shell}};
    \item L'objet \texttt{Tout} est un objet de type \SAGOCompound: \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_Compound}}.
\end{itemize}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=3.0]{./chapitres/ReservoirCube/images/ReservoirCube_ArbreGeometrie}
    \caption{L'arbre d'études après création des géométries}
    \label{Fig:TreeGeometry}
\end{figure}

\subsection{Création des groupes (\SAGOGroupTXT) dans la géométrie}

On va en suite créer les groupes topologiques permettant de définir des zones de la géométrie pour ensuite leur rattacher des nœuds et des éléments lors du maillage. On utilise pour cela \SAGMNewEntityGroupCreate.

Un objet \SAGOGroup est forcément défini sur un objet géométrique, il est donc important de choisir judicieusement cet objet. Dans la fenêtre, cet objet de référence est désigné par \texttt{Main Shape}. Le nom du groupe est à donner dans \texttt{Group Name}. Ce nom doit comporter moins de 24 caractères\footnote{Cette contrainte vient de \CodeAster, si le nom du groupe est trop long lors de la lecture du maillage dans \CodeAster, alors il sera tronqué à 24 caractères} (lettres, chiffres, underscore mais pas d'accents, ni d'espace).

Il faut également sélectionner le type topologique de ce groupe: ponctuel \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_Vertex}}, linéique \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_Edge}}, surfacique \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_Face}} ou volumique \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_Solid}} par l’icône en haut de la fenêtre de création. Pour faciliter la création des groupes sur les objets complexes, on peut restreindre la sélection sur l'un des sous-objets par
\begin{itemize}
    \item \texttt{Geometrical parts of the second Shape}
    \item \texttt{Only Sub-shapes of the Second Shape}
\end{itemize}
Dans ce cas, il faut bien entendu sélectionner ce sous-objet par \texttt{Second Shape}. Puis on utilise la fenêtre graphique pour faire la sélection (ne pas hésiter à zoomer, tourner ou translater pour faciliter les choses ! ).

\begin{mdframed}[outerlinecolor=black,outerlinewidth=2pt,middlelinewidth=3pt,roundcorner=10pt]
    Une erreur fréquente lors de la création des objets \SAGOGroupTXT\xspace est de ne pas sélectionner une sous-partie de l'objet de référence. Par exemple, vous avez créé un cube à l'aide de six faces. Si on veut créer un groupe sur l'une de ses faces, il ne faut \emph{pas} sélectionner l'une des faces \emph{créatrices} de l'objet dans l'arbre d'étude, mais bien la face qui \emph{défini} l'objet que l'on peut obtenir par exemple par \SAGMNewEntityExplode sur le cube.
\end{mdframed}

On va d'abord créer les groupes sur les objets indépendants:
\begin{itemize}
    \item Pour le fluide: un groupe volumique représentant de nom \texttt{Fluide}, un groupe surfacique sur le dessus et de nom \texttt{Surface};
    \item Pour le réservoir: un groupe surfacique de nom \texttt{Reservoir} représentant toutes les parois (y compris le fond), un groupe surfacique pour uniquement pour le fond de nom \texttt{Encas};
    \item Pour l'interface fluide-structure: un groupe surfacique de nom \texttt{Interface} représentant les cinq parois.
\end{itemize}


Chaque groupe est typé selon la nature de l'entité (ponctuelle \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_GroupPonctual}}, linéique \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_GroupLineic}}, surfacique \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_GroupSurfacic}} et volumique \fbox{\includegraphics[scale=0.5]{./images/icon_Tree_GroupVolumic}}).

\section{Réalisation des maillages}
\subsection{Maillage individuel des objets}

Nous allons réaliser un maillage en hexaèdres (pour le fluide) et en rectangles (pour le réservoir et l'interface fluide-structure). Les géométries étant parfaitement isomorphes à un cube, il ne sera pas nécessaire de préparer la géométrie par des partitions.
Créer trois maillages sur les trois objets: le fluide (volumique, de nom \texttt{Fluide}), l'interface fluide-structure (surfacique, de nom \texttt{Interface}) et le réservoir (surfacique, de nom \texttt{Reservoir}).
Il suffit de sélectionner la géométrie dans l'arbre d'étude. \Salome détecte automatiquement la dimension topologique . Si on clique sur le bouton \SAMWCreateMeshAuto puis \SAMWCreateMeshAutoMappedVH, alors \Salome propose une combinaison minimale d'hypothèses et d'algorithmes pour faire le maillage. En l’occurrence:
\begin{itemize}
    \item Pour les hypothèses: \SAMOHypoLNumber que \Salome vous invite à modifier lors de la création. Gardez la valeur par défaut ($15$);
    \item Pour les algorithmes: \SAMOAlgoVHexahedron.
\end{itemize}

Vous pouvez générer les trois maillages avec \SAMMMeshCompute.
Ne pas oublier de transférer les groupes de mailles correspondants aux groupe géométriques par \SAMMMeshCreateGroupFromGeometry, puis sélection des groupes sur les trois objets. Il faut bien sélectionner les groupes topologiques dans l'arbre d'études du module \SAModuleGeom, sous chaque élément géométrique (fluide, réservoir et interface).

\subsection{Maillage global du problème}

On va rassembler les trois objets dans la même entité: c'est un assemblage de maillages. Pour cela \SAMMMeshBuildCompound, la fenêtre \ref{Fig:CreateMeshCompound} s'ouvre. Nous nommerons ce maillage \texttt{IFSAvecInterface1}.
\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{./chapitres/ReservoirCube/images/ReservoirCube_MeshCreateCompound}
    \caption{Création d'un assemblage de maillage}
    \label{Fig:CreateMeshCompound}
\end{figure}

Quand on crée un objet \SAMOCompound, il est possible de laisser \Salome fusionner automatiquement nœuds et mailles coïncidents. Malheureusement, les deux opérations ne sont pas séparables, on va donc les réaliser \emph{a posteriori} de l'opération.
Sélectionner les trois maillages (\texttt{Fluide}, \texttt{Reservoir} et \texttt{Interface}), décocher \texttt{Merge coincident nodes and elements} et importer les groupes en cochant \texttt{Create groups from input objects}.
On va maintenant simplement fusionner les nœuds en commun entre fluide, réservoir et interface fluide-structure à l'aide de la commande \SAMMModificationTransformationMergeNodes.

Par contre, il ne faut surtout pas fusionner les éléments communs !

Avec les hypothèses par défaut (15 éléments par arête) vous obtenez un maillage de 4096 nœuds, 3600 quadrangles et 3375 hexaèdres.

\subsection{Bilan des maillages disponibles}

Nous disposons donc de quatre maillages différents:
\begin{itemize}
    \item Les maillages représentant le fluide (de nom \texttt{Fluide}), le réservoir (de nom \texttt{Reservoir}) et de l'interface fluide-structure (de nom \texttt{Interface})
    \item Le maillage contenant les trois composants (fluide, réservoir et interface fluide-structure) simultanément, avec nœuds communs entre les trois entités. Ce maillage a été créé par un assemblage des maillages;
\end{itemize}
